function message(icon,text)
{
    Swal.fire({
        title: text,
        icon: icon,
        showCancelButton: false,
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true
    });
}

function warning(icon,text)
{
   	Swal.fire({
	  icon: icon,
	  title: 'Warning',
	  text: text,
	})
}

function reloadTable()
{
    $('#tablena').DataTable().ajax.reload();
}


