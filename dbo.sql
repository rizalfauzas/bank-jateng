/*
 Navicat Premium Data Transfer

 Source Server         : SQL
 Source Server Type    : SQL Server
 Source Server Version : 15004236
 Source Host           : 127.0.0.1:1433
 Source Catalog        : bankJateng
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 15004236
 File Encoding         : 65001

 Date: 02/02/2023 13:42:52
*/


-- ----------------------------
-- Table structure for tm_asuransi
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_asuransi]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_asuransi]
GO

CREATE TABLE [dbo].[tm_asuransi] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [asuransi] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [status] int  NULL
)
GO

ALTER TABLE [dbo].[tm_asuransi] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tm_asuransi
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_asuransi] ON
GO

INSERT INTO [dbo].[tm_asuransi] ([id], [asuransi], [status]) VALUES (N'1', N'ASKRINDO', N'1')
GO

INSERT INTO [dbo].[tm_asuransi] ([id], [asuransi], [status]) VALUES (N'2', N'JAMKRINDO', N'1')
GO

SET IDENTITY_INSERT [dbo].[tm_asuransi] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_cabang
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_cabang]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_cabang]
GO

CREATE TABLE [dbo].[tm_cabang] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [id_region] int  NULL,
  [region] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [id_area] int  NULL,
  [area] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [code_cabang] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [kantor_cabang] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [legacy] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [type_cabang] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tm_cabang] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tm_cabang
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_cabang] ON
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'1', N'1', N'RO I - ACEH', N'1', N'AREA BANDA ACEH', N'ID0019024', N'KC BANDA ACEH DAUD BEUREUEH 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'2', N'1', N'RO I - ACEH', N'2', N'AREA LHOKSEUMAWE', N'ID0018329', N'KC LANGSA 2', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'3', N'1', N'RO I - ACEH', N'2', N'AREA LHOKSEUMAWE', N'ID0018330', N'KC BIREUEN CHIK JOHAN', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'4', N'1', N'RO I - ACEH', N'2', N'AREA LHOKSEUMAWE', N'ID0019049', N'KC KUALA SIMPANG', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'5', N'1', N'RO I - ACEH', N'2', N'AREA LHOKSEUMAWE', N'ID0019050', N'KC LANGSA DARUSSALAM', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'6', N'1', N'RO I - ACEH', N'2', N'AREA LHOKSEUMAWE', N'ID0019051', N'KC LHOKSEUMAWE MERDEKA 3', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'7', N'1', N'RO I - ACEH', N'3', N'AREA MEULABOH', N'ID0019048', N'KC KUTACANE', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'8', N'1', N'RO I - ACEH', N'3', N'AREA MEULABOH', N'ID0019055', N'KC TAPAKTUAN', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'9', N'1', N'RO I - ACEH', N'3', N'AREA MEULABOH', N'ID0019289', N'KCP SINGKIL ISKANDAR MUDA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'10', N'1', N'RO I - ACEH', N'3', N'AREA MEULABOH', N'ID0019311', N'KCP JOHAN PAHLAWAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'11', N'2', N'RO II - MEDAN', N'4', N'AREA BATAM', N'ID0018031', N'KC BATAM RADEN PATAH', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'12', N'2', N'RO II - MEDAN', N'4', N'AREA BATAM', N'ID0018192', N'KCP BATUAJI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'13', N'2', N'RO II - MEDAN', N'4', N'AREA BATAM', N'ID0019040', N'KC BATAM BUSINESS CENTER', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'14', N'2', N'RO II - MEDAN', N'5', N'AREA MEDAN KOTA', N'ID0019009', N'KC MEDAN S PARMAN', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'15', N'2', N'RO II - MEDAN', N'5', N'AREA MEDAN KOTA', N'ID0019218', N'KCP LUBUK PAKAM SUDIRMAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'16', N'2', N'RO II - MEDAN', N'6', N'AREA MEDAN RAYA', N'ID0010087', N'KCP TEBING TINGGI SUDIRMAN 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'17', N'2', N'RO II - MEDAN', N'6', N'AREA MEDAN RAYA', N'ID0010158', N'KCP STABAT KH ZAINUL ARIFIN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'18', N'2', N'RO II - MEDAN', N'6', N'AREA MEDAN RAYA', N'ID0019087', N'KCP BINJAI SUDIRMAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'19', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0010194', N'KCP BAGAN BATU SUDIRMAN 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'20', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0010293', N'KCP UJUNG BATU 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'21', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0010484', N'KCP TELUK KUANTAN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'22', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0018060', N'KC PANAM ARENGKA', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'23', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0018137', N'KCP PANGKALAN KERINCI MAHARAJA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'24', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0018258', N'KCP PEKANBARU A YANI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'25', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0019026', N'KC PEKANBARU ARIFIN AHMAD', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'26', N'2', N'RO II - MEDAN', N'7', N'AREA PEKANBARU', N'ID0019178', N'KCP DURI HANG TUAH 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'27', N'2', N'RO II - MEDAN', N'8', N'AREA PEMATANGSIANTAR', N'ID0010030', N'KC RANTAU PRAPAT', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'28', N'2', N'RO II - MEDAN', N'8', N'AREA PEMATANGSIANTAR', N'ID0010049', N'KC PEMATANGSIANTAR PERINTIS', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'29', N'2', N'RO II - MEDAN', N'8', N'AREA PEMATANGSIANTAR', N'ID0010223', N'KCP KOTA PINANG', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'30', N'2', N'RO II - MEDAN', N'8', N'AREA PEMATANGSIANTAR', N'ID0018117', N'KCP PEMATANGSIANTAR', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'31', N'2', N'RO II - MEDAN', N'8', N'AREA PEMATANGSIANTAR', N'ID0019219', N'KCP RANTAU PRAPAT', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'32', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0010180', N'KC BANDAR JAYA', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'33', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0018057', N'KC BANDAR LAMPUNG TELUK BETUNG', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'34', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0018061', N'KC BATURAJA RAHMAN HAMIDI', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'35', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0018161', N'KCP RAJABASA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'36', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0018240', N'KCP BANDAR LAMPUNG ANTASARI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'37', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0018260', N'KCP BELITANG SUDIRMAN', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'38', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0018261', N'KCP KOTA MARTAPURA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'39', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0019028', N'KC BANDAR LAMPUNG KEDATON', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'40', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0019114', N'KCP METRO AH NASUTION', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'41', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0019158', N'KCP SRIBHAWONO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'42', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0019175', N'KCP PRINGSEWU A YANI 3', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'43', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0019248', N'KCP TULANG BAWANG BARAT', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'44', N'3', N'RO III - PALEMBANG', N'9', N'AREA BANDAR LAMPUNG', N'ID0019249', N'KCP BANDAR LAMPUNG NATAR', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'45', N'3', N'RO III - PALEMBANG', N'10', N'AREA BENGKULU', N'ID0010056', N'KC BENGKULU S PARMAN 1', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'46', N'3', N'RO III - PALEMBANG', N'10', N'AREA BENGKULU', N'ID0010209', N'KCP CURUP', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'47', N'3', N'RO III - PALEMBANG', N'10', N'AREA BENGKULU', N'ID0018062', N'KC LUBUK LINGGAU', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'48', N'3', N'RO III - PALEMBANG', N'10', N'AREA BENGKULU', N'ID0018067', N'KC BENGKULU PANORAMA', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'49', N'3', N'RO III - PALEMBANG', N'10', N'AREA BENGKULU', N'ID0018266', N'KCP MUARA BELITI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'50', N'3', N'RO III - PALEMBANG', N'10', N'AREA BENGKULU', N'ID0018290', N'KCP BENGKULU ARGAMAKMUR', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'51', N'3', N'RO III - PALEMBANG', N'10', N'AREA BENGKULU', N'ID0019038', N'KC BENGKULU S PARMAN 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'52', N'3', N'RO III - PALEMBANG', N'11', N'AREA JAMBI', N'ID0010167', N'KCP MUARA BUNGO M YAMIN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'53', N'3', N'RO III - PALEMBANG', N'11', N'AREA JAMBI', N'ID0010310', N'KCP SAROLANGUN 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'54', N'3', N'RO III - PALEMBANG', N'11', N'AREA JAMBI', N'ID0010437', N'KCP JAMBI JELUTUNG', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'55', N'3', N'RO III - PALEMBANG', N'11', N'AREA JAMBI', N'ID0018040', N'KC JAMBI HAYAM WURUK 1', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'56', N'3', N'RO III - PALEMBANG', N'11', N'AREA JAMBI', N'ID0019027', N'KC JAMBI HAYAM WURUK 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'57', N'3', N'RO III - PALEMBANG', N'11', N'AREA JAMBI', N'ID0019203', N'KCP JAMBI RIMBO BUJANG 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'58', N'3', N'RO III - PALEMBANG', N'11', N'AREA JAMBI', N'ID0019204', N'KCP JAMBI SUNGAI BAHAR', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'59', N'3', N'RO III - PALEMBANG', N'12', N'AREA PADANG', N'ID0010226', N'KCP PASAMAN BARAT SUDIRMAN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'60', N'3', N'RO III - PALEMBANG', N'12', N'AREA PADANG', N'ID0010300', N'KCP PULAU PUNJUNG 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'61', N'3', N'RO III - PALEMBANG', N'12', N'AREA PADANG', N'ID0018043', N'KC BUKITTINGGI SUDIRMAN 2', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'62', N'3', N'RO III - PALEMBANG', N'12', N'AREA PADANG', N'ID0018212', N'KCP PAYAKUMBUH SUDIRMAN', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'63', N'3', N'RO III - PALEMBANG', N'12', N'AREA PADANG', N'ID0019025', N'KC PADANG KIS MANGUNSARKORO', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'64', N'3', N'RO III - PALEMBANG', N'12', N'AREA PADANG', N'ID0019221', N'KCP SOLOK A DAHLAN 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'65', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0010019', N'KC PALEMBANG DEMANG', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'66', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0010137', N'KC PRABUMULIH SUDIRMAN 1', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'67', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0010240', N'KCP PALEMBANG KM 6', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'68', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0010291', N'KCP SUNGAI LILIN 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'69', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0010638', N'KCP SUNGAI LIAT TOWN SQUARE', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'70', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018016', N'KC PALEMBANG SUDIRMAN', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'71', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018064', N'KC PALEMBANG SUKODADI', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'72', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018121', N'KCP PALEMBANG DEMANG', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'73', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018263', N'KCP TUGU MULYO 1', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'74', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018264', N'KCP INDRALAYA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'75', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018265', N'KCP MUARA ENIM', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'76', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018275', N'KCP SEKAYU', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'77', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018276', N'KCP PALEMBANG MERDEKA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'78', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018277', N'KCP PANGKALAN BALAI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'79', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0018278', N'KCP PALEMBANG KENTEN 1', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'80', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0019010', N'KC PALEMBANG A RIVAI', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'81', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0019068', N'KC PRABUMULIH SUDIRMAN 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'82', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0019095', N'KCP PALEMBANG SUDIRMAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'83', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0019224', N'KCP PANGKAL PINANG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'84', N'3', N'RO III - PALEMBANG', N'13', N'AREA PALEMBANG', N'ID0019388', N'KCP PALEMBANG KENTEN 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'85', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0010143', N'KCP RANGKASBITUNG 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'86', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0010292', N'KCP LABUAN 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'87', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0010401', N'KCP PANDEGLANG 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'88', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0018033', N'KC CILEGON A YANI', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'89', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0018197', N'KCP SERANG A YANI 1', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'90', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0018199', N'KCP TANGERANG CIKUPA 2', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'91', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0018200', N'KCP TANGERANG BALARAJA 3', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'92', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0019014', N'KC TANGERANG DAAN MOGOT', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'93', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0019015', N'KC CILEGON TIRTAYASA 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'94', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0019090', N'KCP TANGERANG JATIUWUNG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'95', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0019131', N'KCP TANGERANG TANAH TINGGI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'96', N'4', N'RO IV - JAKARTA 1', N'14', N'AREA BANTEN', N'ID0019199', N'KCP SERANG KRAGILAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'97', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0010264', N'KCP CIKARANG METRO BOULEVARD', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'98', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0010603', N'KCP BEKASI KALIABANG', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'99', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0018030', N'KC BEKASI 2', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'100', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0018187', N'KCP CIKARANG KOTA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'101', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0018188', N'KCP BEKASI GRAND WISATA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'102', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0018189', N'KCP BEKASI PONDOK GEDE', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'103', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0018191', N'KCP BEKASI WIBAWA MUKTI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'104', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0019034', N'KC BEKASI SQUARE', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'105', N'4', N'RO IV - JAKARTA 1', N'15', N'AREA BEKASI', N'ID0019082', N'KCP BEKASI GALAXY', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'106', N'4', N'RO IV - JAKARTA 1', N'16', N'AREA JAKARTA KEBON JERUK', N'ID0010045', N'KC JAKARTA KEBON JERUK 1', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'107', N'4', N'RO IV - JAKARTA 1', N'16', N'AREA JAKARTA KEBON JERUK', N'ID0018196', N'KCP TANGERANG CILEDUG 1', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'108', N'4', N'RO IV - JAKARTA 1', N'17', N'AREA JAKARTA KELAPA GADING', N'ID0010144', N'KC JAKARTA HARCO MANGGA DUA', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'109', N'4', N'RO IV - JAKARTA 1', N'18', N'AREA JAKARTA RAWAMANGUN', N'ID0019080', N'KCP BEKASI KALIMALANG SQUARE', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'110', N'4', N'RO IV - JAKARTA 1', N'18', N'AREA JAKARTA RAWAMANGUN', N'ID0019259', N'KCP JAKARTA PANGKALAN JATI 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'111', N'4', N'RO IV - JAKARTA 1', N'19', N'AREA JAKARTA THAMRIN', N'ID0018017', N'KC JAKARTA BENDUNGAN HILIR', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'112', N'4', N'RO IV - JAKARTA 1', N'19', N'AREA JAKARTA THAMRIN', N'ID0018122', N'KCP JAKARTA KEMENTERIAN AGAMA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'113', N'4', N'RO IV - JAKARTA 1', N'19', N'AREA JAKARTA THAMRIN', N'ID0018123', N'KCP JAKARTA MAHKAMAH AGUNG', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'114', N'4', N'RO IV - JAKARTA 1', N'19', N'AREA JAKARTA THAMRIN', N'ID0019011', N'KC JAKARTA WAHID HASYIM', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'115', N'5', N'RO V - JAKARTA 2', N'20', N'AREA BOGOR', N'ID0010128', N'KCP BOGOR TAJUR 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'116', N'5', N'RO V - JAKARTA 2', N'20', N'AREA BOGOR', N'ID0010129', N'KCP BOGOR DRAMAGA PERWIRA', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'117', N'5', N'RO V - JAKARTA 2', N'20', N'AREA BOGOR', N'ID0018232', N'KCP BOGOR CILEUNGSI RAYA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'118', N'5', N'RO V - JAKARTA 2', N'20', N'AREA BOGOR', N'ID0018234', N'KCP BOGOR CITEUREUP 3', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'119', N'5', N'RO V - JAKARTA 2', N'20', N'AREA BOGOR', N'ID0019013', N'KC BOGOR AHMAD YANI', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'120', N'5', N'RO V - JAKARTA 2', N'20', N'AREA BOGOR', N'ID0019101', N'KCP BOGOR PAJAJARAN BANTARJATI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'121', N'5', N'RO V - JAKARTA 2', N'20', N'AREA BOGOR', N'ID0019112', N'KCP BOGOR CIBINONG RAYA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'122', N'5', N'RO V - JAKARTA 2', N'21', N'AREA DEPOK', N'ID0010199', N'KCP DEPOK CIMANGGIS 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'123', N'5', N'RO V - JAKARTA 2', N'21', N'AREA DEPOK', N'ID0019081', N'KCP JAKARTA CIBUBUR 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'124', N'5', N'RO V - JAKARTA 2', N'24', N'AREA JAKARTA SAHARJO', N'ID0019075', N'KCP JAKARTA DEWI SARTIKA 3', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'125', N'5', N'RO V - JAKARTA 2', N'25', N'AREA TANGERANG SELATAN', N'ID0018158', N'KCP BINTARO KEBAYORAN ARCADE 1', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'126', N'5', N'RO V - JAKARTA 2', N'25', N'AREA TANGERANG SELATAN', N'ID0018198', N'KCP TANGERANG KARAWACI MUTIARA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'127', N'5', N'RO V - JAKARTA 2', N'25', N'AREA TANGERANG SELATAN', N'ID0019012', N'KC TANGERANG BSD PAHLAWAN SERIBU', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'128', N'5', N'RO V - JAKARTA 2', N'25', N'AREA TANGERANG SELATAN', N'ID0019195', N'KCP TANGERANG PARAKAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'129', N'5', N'RO V - JAKARTA 2', N'25', N'AREA TANGERANG SELATAN', N'ID0019196', N'KCP TANGERANG ALAM SUTRA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'130', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0010094', N'KC GARUT', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'131', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0010360', N'KCP BANDUNG RANCAEKEK 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'132', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0018108', N'KCP GARUT', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'133', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019004', N'KC BANDUNG CITARUM', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'134', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019017', N'KC BANDUNG SUNIARAJA', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'135', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019083', N'KCP BANDUNG SETIABUDI 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'136', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019091', N'KCP BANDUNG BUAH BATU 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'137', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019134', N'KCP BANDUNG MAJALAYA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'138', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019135', N'KCP BANDUNG CIJERAH', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'139', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019136', N'KCP BANDUNG UJUNG BERUNG 1', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'140', N'6', N'RO VI - BANDUNG', N'26', N'AREA BANDUNG KOTA', N'ID0019160', N'KCP SUMEDANG TANJUNG SARI 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'141', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0010271', N'KCP CIANJUR CIPANAS 2', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'142', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0018052', N'KC SUKABUMI A YANI', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'143', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0018218', N'KCP CIANJUR ABDULLAH BIN NUH', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'144', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0019016', N'KC CIANJUR ABDULLAH BIN NUH', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'145', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0019064', N'KC PURWAKARTA GANDANEGARA', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'146', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0019137', N'KCP CIANJUR CIRANJANG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'147', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0019180', N'KCP SUKABUMI CIBADAK SILIWANGI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'148', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0019189', N'KCP SUBANG OTISTA 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'149', N'6', N'RO VI - BANDUNG', N'27', N'AREA BANDUNG RAYA', N'ID0019197', N'KCP SUKABUMI PELABUHAN RATU', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'150', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0010151', N'KCP KUNINGAN A YANI 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'151', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0010190', N'KCP CIAMIS JUANDA', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'152', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0010241', N'KCP CIREBON PLERED 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'153', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0010247', N'KCP BANJAR SUWARTO', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'154', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0018020', N'KC CIREBON SISINGAMANGARAJA', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'155', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0018138', N'KCP CIREBON PLERED 2', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'156', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0018214', N'KCP CIAMIS SUDIRMAN', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'157', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0019018', N'KC CIREBON SILIWANGI', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'158', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0019065', N'KC TASIKMALAYA A YANI', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'159', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0019084', N'KCP MAJALENGKA JATIWANGI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'160', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0019092', N'KCP INDRAMAYU SOEPRAPTO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'161', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0019139', N'KCP CIREBON ARJAWINANGUN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'162', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0019140', N'KCP MAJALENGKA KADIPATEN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'163', N'6', N'RO VI - BANDUNG', N'28', N'AREA CIREBON', N'ID0019190', N'KCP PANGANDARAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'164', N'7', N'RO VII - SEMARANG', N'29', N'AREA PURWOKERTO', N'ID0019033', N'KC PURWOKERTO KARANGKOBAR', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'165', N'7', N'RO VII - SEMARANG', N'29', N'AREA PURWOKERTO', N'ID0019141', N'KCP CILACAP GATOT SUBROTO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'166', N'7', N'RO VII - SEMARANG', N'29', N'AREA PURWOKERTO', N'ID0019142', N'KCP AJIBARANG PANCASAN 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'167', N'7', N'RO VII - SEMARANG', N'29', N'AREA PURWOKERTO', N'ID0019143', N'KCP PURBALINGGA MT HARYONO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'168', N'7', N'RO VII - SEMARANG', N'29', N'AREA PURWOKERTO', N'ID0019250', N'KCP PEMALANG SUDIRMAN 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'169', N'7', N'RO VII - SEMARANG', N'30', N'AREA SEMARANG KOTA', N'ID0019005', N'KC SEMARANG MT HARYONO', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'170', N'7', N'RO VII - SEMARANG', N'30', N'AREA SEMARANG KOTA', N'ID0019085', N'KCP KUDUS A YANI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'171', N'7', N'RO VII - SEMARANG', N'30', N'AREA SEMARANG KOTA', N'ID0019099', N'KCP SEMARANG SUDIARTO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'172', N'7', N'RO VII - SEMARANG', N'30', N'AREA SEMARANG KOTA', N'ID0019177', N'KCP DEMAK SULTAN FATTAH', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'173', N'7', N'RO VII - SEMARANG', N'30', N'AREA SEMARANG KOTA', N'ID0019246', N'KCP PURWODADI A YANI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'174', N'7', N'RO VII - SEMARANG', N'31', N'AREA SEMARANG RAYA', N'ID0019032', N'KC TEGAL KS TUBUN', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'175', N'7', N'RO VII - SEMARANG', N'31', N'AREA SEMARANG RAYA', N'ID0019063', N'KC PEKALONGAN YAGIS', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'176', N'7', N'RO VII - SEMARANG', N'31', N'AREA SEMARANG RAYA', N'ID0019145', N'KCP BREBES SUDIRMAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'177', N'7', N'RO VII - SEMARANG', N'31', N'AREA SEMARANG RAYA', N'ID0019247', N'KCP KENDAL WELERI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'178', N'7', N'RO VII - SEMARANG', N'32', N'AREA SOLO', N'ID0010238', N'KCP WONOGIRI SUDIRMAN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'179', N'7', N'RO VII - SEMARANG', N'32', N'AREA SOLO', N'ID0019020', N'KC SOLO VETERAN', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'180', N'7', N'RO VII - SEMARANG', N'32', N'AREA SOLO', N'ID0019111', N'KCP KARANGANYAR PALUR 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'181', N'7', N'RO VII - SEMARANG', N'32', N'AREA SOLO', N'ID0019162', N'KCP KLATEN VETERAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'182', N'7', N'RO VII - SEMARANG', N'32', N'AREA SOLO', N'ID0019172', N'KCP SRAGEN ATRIUM BLOK H', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'183', N'7', N'RO VII - SEMARANG', N'33', N'AREA YOGYAKARTA', N'ID0010164', N'KCP WONOSARI', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'184', N'7', N'RO VII - SEMARANG', N'33', N'AREA YOGYAKARTA', N'ID0010189', N'KCP MAGELANG SQUARE', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'185', N'7', N'RO VII - SEMARANG', N'33', N'AREA YOGYAKARTA', N'ID0010245', N'KCP GODEAN 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'186', N'7', N'RO VII - SEMARANG', N'33', N'AREA YOGYAKARTA', N'ID0010598', N'KCP BANTUL MELIKAN LOR', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'187', N'7', N'RO VII - SEMARANG', N'33', N'AREA YOGYAKARTA', N'ID0019019', N'KC YOGYAKARTA KOLONEL SUGIYONO', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'188', N'7', N'RO VII - SEMARANG', N'33', N'AREA YOGYAKARTA', N'ID0019098', N'KCP YOGYAKARTA A DAHLAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'189', N'7', N'RO VII - SEMARANG', N'33', N'AREA YOGYAKARTA', N'ID0019253', N'KCP SLEMAN PRAMBANAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'190', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0010165', N'KCP SUMBAWA DIPONEGORO', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'191', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0010166', N'KCP PANCOR', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'192', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0018042', N'KC MATARAM PEJANGGIK 1', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'193', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0018211', N'KCP SELONG', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'194', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019023', N'KC MATARAM PEJANGGIK 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'195', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019042', N'KC DENPASAR MAHENDRADATTA', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'196', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019073', N'KC BIMA SOETTA 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'197', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019153', N'KCP LOMBOK SELONG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'198', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019216', N'KCP LOMBOK AIKMEL', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'199', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019217', N'KCP LOMBOK PRAYA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'200', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019273', N'KCP SINGARAJA UDAYANA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'201', N'8', N'RO VIII - SURABAYA', N'34', N'AREA DENPASAR', N'ID0019382', N'KCP BERTAIS MANDALIKA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'202', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0010338', N'KCP SITUBONDO BASUKI RAHMAT', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'203', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0018028', N'KC JEMBER A YANI', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'204', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0018165', N'KCP BANYUWANGI KERTOSARI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'205', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0018166', N'KCP JEMBER AMBULU', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'206', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0018169', N'KCP KENCONG', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'207', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0018170', N'KCP SITUBONDO A YANI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'208', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0018254', N'KCP LUMAJANG SUDIRMAN', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'209', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0019061', N'KC BANYUWANGI A YANI', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'210', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0019066', N'KC JEMBER TRUNOJOYO', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'211', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0019149', N'KCP BANYUWANGI ROGOJAMPI 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'212', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0019150', N'KCP BANYUWANGI GENTENG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'213', N'8', N'RO VIII - SURABAYA', N'35', N'AREA JEMBER', N'ID0019215', N'KCP LUMAJANG IMAM BONJOL', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'214', N'8', N'RO VIII - SURABAYA', N'36', N'AREA KEDIRI', N'ID0018027', N'KC KEDIRI TRADE CENTER', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'215', N'8', N'RO VIII - SURABAYA', N'36', N'AREA KEDIRI', N'ID0019022', N'KC KEDIRI HASSANUDIN', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'216', N'8', N'RO VIII - SURABAYA', N'36', N'AREA KEDIRI', N'ID0019067', N'KC MADIUN KARTOHARJO', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'217', N'8', N'RO VIII - SURABAYA', N'36', N'AREA KEDIRI', N'ID0019169', N'KCP NGANJUK YOS SUDARSO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'218', N'8', N'RO VIII - SURABAYA', N'36', N'AREA KEDIRI', N'ID0019170', N'KCP KEDIRI PARE LAWU', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'219', N'8', N'RO VIII - SURABAYA', N'36', N'AREA KEDIRI', N'ID0019171', N'KCP KEDIRI TENDEAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'220', N'8', N'RO VIII - SURABAYA', N'36', N'AREA KEDIRI', N'ID0019205', N'KCP BLITAR TANJUNG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'221', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0010162', N'KCP BATU DIPONEGORO', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'222', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0010659', N'KCP MALANG TUREN 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'223', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0018054', N'KC PROBOLINGGO', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'224', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0018251', N'KCP MALANG SINGOSARI 1', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'225', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0019021', N'KC MALANG SOETTA', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'226', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0019059', N'KC PASURUAN SUDIRMAN', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'227', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0019115', N'KCP MALANG KEPANJEN SULTAN AGUNG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'228', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0019148', N'KCP MALANG PAKIS KEMBAR', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'229', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0019168', N'KCP MALANG BULULAWANG', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'230', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0019270', N'KCP PROBOLINGGO SUDIRMAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'231', N'8', N'RO VIII - SURABAYA', N'37', N'AREA MALANG', N'ID0019385', N'KCP PANDAAN SQUARE', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'232', N'8', N'RO VIII - SURABAYA', N'38', N'AREA SURABAYA KOTA', N'ID0019006', N'KC SURABAYA MERR 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'233', N'8', N'RO VIII - SURABAYA', N'38', N'AREA SURABAYA KOTA', N'ID0019037', N'KC SURABAYA DIPONEGORO', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'234', N'8', N'RO VIII - SURABAYA', N'38', N'AREA SURABAYA KOTA', N'ID0019056', N'KC SIDOARJO AHMAD YANI', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'235', N'8', N'RO VIII - SURABAYA', N'38', N'AREA SURABAYA KOTA', N'ID0019086', N'KCP SIDOARJO GATEWAY', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'236', N'8', N'RO VIII - SURABAYA', N'38', N'AREA SURABAYA KOTA', N'ID0019096', N'KCP SURABAYA HR MUHAMMAD', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'237', N'8', N'RO VIII - SURABAYA', N'38', N'AREA SURABAYA KOTA', N'ID0019237', N'KCP SURABAYA MULYOSARI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'238', N'8', N'RO VIII - SURABAYA', N'39', N'AREA SURABAYA RAYA', N'ID0010103', N'KCP TUBAN BASUKI RAHMAT 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'239', N'8', N'RO VIII - SURABAYA', N'39', N'AREA SURABAYA RAYA', N'ID0010282', N'KCP SUMENEP TRUNOJOYO 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'240', N'8', N'RO VIII - SURABAYA', N'39', N'AREA SURABAYA RAYA', N'ID0018209', N'KCP JOMBANG A WAHID', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'241', N'8', N'RO VIII - SURABAYA', N'39', N'AREA SURABAYA RAYA', N'ID0019097', N'KCP BANGKALAN TRUNOJOYO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'242', N'8', N'RO VIII - SURABAYA', N'39', N'AREA SURABAYA RAYA', N'ID0019165', N'KCP MOJOKERTO MOJOSARI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'243', N'8', N'RO VIII - SURABAYA', N'39', N'AREA SURABAYA RAYA', N'ID0019239', N'KCP BOJONEGORO SUROPATI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'244', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0010041', N'KC KUTAI KARTANEGARA', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'245', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0010295', N'KCP BALIKPAPAN SEPINGGAN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'246', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0010450', N'KCP SAMARINDA DI PANJAITAN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'247', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0010541', N'KCP PENAJAM 1', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'248', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0019029', N'KC SAMARINDA BHAYANGKARA', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'249', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0019030', N'KC BALIKPAPAN SUDIRMAN 3', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'250', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0019154', N'KCP SAMARINDA BUNG TOMO', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'251', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0019183', N'KCP PASER TANAH GROGOT', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'252', N'9', N'RO IX - BANJARMASIN', N'40', N'AREA BALIKPAPAN', N'ID0019235', N'KCP SANGATTA 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'253', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0010125', N'KCP BATULICIN JHONLIN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'254', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0010368', N'KC PANGKALAN BUN', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'255', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0010377', N'KC TANJUNG', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'256', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0010462', N'KC SAMPIT', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'257', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0018009', N'KC BANJARMASIN A YANI 1', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'258', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0019007', N'KC BANJARMASIN A YANI 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'259', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0019071', N'KC PALANGKARAYA 3', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'260', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0019120', N'KCP BANJARBARU A YANI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'261', N'9', N'RO IX - BANJARMASIN', N'41', N'AREA BANJARMASIN', N'ID0019185', N'KCP BANJARMASIN SULTAN ADAM', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'262', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0010026', N'KC PONTIANAK ABDURRACHMAN', N'BSM', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'263', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0010299', N'KCP PONTIANAK DIPONEGORO', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'264', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0010447', N'KCP PONTIANAK SUNGAI JAWI', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'265', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0018037', N'KC PONTIANAK A YANI', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'266', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0019031', N'KC PONTIANAK GUSTI SULUNG', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'267', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0019110', N'KCP PONTIANAK A YANI 2', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'268', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0019126', N'KCP PONTIANAK SIANTAN', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'269', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0019232', N'KCP SANGGAU A YANI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'270', N'9', N'RO IX - BANJARMASIN', N'42', N'AREA PONTIANAK', N'ID0019233', N'KCP SINGKAWANG MERDEKA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'271', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0010122', N'KCP PALOPO RATULANGI', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'272', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0010284', N'KCP SENGKANG SUDIRMAN', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'273', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018014', N'KC MAKASSAR 2', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'274', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018056', N'KC MAKASSAR VETERAN', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'275', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018071', N'KC PALOPO', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'276', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018072', N'KC PAREPARE', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'277', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018237', N'KCP MAKASSAR TAMALANREA 2', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'278', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018306', N'KCP MASAMBA', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'279', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018307', N'KCP TOMONI', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'280', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018310', N'KCP SIDRAP', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'281', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018311', N'KCP ENREKANG', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'282', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0018312', N'KCP WATANSOPPENG', N'BNIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'283', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0019008', N'KC MAKASSAR PETTARANI', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'284', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0019210', N'KCP BONE JEPPEE', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'285', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0019213', N'KCP PINRANG AHMAD YANI', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'286', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0019214', N'KCP GOWA SUNGGUMINASA', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'287', N'10', N'RO X - MAKASSAR', N'43', N'AREA MAKASSAR', N'ID0019267', N'KCP MAKASSAR PANNAMPU', N'BRIS', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'288', N'10', N'RO X - MAKASSAR', N'44', N'AREA MANADO', N'ID0019043', N'KC MANADO MANTOS', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'289', N'10', N'RO X - MAKASSAR', N'45', N'AREA PALU', N'ID0010668', N'KCP BAUBAU YOS SUDARSO', N'BSM', N'KCP')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'290', N'10', N'RO X - MAKASSAR', N'45', N'AREA PALU', N'ID0018050', N'KC PALU M YAMIN', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'291', N'10', N'RO X - MAKASSAR', N'45', N'AREA PALU', N'ID0018051', N'KC KENDARI MT HARYONO', N'BNIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'292', N'10', N'RO X - MAKASSAR', N'45', N'AREA PALU', N'ID0019039', N'KC KENDARI A SILONDAE 2', N'BRIS', N'KC')
GO

INSERT INTO [dbo].[tm_cabang] ([id], [id_region], [region], [id_area], [area], [code_cabang], [kantor_cabang], [legacy], [type_cabang]) VALUES (N'293', N'10', N'RO X - MAKASSAR', N'45', N'AREA PALU', N'ID0019041', N'KC PALU WOLTER MONGINSIDI', N'BRIS', N'KC')
GO

SET IDENTITY_INSERT [dbo].[tm_cabang] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_dokumen_klaim
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_dokumen_klaim]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_dokumen_klaim]
GO

CREATE TABLE [dbo].[tm_dokumen_klaim] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [idklaim] int  NULL,
  [surat_pengajuan_klaim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [berita_acara_klaim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sertifikat_penjaminan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ktp] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [kartu_pegawai] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [rekening] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ket_kolektibilitas] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [perjanjian_kredit] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [kartu_pinjaman] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [berkas_lain] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [keterangan] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [surat_klaim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [polis_asuransi_kredit] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sk_pegawai] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [persetujuan_kredit] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sk_kematian] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [surat_phk] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [kartu_identitas_pekerjaan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [bukti_bayar_premi] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sk_pengangkatan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sk_pengangkatan_tetap] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sk_golongan_terakhir] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [surat_kuasa_potgaji] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [surat_rekomendasi] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [slip_gaji] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [analasi_kredit] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [bukti_pencairan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [bukti_agunan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tm_dokumen_klaim] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'surat_pengajuan_klaim'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'berita_acara_klaim'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'sertifikat_penjaminan'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'ktp'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'kartu_pegawai'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'rekening'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'ket_kolektibilitas'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'perjanjian_kredit'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'kartu_pinjaman'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jamkrindo',
'SCHEMA', N'dbo',
'TABLE', N'tm_dokumen_klaim',
'COLUMN', N'berkas_lain'
GO


-- ----------------------------
-- Records of tm_dokumen_klaim
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_dokumen_klaim] ON
GO

INSERT INTO [dbo].[tm_dokumen_klaim] ([id], [idklaim], [surat_pengajuan_klaim], [berita_acara_klaim], [sertifikat_penjaminan], [ktp], [kartu_pegawai], [rekening], [ket_kolektibilitas], [perjanjian_kredit], [kartu_pinjaman], [berkas_lain], [keterangan], [surat_klaim], [polis_asuransi_kredit], [sk_pegawai], [persetujuan_kredit], [sk_kematian], [surat_phk], [kartu_identitas_pekerjaan], [bukti_bayar_premi], [sk_pengangkatan], [sk_pengangkatan_tetap], [sk_golongan_terakhir], [surat_kuasa_potgaji], [surat_rekomendasi], [slip_gaji], [analasi_kredit], [bukti_pencairan], [bukti_agunan]) VALUES (N'1', N'1', N'SuratPermohonKlaim_1675242021.pdf', N'BeritaAcaraKlaim_1675241899.pdf', N'SertifikatPenjaminan_1675242060.pdf', N'KTP_1675242060.pdf', N'KartuPegawai_1675242060.pdf', N'Rekening_1675242060.pdf', N'KetKolektibilitas_1675242060.pdf', NULL, NULL, NULL, N'oke sipp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[tm_dokumen_klaim] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_groups
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_groups]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_groups]
GO

CREATE TABLE [dbo].[tm_groups] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [groups] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tm_groups] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tm_groups
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_groups] ON
GO

INSERT INTO [dbo].[tm_groups] ([id], [groups]) VALUES (N'1', N'Admin')
GO

INSERT INTO [dbo].[tm_groups] ([id], [groups]) VALUES (N'2', N'Kantor Pusat')
GO

INSERT INTO [dbo].[tm_groups] ([id], [groups]) VALUES (N'3', N'Regional')
GO

INSERT INTO [dbo].[tm_groups] ([id], [groups]) VALUES (N'4', N'Area')
GO

INSERT INTO [dbo].[tm_groups] ([id], [groups]) VALUES (N'5', N'Cabang')
GO

SET IDENTITY_INSERT [dbo].[tm_groups] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_jenis_klaim
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_jenis_klaim]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_jenis_klaim]
GO

CREATE TABLE [dbo].[tm_jenis_klaim] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [jenis_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tm_jenis_klaim] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tm_jenis_klaim
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_jenis_klaim] ON
GO

INSERT INTO [dbo].[tm_jenis_klaim] ([id], [jenis_klaim]) VALUES (N'1', N'Klaim Meninggal Dunia')
GO

INSERT INTO [dbo].[tm_jenis_klaim] ([id], [jenis_klaim]) VALUES (N'2', N'Pemutusan Hak Kerja (PHK)')
GO

INSERT INTO [dbo].[tm_jenis_klaim] ([id], [jenis_klaim]) VALUES (N'3', N'Kredit Macet')
GO

SET IDENTITY_INSERT [dbo].[tm_jenis_klaim] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_menu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_menu]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_menu]
GO

CREATE TABLE [dbo].[tm_menu] (
  [id_menu] int  IDENTITY(1,1) NOT NULL,
  [nama_menu] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [link] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [icon] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [kat_menu] int  NULL,
  [status] int  NULL
)
GO

ALTER TABLE [dbo].[tm_menu] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tm_menu
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_menu] ON
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'1', N'Beranda', N'#', N'dripicons-home', N'0', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'2', N'Klaim', N'#', N'dripicons-to-do', N'0', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'3', N'Pengajuan Klaim', N'klaim', NULL, N'2', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'4', N'Klaim Mekanisme 2', N'klaim_peralihan', NULL, N'2', N'0')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'5', N'Main Dashboard', N'beranda/index', NULL, N'1', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'6', N'Klaim Per Mitra Asuransi', N'beranda/permitra', NULL, N'1', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'7', N'Klaim Per Cabang', N'beranda/percabang', NULL, N'1', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'8', N'Penyebab Klaim Ditolak', N'beranda/klaimditolak', NULL, N'1', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'9', N'Dokumen Belum Lengkap', N'beranda/dokbelumlengkap', NULL, N'1', N'1')
GO

INSERT INTO [dbo].[tm_menu] ([id_menu], [nama_menu], [link], [icon], [kat_menu], [status]) VALUES (N'10', N'Data Klaim New & Existing', N'beranda/newexist', NULL, N'1', N'1')
GO

SET IDENTITY_INSERT [dbo].[tm_menu] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_pengajuan_klaim
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_pengajuan_klaim]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_pengajuan_klaim]
GO

CREATE TABLE [dbo].[tm_pengajuan_klaim] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [norek] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_akad] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_cif] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [nama_debitur] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [tanggal_lahir] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_ktp] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_sertifikat_polis] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [kantor_cabang] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [nama_perusahaan_asuransi] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [jenis_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [fasilitas_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [tanggal_kejadian] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [jangka_waktu_mulai_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [jangka_waktu_akhir_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [tenor_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [premi_asuransi] numeric(18)  NULL,
  [nilai_plafond_kredit] numeric(18)  NULL,
  [nilai_pokok_pengajuan_klaim] numeric(18)  NULL,
  [nilai_bunga_pengajuan_klaim] numeric(18)  NULL,
  [nilai_total_pengajuan_klaim] numeric(18)  NULL,
  [nilai_persetujuan_klaim] numeric(18)  NULL,
  [nilai_klaim_yg_dbayar] numeric(18)  NULL,
  [tanggal_pembayaran_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [bukti_pembayaran] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [status_klaim] tinyint  NOT NULL,
  [create_by] int  NULL,
  [create_date] datetime  NULL,
  [update_by] int  NULL,
  [update_date] datetime2(7)  NULL,
  [p_klaim_1] int  NULL,
  [p_klaim_2] int  NULL,
  [alamat] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sebab_klaim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [no_ld] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tm_pengajuan_klaim] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'0: pengajuan, 1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kurang dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tm_pengajuan_klaim',
'COLUMN', N'status_klaim'
GO


-- ----------------------------
-- Records of tm_pengajuan_klaim
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_pengajuan_klaim] ON
GO

INSERT INTO [dbo].[tm_pengajuan_klaim] ([id], [norek], [no_klaim], [no_akad], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [no_sertifikat_polis], [kantor_cabang], [nama_perusahaan_asuransi], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [alamat], [sebab_klaim], [no_ld]) VALUES (N'1', N'12345', N'1675239892_00001', N'123456', N'12345464', N'AGUS DAYAT ISNAENI HIDAYATULLOH NUR WAHID SULISTIOWATI', N'2004-09-13', N'32018281782178', N'1234564', N'KC. Kutoarjo', N'1', N'1', N'TEST', N'2023-02-01', N'2023-02-01', N'2023-02-03', N'180', N'500000000', N'500000000', N'500000000', NULL, N'500000000', NULL, NULL, NULL, NULL, NULL, N'4', N'1', N'2023-02-01 17:26:51.000', NULL, NULL, NULL, NULL, N'Jl. Majalaya', N'syaraf kejepit', NULL)
GO

SET IDENTITY_INSERT [dbo].[tm_pengajuan_klaim] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_pengajuan_klaim_peralihan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_pengajuan_klaim_peralihan]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_pengajuan_klaim_peralihan]
GO

CREATE TABLE [dbo].[tm_pengajuan_klaim_peralihan] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [norek] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_cif] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [nama_debitur] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [tanggal_lahir] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [no_ktp] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [kantor_cabang] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [jenis_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [fasilitas_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [tanggal_kejadian] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [jangka_waktu_mulai_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [jangka_waktu_akhir_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [tenor_kredit] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [premi_asuransi] numeric(18)  NULL,
  [nilai_plafond_kredit] numeric(18)  NULL,
  [nilai_pokok_pengajuan_klaim] numeric(18)  NULL,
  [nilai_bunga_pengajuan_klaim] numeric(18)  NULL,
  [nilai_total_pengajuan_klaim] numeric(18)  NULL,
  [nilai_persetujuan_klaim] numeric(18)  NULL,
  [nilai_klaim_yg_dbayar] numeric(18)  NULL,
  [tanggal_pembayaran_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [bukti_pembayaran] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [status_klaim] tinyint  NOT NULL,
  [create_by] int  NULL,
  [create_date] datetime  NULL,
  [update_by] int  NULL,
  [update_date] datetime2(7)  NULL,
  [p_klaim_1] int  NULL,
  [p_klaim_2] int  NULL,
  [dokumen_klaim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sk_meninggal] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [fc_surat_tagihan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [slik_ojk] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sk_terjadi_kebakaran] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [syarat_lain] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [alamat] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sebab_klaim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [dokumen_tambahan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_ktp] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_kk] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_akad] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_norek] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_realisasi] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_skpengangkatan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_surat_pengajuan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_meninggal] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_macet] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_kebakaran] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [file_slik] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tm_pengajuan_klaim_peralihan] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'0: pengajuan, 1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kurang dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tm_pengajuan_klaim_peralihan',
'COLUMN', N'status_klaim'
GO

EXEC sp_addextendedproperty
'MS_Description', N'kelurahan, polisi atau rumahsakit',
'SCHEMA', N'dbo',
'TABLE', N'tm_pengajuan_klaim_peralihan',
'COLUMN', N'sk_meninggal'
GO


-- ----------------------------
-- Records of tm_pengajuan_klaim_peralihan
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_pengajuan_klaim_peralihan] ON
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'1', N'7201902201', N'1665992600_00001', N'88341613', N'SARWAN NAIM', N'1996-08-03', N'7405203108970001', N'KC KENDARI A SILONDAE 2', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DESA AMASARA, KEL. AMASARA, KEC. BAITO, KOTA/KAB KAB. KONAWE SELATAN , KODE POS 93883', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'2', N'7202400927', N'1665992600_00001', N'88370113', N'MORIANTO', N'2000-05-29', N'7402032905000002', N'KC KENDARI A SILONDAE 2', N'2', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KASUMEWUHO, KEL. KASUMEWUHO, KEC. WAWOTOBI, KOTA/KAB KAB. KONAWE, KODE POS 93461', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'3', N'7202801767', N'1665992600_00001', N'88401471', N'ARMIN', N'1987-06-01', N'7203270106870001', N'KC KENDARI A SILONDAE 2', N'3', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ASMIL KIPAN A-YONIF 711/RKS, KEL. MEKAR BARU, KEC. BANAWA TENGAH, KOTA/KAB KAB. DONGGALA , KODE POS 94351', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'4', N'7206618536', N'1665992600_00001', N'88752290', N'NOVIA KHARISMA PUTRI', N'1999-11-29', N'7471046911990001', N'KC KENDARI A SILONDAE 2', N'3', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. BANTENG, KEL. RAHANDOUNA, KEC. POASIA, KOTA/KAB KOTA KENDARI , KODE POS 93232', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'5', N'7206660966', N'1665992600_00001', N'88755419', N'IQRAM DAHLAN', N'1998-09-08', N'7317191303850001', N'KC KENDARI A SILONDAE 2', N'2', N'TEST', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'148675000', NULL, N'148675000', NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 15:12:52.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KIJANG BTN GRAHA ASYURIA NO 5, KEL. RAHANDOUNA, KEC. POASIA, KOTA/KAB KOTA KENDARI , KODE POS 93232', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'6', N'7207052528', N'1665992600_00001', N'88781202', N'DARWIN', N'1986-08-30', N'1371043008860007', N'KC KENDARI A SILONDAE 2', N'1', N'FLPP', NULL, N'2022-08-29', N'2032-08-29', N'120', N'8177125', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ASMIL YONIF/133 YS JL DR. PROF HAMKA, KEL. AIR TAWAR TIMUR, KEC. PADANG UTARA, KOTA/KAB KOTA PADANG, KODE POS 25132', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'7', N'8114007771', N'1665992600_00001', N'61477826', N'WAHYUDIN MADIL', N'1989-01-08', N'7471080801890001', N'KC KENDARI A SILONDAE 2', N'1', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BTN BATUMARUPA BLOK A.7, KEL. RAHANDOUNA, KEC. POASIA, KOTA/KAB KOTA KENDARI , KODE POS 93232', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'8', N'7203109851', N'1665992600_00001', N'82234309', N'RIZAL RIANTO', N'1996-04-17', N'7409033112970001', N'KC KENDARI MT HARYONO', N'3', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DESA PARIAMA, KEL. PARIAMA, KEC. LANGGIKIMA, KOTA/KAB KAB. KONAWE UTARA, KODE POS 93351', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'9', N'1052157656', N'1665992600_00001', N'51540274', N'YUDA KURNIAWAN', N'1996-09-04', N'6207020409960002', N'KC KUALA SIMPANG', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN BUKIT BUNDAR, KEL. KEBUN TANAH TERBAN, KEC. KARANG BARU, KOTA/KAB KAB. ACEH TAMIANG, KODE POS 24476', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'10', N'1050356767', N'1665992600_00001', N'54146531', N'LAMBOK APRIJON', N'1999-06-26', N'1102042606990001', N'KC KUTACANE', N'3', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'TITI PANJANG, KEL. PERAPAT TITI PANJKANG, KEC. BABUSSALAM, KOTA/KAB KAB. ACEH TENGGARA , KODE POS 24651', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'11', N'1050353229', N'1665992600_00001', N'51476688', N'CANDRA JON EFFENDI', N'1979-11-12', N'1102081211790002', N'KC KUTACANE', N'2', N'FLPP', NULL, N'2022-08-10', N'2035-08-10', N'156', N'10188750', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KANDANG MBELANG MANDIRI, KEL. KANDANG MBELANG MANDIRI, KEC. LAWE BULAN, KOTA/KAB KAB. ACEH TENGGARA , KODE POS 24651', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'12', N'1052690532', N'1665992600_00001', N'53924173', N'HENDRI SAPUTRA', N'1985-11-24', N'1108042411850001', N'KC LHOKSEUMAWE MERDEKA 3', N'1', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'11921250', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL MALIKUSSALEH, KEL. KOTA LHOKSUKON, KEC. LHOKSUKON, KOTA/KAB KAB. ACEH UTARA , KODE POS 24372', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'13', N'1046525759', N'1665992600_00001', N'53134825', N'AGUS MUHARRAMUDDIN', N'1988-08-14', N'1111161408860001', N'KC LHOKSEUMAWE MERDEKA 3', N'1', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'11921250', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN H. BANSON, KEL. TEUPIN REUDEUP, KEC. PEUSANGAN SELATAN, KOTA/KAB KAB. ACEH JEUMPA/BIREUEN, KODE POS 24267', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'14', N'1173335034', N'1665992600_00001', N'64639125', N'TITIN RAHMAWATI', N'1992-02-01', N'1173044102920001', N'KC LHOKSEUMAWE MERDEKA 3', N'2', N'FLPP', NULL, N'2022-08-10', N'2035-08-10', N'156', N'8580000', N'120000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN C, KEL. BATUPHAT TIMUR, KEC. MUARA SATU, KOTA/KAB KOTA LHOKSEUMAWE , KODE POS 24353', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'15', N'7199005353', N'1665992600_00001', N'88144161', N'DEFRI WAHYUDIN JH', N'1994-12-28', N'1605102812940001', N'KC LUBUK LINGGAU', N'3', N'FLPP', NULL, N'2022-08-05', N'2032-08-05', N'120', N'7859500', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN I , KEL. SUKAMANA, KEC. STL ULU TERAWAS, KOTA/KAB KAB. MUSI RAWAS , KODE POS 31652', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'16', N'8426957540', N'1665992600_00001', N'62611728', N'FIRMANSYAH', N'1991-06-21', N'1673052106910001', N'KC LUBUK LINGGAU', N'3', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'11954250', N'144900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LRG AN NUR RT 001 , KEL. PASAR PERMIRI, KEC. KEC LLG BARAT II, KOTA/KAB KOTA LUBUKLINGGAU , KODE POS 31613', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'17', N'9766104270', N'1665992600_00001', N'64356536', N'YOHAN DIKA PUTRA', N'1987-04-28', N'1673052804870001', N'KC LUBUK LINGGAU', N'2', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11954250', N'144900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KENANGA I NO 63 RT 011, KEL. KENANGA, KEC. LUBUKLINGGAU UTARA II, KOTA/KAB KOTA LUBUKLINGGAU , KODE POS 31619', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'18', N'7201379584', N'1665992600_00001', N'88307401', N'EMI IMELDA', N'1977-12-12', N'3171075212770004', N'KC MADIUN KARTOHARJO', N'1', N'FLPP', NULL, N'2022-08-12', N'2029-08-12', N'84', N'5409250', N'140500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KEBON SIRIH BARAT GG XIV, KEL. KEBON SIRIH, KEC. MENTENG, KOTA/KAB WIL. KOTA JAKARTA PUSAT , KODE POS 10250', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'19', N'7201472797', N'1665992600_00001', N'88313190', N'DEWI ANGGUN RETNONINGRUM', N'1990-12-15', N'3321105512900001', N'KC MADIUN KARTOHARJO', N'2', N'FLPP', NULL, N'2022-08-12', N'2032-08-12', N'120', N'7810000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SUMBERBENING, KEL. SUMBERBENING, KEC. BALEREJO, KOTA/KAB KAB. MADIUN , KODE POS 63152', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'20', N'7200855412', N'1665992600_00001', N'78150540', N'RYCHE DESTYANI YUNIAR', N'1991-12-13', N'3525135312910003', N'KC MADIUN KARTOHARJO', N'1', N'FLPP', NULL, N'2022-08-08', N'2035-08-08', N'156', N'10153000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BOTENG, KEL. BOTENG, KEC. MENGANTI, KOTA/KAB KAB. GRESIK, KODE POS 61174', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'21', N'7201318054', N'1665992600_00001', N'88303273', N'IRMA PUJI HANDAYANI', N'1998-08-31', N'3510167108980002', N'KC MADIUN KARTOHARJO', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'WADENG, KEL. SIDOMULYO, KEC. SAWAHAN, KOTA/KAB KAB. MADIUN , KODE POS 63162', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'22', N'7201622594', N'1665992600_00001', N'88322961', N'ANGGITA KRISNA PUTRI', N'2001-10-10', N'3507135010010006', N'KC MADIUN KARTOHARJO', N'2', N'FLPP', NULL, N'2022-08-26', N'2033-08-26', N'132', N'8470000', N'140000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN DEPOK, KEL. KARANGANYAR, KEC. GANDUSARI, KOTA/KAB KAB. TRENGGALEK , KODE POS 66372', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'23', N'7201403787', N'1665992600_00001', N'88308978', N'OKKI JUNIARKO', N'1998-06-04', N'3519100406980001', N'KC MADIUN KARTOHARJO', N'2', N'FLPP', NULL, N'2022-08-26', N'2036-08-26', N'168', N'10934000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KEDUNGREJO, KEL. KEDUNGREJO, KEC. BALEREJO, KOTA/KAB KAB. MADIUN , KODE POS 63152', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'24', N'7201153306', N'1665992600_00001', N'88292381', N'DENI NUR FAUZI', N'1996-03-24', N'3507032403960001', N'KC MADIUN KARTOHARJO', N'2', N'FLPP', NULL, N'2022-08-29', N'2032-08-29', N'120', N'6682500', N'121500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KRAJAN, KEL. BANTUR, KEC. BANTUR, KOTA/KAB KAB. MALANG , KODE POS 65179', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'25', N'7205464918', N'1665992600_00001', N'50249542', N'ABD HASYIA RAHMAN', N'1994-05-26', N'7308022605940001', N'KC MAKASSAR 2', N'3', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'12251250', N'148500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL TUN ABD RAZAK PERM BUMI AROEPALA, KEL. PACCINONGANG, KEC. SOMBA OPU, KOTA/KAB KAB. GOWA , KODE POS 92113', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'26', N'7202824247', N'1665992600_00001', N'88401676', N'RAY JUSUF VENTJE SAHETAPY', N'1991-08-07', N'7106030708910003', N'KC MANADO MANTOS', N'3', N'FLPP', NULL, N'2022-08-08', N'2037-08-08', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'AIRMADIDI BAWAH AIRMADIDI, KEL. AIRMADIDI BAWAH, KEC. AIRMADIDI, KOTA/KAB KAB. MINAHASA UTARA, KODE POS 95371', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'27', N'7202624426', N'1665992600_00001', N'88389348', N'MOHAMAD SANDITIAS PULUHULAWA', N'1992-11-14', N'7501041411920001', N'KC MANADO MANTOS', N'3', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN V, KEL. SINGKIL SATU, KEC. SINGKIL, KOTA/KAB KOTA MENADO , KODE POS 95234', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'28', N'7203527173', N'1665992600_00001', N'88451897', N'MUHHAMMAD KASIM', N'1989-04-04', N'7172040405890002', N'KC MANADO MANTOS', N'3', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN IV, KEL. WINENET SATU, KEC. AERTEMBAGA, KOTA/KAB KOTA BITUNG , KODE POS 95525', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'29', N'7205537176', N'1665992600_00001', N'83819104', N'SULASTRI DAUD', N'1994-02-14', N'7172065402940001', N'KC MANADO MANTOS', N'1', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN I, KEL. WANGURER, KEC. GIRIAN , KOTA/KAB KOTA BITUNG , KODE POS 95543', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'30', N'7205491095', N'1665992600_00001', N'88676212', N'RELYA F SIDANGOLI', N'1989-07-25', N'7109016507890002', N'KC MANADO MANTOS', N'1', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN I, KEL. KAREKO, KEC. LEMBEH UTARA, KOTA/KAB KOTA BITUNG , KODE POS 95558', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'31', N'7202625058', N'1665992600_00001', N'88389382', N'JOKSAN GERI KALANGI', N'2002-01-20', N'7107022001020001', N'KC MANADO MANTOS', N'1', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JAGA IV, KEL. MAKALU, KEC. PUSOMAEN, KOTA/KAB KAB. MINAHASA TENGGARA, KODE POS 95697', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'32', N'7205718391', N'1665992600_00001', N'88691436', N'GREGORIUS TOAR PALIT', N'2000-07-23', N'7102122307000001', N'KC MANADO MANTOS', N'2', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN I, KEL. KAWANGKOAN UTARA, KEC. MINAHASA, KOTA/KAB KAB. MINAHASA , KODE POS 95692', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'33', N'7205793156', N'1665992600_00001', N'88696466', N'JUAN MARCELLLYNO ARNESTO DELEI', N'2000-12-18', N'7106031812000001', N'KC MANADO MANTOS', N'2', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'RAP-RAP, KEL. RAP-RAP, KEC. AIRMADIDI, KOTA/KAB KAB. MINAHASA UTARA, KODE POS 95371', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'34', N'7203523388', N'1665992600_00001', N'88451628', N'JUHARI', N'1989-08-29', N'1271122808890002', N'KC MANADO MANTOS', N'2', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'WANENET , KEL. WINENET SATU, KEC. WANENET, KOTA/KAB KOTA BITUNG , KODE POS 95525', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'35', N'7205470969', N'1665992600_00001', N'88674854', N'NOPRI POMANTO', N'1987-02-16', N'7504061602870002', N'KC MANADO MANTOS', N'2', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN I, KEL. MANEMBO-NEMBO ATAS, KEC. MATUARI, KOTA/KAB KOTA BITUNG , KODE POS 95545', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'36', N'7178748615', N'1665992600_00001', N'51406499', N'VIDYIA TRI UTAMI', N'1997-01-20', N'7171056001970001', N'KC MANADO MANTOS', N'3', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN III, KEL. BANJER, KEC. TIKALA, KOTA/KAB KOTA MENADO , KODE POS 95125', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'37', N'7203661074', N'1665992600_00001', N'88460663', N'SHINTIYA MADINA', N'1995-03-20', N'7101096003950212', N'KC MANADO MANTOS', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN I , KEL. IBOLIAN, KEC. DUMPGA TENGAH, KOTA/KAB KAB. BOLAANG MONGONDOW , KODE POS 95773', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'38', N'7206824411', N'1665992600_00001', N'88765870', N'JACKY RUDY MANGIL ALENG', N'1968-08-07', N'7171060708680001', N'KC MANADO MANTOS', N'3', N'FLPP', NULL, N'2022-08-26', N'2028-08-26', N'72', N'4906275', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN IV NO. 20, KEL. TITIWUNGAN UTARA, KEC. SARIO, KOTA/KAB KOTA MENADO , KODE POS 95113', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'39', N'7206468585', N'1665992600_00001', N'88741976', N'FRISKA PATRICIA KUMENDONG', N'1994-02-13', N'7171075302940001', N'KC MANADO MANTOS', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN III, KEL. KAROMBASAN UTARA, KEC. WANEA, KOTA/KAB KOTA MENADO , KODE POS 95117', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'40', N'7206553167', N'1665992600_00001', N'88747914', N'RISKI ANDRE TIANTO', N'1999-08-26', N'7171022608990003', N'KC MANADO MANTOS', N'1', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN I, KEL. BITUNG KARANGRIA, KEC. TUMINTING, KOTA/KAB KOTA MENADO , KODE POS 95237', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'41', N'7206102726', N'1665992600_00001', N'88717389', N'MUHAMMAD FAJRIN ABASI', N'1997-03-24', N'7204072403970002', N'KC MANADO MANTOS', N'1', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN I, KEL. TERNATE BARU, KEC. SINGKIL, KOTA/KAB KOTA MENADO , KODE POS 95231', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'42', N'7203088398', N'1665992600_00001', N'88420582', N'NI KOMANG ARINI', N'1990-08-18', N'5271036004950003', N'KC MATARAM PEJANGGIK 2', N'1', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'13117500', N'159000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. AA GDE NGURAH PANARAGA TIMUR , KEL. CAKRANEGARA SELATAN, KEC. CAKRANEGARA, KOTA/KAB KOTA MATARAM , KODE POS 83232', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'43', N'7203045993', N'1665992600_00001', N'88418746', N'ARIF SUKMA APRIANSYAH', N'1999-04-05', N'5271030504950006', N'KC MATARAM PEJANGGIK 2', N'2', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DJL PERMAS INDAH BLOK A, KEL. LABUAPI UTARA, KEC. LABUAPI, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83361', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'44', N'7203041181', N'1665992600_00001', N'88418402', N'TASLIMA SEPTININGTIAS', N'1997-07-07', N'5201094707970001', N'KC MATARAM PEJANGGIK 2', N'2', N'FLPP', NULL, N'2022-08-02', N'2037-08-02', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PESONA RINJANI NO 76 RT 006, KEL. JATISELA, KEC. GUNUNGSARI, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83351', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'45', N'1059754994', N'1665992600_00001', N'50776274', N'HAPSARI SUCI ARYANI', N'1997-01-09', N'5201084901970003', N'KC MATARAM PEJANGGIK 2', N'2', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'13158750', N'159500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BTN GRIYA PERAMPUAN ASRI BLOK S NO. 10 , KEL. KARANG BONGKOT, KEC. LABUAPI, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83361', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'46', N'7201941727', N'1665992600_00001', N'88344145', N'HILWA BAFADAL', N'1998-11-06', N'5203074611980005', N'KC MATARAM PEJANGGIK 2', N'2', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'OTAK PANCOR UTARA, KEL. LENDANG NANGKA UTARA, KEC. MASBAGIK, KOTA/KAB KAB. LOMBOK TIMUR , KODE POS 83661', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'47', N'7203384364', N'1665992600_00001', N'52574891', N'ARIF RAHMAN', N'1983-04-07', N'5206040704830002', N'KC MATARAM PEJANGGIK 2', N'3', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'10560000', N'128000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL MERDEKA 17, KEL. PAGESANGAN, KEC. MATARAM, KOTA/KAB KOTA MATARAM , KODE POS 83127', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'48', N'7205445808', N'1665992600_00001', N'88672979', N'YULI YANTI DAARIS', N'1992-01-01', N'5206094101921002', N'KC MATARAM PEJANGGIK 2', N'3', N'FLPP', NULL, N'2022-08-08', N'2037-08-08', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JALAN LAHAMI, KEL. SANDUE, KEC. SANGGAR, KOTA/KAB KAB. BIMA , KODE POS 84191', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'49', N'7201401377', N'1665992600_00001', N'88308621', N'SARIPATUL AENI', N'2000-04-27', N'5203086704000006', N'KC MATARAM PEJANGGIK 2', N'3', N'FLPP', NULL, N'2022-08-09', N'2037-08-09', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'GEGURUN, POHGADING TIMUR, KEL. POHGADING TIMUR, KEC. PRINGGABAYA, KOTA/KAB KAB. LOMBOK TIMUR , KODE POS 83654', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'50', N'7136764866', N'1665992600_00001', N'83282887', N'SRI RATNA DURRI AMD KEP', N'1983-01-09', N'5201134901830001', N'KC MATARAM PEJANGGIK 2', N'3', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BELEKE, KEL. JEMBATAN KEMBAR, KEC. LEMBAR, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83364', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'51', N'7205477009', N'1665992600_00001', N'88675194', N'MUHAMMAD ZULFIQAR MZ', N'1995-05-05', N'5272050505950002', N'KC MATARAM PEJANGGIK 2', N'1', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SADIA I MPUNDA  BIMA, KEL. SADIA, KEC. MPUNDA, KOTA/KAB KOTA. BIMA, KODE POS 84112', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'52', N'7205697057', N'1665992600_00001', N'88688481', N'TAENI', N'1995-01-09', N'5201074107640037', N'KC MATARAM PEJANGGIK 2', N'1', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN SIUNG, KEL. BATU PUTIH, KEC. SEKOTONG, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83365', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'53', N'7206336573', N'1665992600_00001', N'50794006', N'HENYATI', N'1984-05-13', N'5205015305840003', N'KC MATARAM PEJANGGIK 2', N'1', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN OO BARAT, KEL. OO, KEC. DOMPU, KOTA/KAB KAB. DOMPU , KODE POS 84219', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'54', N'1018538535', N'1665992600_00001', N'50800192', N'Musmuliadin', N'1991-09-09', N'5201130909910002', N'KC MATARAM PEJANGGIK 2', N'1', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'GRANADA, KEL. JEMBATAN KEMBAR, KEC. LEMBAR, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83364', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'55', N'7198175646', N'1665992600_00001', N'88086370', N'ADRIANDA ANWAR', N'1990-06-05', N'1304020506900003', N'KC MATARAM PEJANGGIK 2', N'2', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL A YANI, KEL. SIGANDO, KEC. PADANG PANJANG TIMUR, KOTA/KAB KOTA PADANG PANJANG , KODE POS 27126', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'56', N'7205945819', N'1665992600_00001', N'88707191', N'DEDEN PRATAMA', N'1997-03-07', N'5206090703971002', N'KC MATARAM PEJANGGIK 2', N'3', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PIONG, KEL. PIONG, KEC. SANGGAR, KOTA/KAB KAB. BIMA , KODE POS 84191', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'57', N'7206511014', N'1665992600_00001', N'54539626', N'AZMUL FIKRI SODIKIN', N'1995-01-08', N'5271010801950003', N'KC MATARAM PEJANGGIK 2', N'3', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PEJERUK DESA KEL.PEJERUK KEC.AMPENAN, KEL. PEJERUK, KEC. AMPENAN, KOTA/KAB KOTA MATARAM , KODE POS 83113', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'58', N'7205858134', N'1665992600_00001', N'88701115', N'BAIQ MUTMAINNAH', N'1994-11-29', N'5201026911940001', N'KC MATARAM PEJANGGIK 2', N'2', N'FLPP', NULL, N'2022-08-24', N'2037-08-24', N'180', N'13158750', N'159500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KEBON ORONG RT.001 , KEL. DASAN BARU, KEC. KEDIRI, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83362', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'59', N'7206959066', N'1665992600_00001', N'88774625', N'HUSNIATI', N'1999-06-10', N'5202115006990001', N'KC MATARAM PEJANGGIK 2', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'13167000', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BALE LUAH KEL. DAREK , KEL. DAREK, KEC. PRAYA BARAT DAYA, KOTA/KAB KAB. LOMBOK TENGAH , KODE POS 83572', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'60', N'7206891887', N'1665992600_00001', N'88770432', N'HANGGA RIZKY FEFTYANTO', N'1992-07-30', N'5201083007920002', N'KC MATARAM PEJANGGIK 2', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'13158750', N'159500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. GUNUNG BATUR MO. 17 BTN PENGSONG, KEL. PERAMPUAN, KEC. LABUAPI, KOTA/KAB KAB. LOMBOK BARAT , KODE POS 83361', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'61', N'7206913333', N'1665992600_00001', N'88771790', N'KUSPIADI', N'1978-07-05', N'5203170507780005', N'KC MATARAM PEJANGGIK 2', N'3', N'FLPP', NULL, N'2022-08-30', N'2037-03-30', N'175', N'12801250', N'159600000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGKUNGAN GERES DAYA, KEL. GERES, KEC. LABUHAN HAJI, KOTA/KAB KAB. LOMBOK TIMUR , KODE POS 83675', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'62', N'3935828960', N'1665992600_00001', N'60894417', N'YASRIL ARMAN', N'1990-04-04', N'1371110404900015', N'KC PADANG KIS MANGUNSARKORO', N'2', N'FLPP', NULL, N'2022-08-04', N'2032-08-04', N'120', N'7315000', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'AIR PACAH, KEL. AIR PACAH, KEC. KOTO TANGAH, KOTA/KAB KOTA PADANG, KODE POS 25176', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'63', N'7198989678', N'1665992600_00001', N'88143195', N'ANDRE PRATAMA', N'1988-11-29', N'1371062911880003', N'KC PADANG KIS MANGUNSARKORO', N'3', N'FLPP', NULL, N'2022-08-09', N'2037-08-09', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'GURUN LAWEH NO. 35, KEL. GURUN LAWEH NAN XX, KEC. LUBUK BEGALUNG, KOTA/KAB KOTA PADANG, KODE POS 25221', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'64', N'7203090018', N'1665992600_00001', N'88421647', N'AGUSTINI', N'1989-08-17', N'1671135708880008', N'KC PALEMBANG SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-04', N'2037-08-04', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KI MAROGAN LR WIJAYA RT/RW : 37/007, KEL. KEMANG AGUNG, KEC. KERTAPATI, KOTA/KAB KOTA PALEMBANG , KODE POS 30258', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'65', N'7205975084', N'1665992600_00001', N'88709027', N'ANDRIE HANDOKO', N'1995-04-28', N'1671132804950016', N'KC PALEMBANG SUDIRMAN', N'1', N'FLPP', NULL, N'2022-08-15', N'2032-08-15', N'120', N'7863625', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL ABIKUSNO CS , KEL. KEMANG AGUNG, KEC. KERTAPATI, KOTA/KAB KOTA PALEMBANG , KODE POS 30258', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'66', N'7205957868', N'1665992600_00001', N'50339679', N'ANDI SAPUTRA', N'1992-08-18', N'1607061808920001', N'KC PALEMBANG SUDIRMAN', N'1', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SEI HITAM LR FAMILI I NO 187, KEL. SIRING AGUNG, KEC. ILIR BARAT I, KOTA/KAB KOTA PALEMBANG , KODE POS 30138', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'67', N'7205754517', N'1665992600_00001', N'88693884', N'ANGGA', N'1995-11-22', N'1603212211970001', N'KC PALEMBANG SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN I, KEL. MENANTI, KEC. KELEKAR, KOTA/KAB KOTA PALEMBANG , KODE POS 30171', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'68', N'7205953951', N'1665992600_00001', N'88695969', N'RIAN SAPUTRA', N'1987-09-10', N'1671031009870011', N'KC PALEMBANG SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-12', N'2032-08-12', N'120', N'7863625', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LR SENTOSA JAYA RT 18 RW 07, KEL. TANGGA TAKAT, KEC. SEBERANG ULU II, KOTA/KAB KOTA PALEMBANG , KODE POS 30264', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'69', N'7206029484', N'1665992600_00001', N'88712344', N'ACHMAD ANNURI', N'1994-11-10', N'1606121011940001', N'KC PALEMBANG SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-16', N'2032-08-16', N'120', N'7863625', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PALEMBANG JAMBI RT. 2 RW. 1, KEL. PENINGGALAN, KEC. TUNGKAL JAYA, KOTA/KAB KAB. MUSI BANYUASIN , KODE POS 30756', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'70', N'7207028368', N'1665992600_00001', N'88766212', N'RUSAN', N'1982-12-17', N'1606021712820002', N'KC PALEMBANG SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-29', N'2032-08-29', N'120', N'7863625', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DSN III TELUK, KEL. TELUK, KEC. LAIS, KOTA/KAB KAB. MUSI BANYUASIN , KODE POS 30711', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'71', N'7207031091', N'1665992600_00001', N'88779784', N'HERLY SEPTIADI', N'1985-09-20', N'1607102009850008', N'KC PALEMBANG SUDIRMAN', N'1', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SUKARELA LR SWADAYA II, KEL. SUKARAMI, KEC. SUKARAMI, KOTA/KAB KOTA PALEMBANG , KODE POS 30152', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'72', N'7207013875', N'1665992600_00001', N'74861118', N'DEVI RAMADONA', N'1994-03-04', N'1671134403940005', N'KC PALEMBANG SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL ABIKUSNO COKRO SUYOSO, KEL. KEMANG AGUNG, KEC. KERTAPATI, KOTA/KAB KOTA PALEMBANG , KODE POS 30258', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'73', N'7207007697', N'1665992600_00001', N'88778345', N'HERIYANTO', N'1989-03-15', N'1671131503890008', N'KC PALEMBANG SUDIRMAN', N'1', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KERAMASAN, KEL. KERAMASAN, KEC. KERTAPATI, KOTA/KAB KOTA PALEMBANG , KODE POS 30259', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'74', N'7201804757', N'1665992600_00001', N'88335187', N'NASRUDIN', N'1976-04-27', N'1606072704760002', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-02', N'2037-08-02', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KEL. SUNGAI LILIN, KEL. SUNGAI LILIN, KEC. SUNGAI LILIN, KOTA/KAB KAB. MUSI BANYUASIN , KODE POS 30755', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'75', N'7202750855', N'1665992600_00001', N'88398094', N'ALAMSYAH', N'1983-05-14', N'1602141405830001', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-02', N'2037-08-02', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN MARGO MULYO , KEL. BUKIT BATU, KEC. OGAN KOMERING ILIR , KOTA/KAB KAB. BANYUASIN, KODE POS 30656', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'76', N'7206148572', N'1665992600_00001', N'88720491', N'EKO SUROSO', N'1991-07-17', N'1603031707910004', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-16', N'2032-08-15', N'120', N'7837500', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN II RT 003 RW 002, KEL. AIR LIMAU, KEC. RAMBANG DANGKU, KOTA/KAB KAB. LEMATANG ILIR OGAN TENGAH (MUARA ENIM) , KODE POS 31172', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'77', N'7206265129', N'1665992600_00001', N'88728203', N'ADITYA RIZKI PRATAMA', N'1996-04-01', N'1671080104960008', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PEGAYUT II NO 3 RT 31 RW 12, KEL. SIALANG, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'78', N'7206141829', N'1665992600_00001', N'88720021', N'NURHANI', N'1991-02-08', N'1607200602910002', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-16', N'2037-08-15', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL RATNA LR ATOM NO 50 RT 23 RW 10, KEL. 29 ILIR, KEC. ILIR BARAT II, KOTA/KAB KOTA PALEMBANG , KODE POS 30143', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'79', N'7206671135', N'1665992600_00001', N'88756140', N'INDAH DESTALYA', N'2001-12-25', N'1671066512010008', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LR SATRIA NO 141 , KEL. 2 ILIR , KEC. ILIR TIMUR II, KOTA/KAB KOTA PALEMBANG , KODE POS 30118', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'80', N'7190422199', N'1665992600_00001', N'87447840', N'NABILA', N'2002-10-27', N'1671086710000006', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL LEBAK MURNI , KEL. SAKO, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'81', N'7206674916', N'1665992600_00001', N'88756397', N'FEBRI TRISNAWATI', N'1997-02-14', N'1608105402970001', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN IV, KEL. CAMPANG TIGA ILIR , KEC. CEMPAKA , KOTA/KAB KAB. OGAN KOMERING ULU , KODE POS 32184', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'82', N'7206840708', N'1665992600_00001', N'88766997', N'PUTRI PURNAMA SARI', N'1992-09-23', N'1671036309920002', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL DI PANJAITAN NO 21 , KEL. TALANG JAMBE, KEC. SUKARAMI, KOTA/KAB KOTA PALEMBANG , KODE POS 30155', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'83', N'7206216993', N'1665992600_00001', N'88725074', N'TIARA SANDRA FRANSISKA', N'2000-01-12', N'1671085201000010', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KENTEN SUKAMAJU , KEL. SUKAMJU, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30164', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'84', N'7206940926', N'1665992600_00001', N'88773664', N'WULAN ROSVITA DEWI', N'1996-10-21', N'1671066110960005', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. PENDAWA LRG. NAKULA NO. 24, KEL. 2 ILIR , KEC. ILIR TIMUR II , KOTA/KAB KOTA PALEMBANG , KODE POS 30118', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'85', N'7205662318', N'1665992600_00001', N'80420910', N'NADIA ALFADISA', N'1997-03-18', N'1609135803970001', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KOTA BATU , KEL. KOTA BATU , KEC. WARKUK RANAU SELATAN , KOTA/KAB KAB. OGAN KOMERING ULU , KODE POS 32275', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'86', N'7199964458', N'1665992600_00001', N'88211707', N'KGS A HAMID', N'1980-07-10', N'1671081007800006', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JLN MITRA IV NO 4853, KEL. SAKO , KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30164', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'87', N'7199968135', N'1665992600_00001', N'88211933', N'KGS M ARIEF', N'1986-05-15', N'1671081505860010', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL BATU BARA II NO 3496, KEL. SAKO , KEC. SAKO , KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'88', N'7206268756', N'1665992600_00001', N'88728449', N'DARMAWATI', N'1985-08-04', N'1607044408860001', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'RINGIN HARJO, KEL. RINGIN HARJO , KEC. SELAT PENUGUAN, KOTA/KAB KAB. BANYUASIN, KODE POS 30759', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'89', N'7205628775', N'1665992600_00001', N'88685494', N'JAKA SINAGA', N'2001-01-26', N'1671022601010008', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. TEMBOK BARU LRG. KEL. 9-10 ULU , KEL. SEMBILAN SEPULUH ULU , KEC. JAKABARING , KOTA/KAB KOTA PALEMBANG , KODE POS 30251', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'90', N'1207226461', N'1665992600_00001', N'64691475', N'M LUFFI AKBAR', N'1997-06-28', N'1671082806970006', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JLN ANGGREK III NO 127, KEL. SIALANG , KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'91', N'7205670453', N'1665992600_00001', N'88688333', N'DIO NOVANDRI PRATAMA', N'1998-11-04', N'1371110411980008', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'GRAHA AGUNG PERDANA PADANG , KEL. BATIPUH PANJANG , KEC. KOTA TANGAH, KOTA/KAB KOTA PADANG, KODE POS 25179', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'92', N'7200215502', N'1665992600_00001', N'88229210', N'BAGAS AJI PURNOMO', N'1999-10-27', N'1707042710990001', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KELURAHAN TES , KEL. TES , KEC. LEBONG SELATAN , KOTA/KAB KAB. LEBONG, KODE POS 39258', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'93', N'1230409911', N'1665992600_00001', N'60395528', N'SEPRI ANTI HASANA', N'1991-09-04', N'1671064409910007', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL YAYASAN II LRG TJ KATES NO 3680, KEL. 2 ILIR, KEC. ILIR TIMUR II, KOTA/KAB KOTA PALEMBANG , KODE POS 30118', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'94', N'7200005249', N'1665992600_00001', N'88214720', N'APRIYANTO', N'1983-01-02', N'1671060204830012', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SULTAN SYAHRIR GG, KEL. SAKO, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'95', N'9160177870', N'1665992600_00001', N'61670128', N'WINDA OKTAVIA', N'1997-10-29', N'1671096910970003', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KAPTEN A RIVAI GG SEPAKAT 24, KEL. 26 ILIR , KEC. BUKIT KECIL, KOTA/KAB KOTA PALEMBANG , KODE POS 30135', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'96', N'7205669781', N'1665992600_00001', N'88688288', N'HALIMAH', N'1985-10-12', N'1671095210850008', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JLN PIPA NO 811 , KEL. PIPA REJA , KEC. KEMUNING , KOTA/KAB KOTA PALEMBANG , KODE POS 30127', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'97', N'7206803945', N'1665992600_00001', N'61792601', N'FEBYOLA PUTRI NURSIYAH', N'1999-02-10', N'1671065002990006', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SEDUDUK PUTIH GG JAYA KARTA , KEL. SUKAMJU, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30164', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'98', N'7205671948', N'1665992600_00001', N'88688440', N'HERIYANTO', N'1986-09-21', N'1671092109860005', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SUPEREMAR LRG. MULIA , KEL. PIPA REJA, KEC. KEMUNING , KOTA/KAB KOTA PALEMBANG , KODE POS 30127', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'99', N'7206912809', N'1665992600_00001', N'88771773', N'SUGENG TURMUDI', N'1984-06-20', N'1671092006840009', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PIPA NO 811, KEL. PIPA REJA, KEC. KEMUNING, KOTA/KAB KOTA PALEMBANG , KODE POS 30127', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'100', N'7206919878', N'1665992600_00001', N'88772234', N'GUSTI HIDAYAT', N'2000-08-05', N'1671160508000001', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL LEMATANG II , KEL. LEBUNG GAJAH , KEC. SEMATANG BORANG , KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'101', N'7206910498', N'1665992600_00001', N'88771614', N'GABRIELL HIDAYATULLAH', N'1997-04-14', N'3273221404970011', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN IV, KEL. PULAU BERINGIN UTARA , KEC. PULAU BERINGIN , KOTA/KAB KAB. OGAN KOMERING ULU , KODE POS 32211', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'102', N'7199851491', N'1665992600_00001', N'88204352', N'MUHAMMAD YUSUF', N'1994-11-05', N'1671020511940007', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL YANI LRG H UMAR, KEL. 9-10 ULU, KEC. JAKABARING , KOTA/KAB KOTA PALEMBANG , KODE POS 30251', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'103', N'7206917789', N'1665992600_00001', N'60535648', N'TIKA AFRIDA', N'1996-04-25', N'1671086504960007', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PEGAYUT I NO 184, KEL. SIALANG, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'104', N'7206268675', N'1665992600_00001', N'88728448', N'ADEMAN MAKARIAN', N'1975-02-04', N'2171100402759003', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-26', N'2032-08-26', N'120', N'7863625', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL GERSIK AMAL MUSLIM 2, KEL. SEKIP JAYA , KEC. KEMUNING , KOTA/KAB KOTA PALEMBANG , KODE POS 30126', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'105', N'7207049478', N'1665992600_00001', N'88781027', N'AGUSTRI YENDI', N'1993-08-16', N'1611052108940001', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DESA BABATAN, KEL. BABATAN, KEC. LINTANG KANAN, KOTA/KAB KAB. EMPAT LAWANG, KODE POS 31592', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'106', N'7206375706', N'1665992600_00001', N'88735593', N'M BINTANG CENDIKIA', N'1998-08-07', N'1671070708980010', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LR. HIKMAH SUKAWINATAN RT/RW. 062/010 , KEL. SUKAJAYA , KEC. SUKARAMI , KOTA/KAB KOTA PALEMBANG , KODE POS 30151', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'107', N'7207193113', N'1665992600_00001', N'88790713', N'ALIM MUSTOFA', N'1996-06-30', N'1671083006960006', N'KC PALEMBANG SUKODADI', N'1', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PATRA I BLOK J7 NO 08 RT 067 RW 026, KEL. SAKO, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'108', N'7207031253', N'1665992600_00001', N'88779799', N'JUNAIDI', N'1991-06-26', N'1671082606910015', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL BRIGJEND HASAN KASIM NO 69 RT44 RW 6, KEL. BUKIT SANGKAL, KEC. KALIDONI, KOTA/KAB KOTA PALEMBANG , KODE POS 30114', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'109', N'7207319858', N'1665992600_00001', N'88798941', N'RORO KASIH', N'1986-12-15', N'1509045512860001', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-31', N'2037-08-15', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PANGKALAN UJUNG , KEL. SAKO , KEC. SAKO BARU , KOTA/KAB KOTA PALEMBANG , KODE POS 30165', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'110', N'7206205746', N'1665992600_00001', N'88724315', N'ILHAM ADI SATRIA', N'1998-05-28', N'1671082805980004', N'KC PALEMBANG SUKODADI', N'3', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'GRIYA SEJAHTERA BORANG KK 4 , KEL. SAKO, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'111', N'7206706664', N'1665992600_00001', N'88758450', N'MANISA NATA LOLA', N'1997-01-01', N'1671084101970009', N'KC PALEMBANG SUKODADI', N'2', N'FLPP', NULL, N'2022-08-31', N'2037-08-15', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KOMP GRIYA SEJAHTERA BLOK FF , KEL. SAKO, KEC. SAKO, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'112', N'7203179434', N'1665992600_00001', N'88427994', N'RINI FITRIANI', N'1981-12-02', N'7210014212810002', N'KC PALU M YAMIN', N'1', N'FLPP', NULL, N'2022-08-09', N'2037-08-09', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. LASOSO, KEL. LOLU, KEC. SIGI BIROMARU, KOTA/KAB KAB. SIGI , KODE POS 94364', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'113', N'7193558423', N'1665992600_00001', N'87694202', N'ADE SULISTYAWATI', N'1992-01-10', N'7271015901920002', N'KC PALU M YAMIN', N'1', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. HANG TUAH NO 20, KEL. TALISE, KEC. MANTIKOLORE, KOTA/KAB KOTA PALU , KODE POS 94116', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'114', N'7205992086', N'1665992600_00001', N'88708316', N'EKA MURTI', N'1993-12-20', N'7203276012930001', N'KC PALU M YAMIN', N'3', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BTN LAGARUTU, KEL. TALISE VALANGGUNI, KEC. MANTIKOLORE, KOTA/KAB KOTA PALU , KODE POS 94116', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'115', N'7206009114', N'1665992600_00001', N'88711268', N'AYU ERIKA PUTRI', N'1999-09-25', N'6474026509990002', N'KC PALU M YAMIN', N'2', N'FLPP', NULL, N'2022-08-18', N'2032-08-18', N'120', N'8177125', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. SLAMET RIYADI, KEL. BESUSU TIMUR, KEC. PALU TIMUR, KOTA/KAB KOTA PALU , KODE POS 94113', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'116', N'7206648915', N'1665992600_00001', N'88754524', N'ANDI TENRI NURRUL IZZAH ALIK', N'1992-08-06', N'7373054608920002', N'KC PALU M YAMIN', N'3', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. DUTA BUNTU, KEL. DURI KEPA, KEC. KEBON JERUK, KOTA/KAB WIL. KOTA JAKARTA BARAT , KODE POS 11510', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'117', N'7206928373', N'1665992600_00001', N'88772827', N'HENDRIK GUSTIAWAN', N'1990-07-23', N'7210092304900001', N'KC PALU M YAMIN', N'2', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. SEROJA, KEL. BALAROA, KEC. PALU BARAT, KOTA/KAB KOTA PALU , KODE POS 94226', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'118', N'7088539022', N'1665992600_00001', N'78285872', N'MUHAMMAD FAIRUS BAHARUDDIN PALOLOANG', N'1995-11-01', N'7271010111950004', N'KC PALU M YAMIN', N'1', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'12265688', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PERDOS UNTAD BLOK A II NO 15, KEL. TONDO, KEC. MANTIKOLORE, KOTA/KAB KOTA PALU , KODE POS 94117', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'119', N'7206739821', N'1665992600_00001', N'88760638', N'NI KETUT DINI WULANDARI', N'1998-08-19', N'5171045908980001', N'KC PANGKALAN BUN', N'1', N'FLPP', NULL, N'2022-08-24', N'2032-08-24', N'120', N'8595125', N'156275000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. PASIR PANJANG , KEL. PASIR PANJANG, KEC. ARUT SELATAN, KOTA/KAB KAB. KOTAWARINGIN BARAT , KODE POS 74181', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'120', N'7206941612', N'1665992600_00001', N'88773704', N'ELDI', N'1999-08-31', N'6201023108990002', N'KC PANGKALAN BUN', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'12892688', N'156275000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. PASANAH RT.27 RW.000, KEL. SIDOREJO, KEC. ARUT SELATAN, KOTA/KAB KAB. KOTAWARINGIN BARAT , KODE POS 74111', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'121', N'7205443357', N'1665992600_00001', N'88672776', N'ADI SUROSO', N'1997-11-29', N'3571022911970005', N'KC PASURUAN SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-05', N'2032-08-05', N'120', N'7810000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN TEGALAN, KEL. KARANGASEM, KEC. WONOREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67173', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'122', N'7203733547', N'1665992600_00001', N'88465601', N'ATIM SAFII', N'1992-11-10', N'3575011011920006', N'KC PASURUAN SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL GATOT SUBROTO, KEL. PETAHUNAN, KEC. GADINGREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67136', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'123', N'7203732141', N'1665992600_00001', N'88465468', N'EKO WIYONO', N'1982-01-30', N'3575033001830002', N'KC PASURUAN SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-05', N'2032-08-05', N'120', N'7810000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'GAYAMAN NO 43, KEL. KEBONAGUNG, KEC. PURWOREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67116', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'124', N'7203736465', N'1665992600_00001', N'88465750', N'ARDYANSYAH ROESCAHYANTO', N'1991-04-29', N'3575012904910002', N'KC PASURUAN SUDIRMAN', N'1', N'FLPP', NULL, N'2022-08-05', N'2032-08-05', N'120', N'7810000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PRIMA KEBRAON 2  HH 23, KEL. KEBRAON, KEC. KARANG PILANG, KOTA/KAB KOTA SURABAYA , KODE POS 60222', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'125', N'7203736503', N'1665992600_00001', N'88465785', N'RACHMAT FARDILLAH', N'1997-06-19', N'3514201906970003', N'KC PASURUAN SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-06', N'2037-08-06', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUSN KEDAWUNG, KEL. KEDAWUNG KULON, KEC. GRATI, KOTA/KAB KAB. PASURUAN , KODE POS 67184', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'126', N'7127602136', N'1665992600_00001', N'82652465', N'IMAM FIKRY FANANI', N'1990-08-18', N'3575031808900001', N'KC PASURUAN SUDIRMAN', N'1', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL VETERAN NO 26, KEL. BUGUL KIDUL, KEC. BUGUL KIDUL, KOTA/KAB KOTA PASURUAN , KODE POS 67121', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'127', N'7206009898', N'1665992600_00001', N'88710951', N'FAIZAH WAHYUNINGPRIANTI', N'1996-06-26', N'3575026606960001', N'KC PASURUAN SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL GAJAHMADA GANG I / 18G, KEL. KEBONSARI, KEC. PANGGUNGREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67114', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'128', N'7206184684', N'1665992600_00001', N'88722827', N'MUCHAMAD NUR KOMARI', N'1979-11-19', N'3575021911790004', N'KC PASURUAN SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KH MANSUR RT001 RW001, KEL. TEMBOKREJO, KEC. PURWOREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67116', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'129', N'8311828250', N'1665992600_00001', N'61761864', N'SULAIMAN', N'1999-06-08', N'3575020806990004', N'KC PASURUAN SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KOL SUGIONO III E / 60, KEL. MAYANGAN, KEC. PANGGUNGREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67112', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'130', N'7206183564', N'1665992600_00001', N'88722780', N'ACHMAD KHAIRUDDIN', N'1991-02-23', N'3576022302910001', N'KC PASURUAN SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-16', N'2032-08-16', N'120', N'7810000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KALIMANTAN I/13, KEL. TRAJENG, KEC. PANGGUNGREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67111', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'131', N'7206183459', N'1665992600_00001', N'88722707', N'SHINTA AYU PRATIWI', N'1991-01-24', N'3575026401910002', N'KC PASURUAN SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SUNGEGENENG , KEL. LAMONGAN, KEC. SEKARAN, KOTA/KAB KAB. LAMONGAN , KODE POS 62261', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'132', N'7206307492', N'1665992600_00001', N'88731046', N'ADITYAS FIRMANSYAH', N'1994-04-30', N'3575023004940002', N'KC PASURUAN SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL IRIAN JAYA NO 1/26, KEL. KARANGANYAR, KEC. PANGGUNGREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67133', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'133', N'7206183505', N'1665992600_00001', N'88722747', N'MUHAMMAD IDRUS', N'1997-09-12', N'3575011209970001', N'KC PASURUAN SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL HALMAHERA XIII/15, KEL. KARANGANYAR, KEC. PANGGUNGREJO, KOTA/KAB KOTA PASURUAN , KODE POS 67131', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'134', N'7203703478', N'1665992600_00001', N'88074305', N'SIEK STEFANUS HENDRA WIJAYA', N'1995-09-14', N'3374031409950002', N'KC PEKALONGAN YAGIS', N'3', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. PARANGKLITIK IV-5 RT 003 RW 019, KEL. TLOGOSARI KULON, KEC. PEDURUNGAN, KOTA/KAB KOTA SEMARANG , KODE POS 50196', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'135', N'7203652528', N'1665992600_00001', N'88459998', N'AGIL APRIYANDA HASIBUAN', N'2001-06-26', N'1208242604010002', N'KC PEMATANGSIANTAR PERINTIS', N'1', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'HUTA IV , KEL. BANDAR REJO, KEC. BANDAR MASILAM, KOTA/KAB KAB. BATU BARA, KODE POS 21184', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'136', N'7203655198', N'1665992600_00001', N'88460332', N'ARUNG BAHARI SIDALO', N'1995-08-28', N'1207242308950002', N'KC PEMATANGSIANTAR PERINTIS', N'1', N'FLPP', NULL, N'2022-08-04', N'2037-08-04', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PERUM GRIYA PRATAMA SEJAHTERA, KEL. HAMPARAN PERAK, KEC. HAMPARAN PERAK, KOTA/KAB KAB. DELI SERDANG , KODE POS 20374', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'137', N'7205728915', N'1665992600_00001', N'88692165', N'THERESIA INGLANY PARDEDE', N'1999-12-06', N'1207234312990005', N'KC PEMATANGSIANTAR PERINTIS', N'1', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUDUN VII KOMPLEK PARDEDE, KEL. PURWODADI, KEC. SUNGGAL, KOTA/KAB KAB. DELI SERDANG , KODE POS 20351', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'138', N'7205607891', N'1665992600_00001', N'88683680', N'SOPIAN MANURUNG', N'1991-07-25', N'1208122507910002', N'KC PEMATANGSIANTAR PERINTIS', N'2', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'HUTA III BUNTU BAYU, KEL. BUNTU BAYU, KEC. HATONDUHAN, KOTA/KAB KAB. SIMALUNGUN, KODE POS 21178', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'139', N'7205484037', N'1665992600_00001', N'84974414', N'NOPEM SINAGA', N'1997-11-10', N'1208071011970002', N'KC PEMATANGSIANTAR PERINTIS', N'2', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SILAWAR, KEL. SAMBOSAR RAYA, KEC. RAYA KAHEAN, KOTA/KAB KAB. SIMALUNGUN, KODE POS 21158', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'140', N'7206443825', N'1665992600_00001', N'88740206', N'ALFISAH WALDIGAR BUANA', N'1989-10-10', N'1219021010890005', N'KC PEMATANGSIANTAR PERINTIS', N'2', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN III , KEL. SIPARE PARE, KEC. AIR PUTIH, KOTA/KAB KAB. BATU BARA, KODE POS 21256', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'141', N'7206416674', N'1665992600_00001', N'88738157', N'FAHRUR RAJI', N'1992-06-20', N'1219032006920004', N'KC PEMATANGSIANTAR PERINTIS', N'2', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KAMAPUNG DALAM, KEL. SUKARAJA, KEC. AIR PUTIH, KOTA/KAB KAB. BATU BARA, KODE POS 21256', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'142', N'7206704645', N'1665992600_00001', N'88758283', N'ANDIKA', N'1987-12-10', N'1219031012870006', N'KC PEMATANGSIANTAR PERINTIS', N'3', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN TERATAI, KEL. PASAR LAPAN, KEC. AIR PUTIH, KOTA/KAB KAB. BATU BARA, KODE POS 21256', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'143', N'7206954579', N'1665992600_00001', N'88774546', N'ELFRIDA SILITONGA', N'1997-11-21', N'1202056111970002', N'KC PEMATANGSIANTAR PERINTIS', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'HUTA LOMBU, KEL. SIMASOM, KEC. PAHAE JULU, KOTA/KAB KAB. TAPANULI UTARA , KODE POS 22463', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'144', N'7206925498', N'1665992600_00001', N'88772566', N'RAIHAN NST', N'1991-08-05', N'1213050508910002', N'KC PEMATANGSIANTAR PERINTIS', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'HUTABARINGIN, KEL. HUTABARINGIN, KEC. PENYABUNGAN BARAT, KOTA/KAB KAB. MANDAILING NATAL  , KODE POS 22977', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'145', N'8044421410', N'1665992600_00001', N'60812895', N'RAMA NOPRIADI', N'1995-04-29', N'6107052904950002', N'KC PONTIANAK ABDURRACHMAN', N'3', N'FLPP', NULL, N'2022-08-02', N'2032-08-02', N'120', N'8580000', N'156000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN SELUAS, KEL. SELUAS, KEC. SELUAS, KOTA/KAB KAB. BENGKAYANG    , KODE POS 79285', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'146', N'9461280390', N'1665992600_00001', N'63217932', N'DODI AFRIAN', N'1997-04-28', N'6104182804970001', N'KC PONTIANAK ABDURRACHMAN', N'1', N'FLPP', NULL, N'2022-08-02', N'2032-08-02', N'120', N'8580000', N'156000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. ARIF RAHMAN HAKIM. RT.002/RW.001, KEL. TUAN TUAN, KEC. BENUA KAYONG, KOTA/KAB KAB. KETAPANG , KODE POS 78822', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'147', N'7202428856', N'1665992600_00001', N'88375756', N'MUHAMMAD ROMLI', N'1996-07-09', N'6171030907960006', N'KC PONTIANAK GUSTI SULUNG', N'1', N'FLPP', NULL, N'2022-08-09', N'2037-08-09', N'180', N'12870000', N'156000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL KY SUDARSO GG SUKAMAJU DALAM 3, KEL. SUNGAI JAWI LUAR, KEC. PONTIANAK BARAT, KOTA/KAB KOTA PONTIANAK , KODE POS 78113', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'148', N'7205412543', N'1665992600_00001', N'73493874', N'HARTOTO', N'1982-05-09', N'6104170905820004', N'KC PONTIANAK GUSTI SULUNG', N'1', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'12870000', N'156000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL HUSIN HAMZAH GG PENDIDIKAN, KEL. PAL LIMA, KEC. PONTIANAK BARAT, KOTA/KAB KOTA PONTIANAK , KODE POS 78114', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'149', N'7206855497', N'1665992600_00001', N'88768049', N'WURI AMANDA PUTRI', N'1996-04-21', N'6171036104960002', N'KC PONTIANAK GUSTI SULUNG', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'12870000', N'156000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL HUSIN HAMZAH KP VILLA ARTHALAND NO D9, KEL. PAL LIMA, KEC. PONTIANAK BARAT, KOTA/KAB KOTA PONTIANAK , KODE POS 78114', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'150', N'7206612015', N'1665992600_00001', N'88751875', N'ENI ENJELINA SILBUATI', N'2001-07-18', N'6103095807010001', N'KC PONTIANAK GUSTI SULUNG', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'12870000', N'156000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN GOK NALA, KEL. DOSAN, KEC. PARINDU, KOTA/KAB KAB. SANGGAU , KODE POS 78561', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'151', N'7205355768', N'1665992600_00001', N'88459424', N'ZULFI ZUMALA DEWI', N'1991-04-25', N'1608126504910001', N'KC PRABUMULIH SUDIRMAN 1', N'2', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. JEND. SUDIRMAN, KEL. MUARA DUA, KEC. PRABUMULIH TIMUR, KOTA/KAB KOTA PRABUMULIH , KODE POS 31111', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'152', N'7205779129', N'1665992600_00001', N'87853613', N'NAUFAL ZHORIFFALAH', N'2000-04-29', N'1674023004000005', N'KC PRABUMULIH SUDIRMAN 1', N'2', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL TAMPOMAS, KEL. MUARA DUA, KEC. PRABUMULIH TIMUR, KOTA/KAB KOTA PRABUMULIH , KODE POS 31112', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'153', N'7134672417', N'1665992600_00001', N'83135960', N'ROLISA APTIANI', N'2001-07-02', N'1603154107010080', N'KC PRABUMULIH SUDIRMAN 1', N'3', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN III, KEL. SUGIHAN, KEC. RAMBANG, KOTA/KAB KAB. LEMATANG ILIR OGAN TENGAH (MUARA ENIM) , KODE POS 31173', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'154', N'7200763248', N'1665992600_00001', N'88267051', N'ALITO BARROS', N'1986-06-06', N'5304131306840001', N'KC PROBOLINGGO', N'3', N'FLPP', NULL, N'2022-08-12', N'2034-08-12', N'144', N'9405000', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL BRIGJEN SLAMET RIYADI 39 B, KEL. ORO ORO DOWO, KEC. KLOJEN, KOTA/KAB KOTA MALANG , KODE POS 65119', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'155', N'7205960265', N'1665992600_00001', N'88708024', N'ENNY HERLANY', N'1984-01-23', N'3573026301840005', N'KC PROBOLINGGO', N'3', N'FLPP', NULL, N'2022-08-16', N'2034-08-16', N'144', N'9405000', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL MAYJEN PANJAITAN 31, KEL. PENANGGUNGAN, KEC. KLOJEN, KOTA/KAB KOTA MALANG , KODE POS 65113', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'156', N'7207414551', N'1665992600_00001', N'88805121', N'MOCH YUDIK PRASTYO', N'1993-08-28', N'3513142808930001', N'KC PROBOLINGGO', N'3', N'FLPP', NULL, N'2022-08-31', N'2037-08-31', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SULTAN AGUNG RT 02 RW 04, KEL. SEMAMPIR, KEC. KRAKSAAN, KOTA/KAB KAB. PROBOLINGGO , KODE POS 67282', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'157', N'7206561526', N'1665992600_00001', N'88748356', N'BESLI TAMPUBOLON', N'1978-08-19', N'3603121908780007', N'KC RANTAU PRAPAT', N'1', N'FLPP', NULL, N'2022-08-24', N'2032-08-24', N'120', N'7837500', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. ANYELIR II BLOK CC/08 BUMI INDAH, KEL. KUTAJAYA, KEC. PASAR KEMIS, KOTA/KAB KAB. TANGERANG , KODE POS 15560', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'158', N'7206638243', N'1665992600_00001', N'88753800', N'REVINA PANDIANGAN', N'1988-05-05', N'3273104505880009', N'KC RANTAU PRAPAT', N'1', N'FLPP', NULL, N'2022-08-24', N'2037-08-24', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. SENIN PERUM PLAMBOYAN, KEL. AEK KANOPAN, KEC. KUALUH HULU, KOTA/KAB KAB. LABUHANBATU UTARA, KODE POS 21273', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'159', N'1017363437', N'1665992600_00001', N'51068522', N'HENDY SEPTYA RAHMAN', N'1988-08-26', N'6472032609880003', N'KC SAMARINDA BHAYANGKARA', N'1', N'FLPP', NULL, N'2022-08-03', N'2037-08-03', N'180', N'12892688', N'156275000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. AW. SYAHRANIE RATINDO II NO E9, KEL. AIR HITAM, KEC. SAMARINDA ULU, KOTA/KAB KOTA SAMARINDA , KODE POS 75124', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'160', N'7203659207', N'1665992600_00001', N'61203232', N'ELMA YULIANTINA', N'1994-07-24', N'3278026407940003', N'KC TASIKMALAYA A YANI', N'1', N'FLPP', NULL, N'2022-08-12', N'2034-08-12', N'144', N'8778000', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL RE MARTADINATA 79, KEL. CIPEDES, KEC. CIPEDES, KOTA/KAB KOTA TASIKMALAYA , KODE POS 46133', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'161', N'7203364557', N'1665992600_00001', N'88440783', N'MOCHAMMAD PRAYOGO', N'1994-09-04', N'3374020409940001', N'KC TEGAL KS TUBUN', N'2', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PALA BARAT 3 NO 18, KEL. MEJASEM BARAT, KEC. KRAMAT, KOTA/KAB KAB. TEGAL , KODE POS 52181', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'162', N'1057268065', N'1665992600_00001', N'51139124', N'IRFAN ADI RIANTO', N'1993-10-31', N'3328083110930003', N'KC TEGAL KS TUBUN', N'3', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'9075000', N'110000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL BANYUWANGI RT 6 RW 1, KEL. SUMURPANGGANG, KEC. MARGADANA, KOTA/KAB KOTA TEGAL , KODE POS 52141', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'163', N'7203710989', N'1665992600_00001', N'88464060', N'ARIF NANDA NUGROHO', N'1990-12-21', N'1871132112900005', N'KCP BANDAR LAMPUNG ANTASARI', N'3', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PRAMUKA PURNAWIRAWAN LK II, KEL. SUMBERREJO, KEC. KEMILING, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35153', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'164', N'7206034674', N'1665992600_00001', N'88712912', N'NOPRIZAL SETIAWAN', N'1994-11-16', N'1871111611940006', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. M YUNUS UJUNG SAMPING GG WARU NO. 67, KEL. PEMATANG WANGI, KEC. TANJUNG SENANG, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35141', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'165', N'7205510294', N'1665992600_00001', N'88677560', N'ZULFERIANI', N'1986-02-25', N'1871136502860003', N'KCP BANDAR LAMPUNG ANTASARI', N'1', N'FLPP', NULL, N'2022-08-10', N'2037-08-10', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL IMAM BONJOL GG MADANI NO 09 LK II, KEL. SUMBERREJO, KEC. KEMILING, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35153', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'166', N'1025616096', N'1665992600_00001', N'53527427', N'HENDRIANSYAH', N'1989-09-26', N'1807022609890003', N'KCP BANDAR LAMPUNG ANTASARI', N'1', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. LETNAN TUKIRAN NO. 1260 , KEL. TANJUNG AGUNG, KEC. BATURAJA BARAT, KOTA/KAB KAB. OGAN KOMERING ULU , KODE POS 32113', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'167', N'7206099601', N'1665992600_00001', N'88717217', N'YULI ANTIKA', N'2001-04-10', N'1808085004010002', N'KCP BANDAR LAMPUNG ANTASARI', N'3', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BANDAR SARI, KEL. BANDAR SARI, KEC. WAY TUBA, KOTA/KAB KAB.  WAY KANAN, KODE POS 34764', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'168', N'7206035123', N'1665992600_00001', N'88712950', N'UNTUNG DINATA', N'1984-12-25', N'1801082512840006', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN III A, KEL. FAJAR BARU, KEC. JATI AGUNG, KOTA/KAB KAB. LAMPUNG SELATAN , KODE POS 35365', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'169', N'7206167194', N'1665992600_00001', N'88721798', N'GUNAWAN', N'1996-03-28', N'1810072803960003', N'KCP BANDAR LAMPUNG ANTASARI', N'3', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. NANGKA II LK. II, KEL. KORPRI JAYA, KEC. SUKARAME, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35131', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'170', N'7206356752', N'1665992600_00001', N'88734302', N'FADLIAN CAHYA KAMALA', N'1995-08-01', N'1872030108950004', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'MULYOJATI, KEL. MULYOJATI, KEC. METRO BARAT, KOTA/KAB KOTA  METRO , KODE POS 34125', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'171', N'7206355691', N'1665992600_00001', N'88734190', N'RANDI SAPUTRA', N'1991-12-12', N'1871111212910001', N'KCP BANDAR LAMPUNG ANTASARI', N'1', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL R DIBALAU GG DAMAU 3 LK I NO 32 , KEL. TANJUNG SENANG, KEC. TANJUNG SENANG, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35141', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'172', N'7206685055', N'1665992600_00001', N'88757081', N'GEFRINA YULANDA RIZKI', N'1999-06-21', N'1810086106990006', N'KCP BANDAR LAMPUNG ANTASARI', N'1', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN PAMASALAK, KEL. SINARBARU, KEC. SUKOHARJO, KOTA/KAB KAB. PRINGSEWU , KODE POS 35674', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'173', N'7206846946', N'1665992600_00001', N'75569323', N'ANGGRI YUDHA PRASETYA', N'1989-12-19', N'1803071912890002', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'GUNUNG BESAR, KEL. GUNUNG BESAR, KEC. ABUNG TENGAH, KOTA/KAB KAB. LAMPUNG UTARA , KODE POS 34558', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'174', N'7206849899', N'1665992600_00001', N'88767630', N'MUHAMMAD RIYAN SABIL', N'2000-07-24', N'1803102407000004', N'KCP BANDAR LAMPUNG ANTASARI', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. PERINTIS GG DARA 6 NO. 469, KEL. TANJUNG AMAN, KEC. KOTABUMI SELATAN, KOTA/KAB KAB. LAMPUNG UTARA , KODE POS 34511', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'175', N'7206965368', N'1665992600_00001', N'64577432', N'ARBI ARIANDO', N'2000-01-01', N'1803040101000003', N'KCP BANDAR LAMPUNG ANTASARI', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11632500', N'141000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'MERAMBUNG, KEL. MERAMBUNG, KEC. TANJUNG RAJA, KOTA/KAB KAB. LAMPUNG UTARA , KODE POS 34557', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'176', N'7206783561', N'1665992600_00001', N'88763264', N'SYAHKURON FAHMI', N'2001-04-10', N'1871091004010001', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11715000', N'142000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL HASANUDDIN GG KTR POS/18 LK II, KEL. GUNUNG MAS, KEC. TELUKBETUNG SELATAN, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35211', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'177', N'7207033892', N'1665992600_00001', N'88778047', N'HANDIKA TRIA RENALDI', N'1999-01-07', N'1807060701990003', N'KCP BANDAR LAMPUNG ANTASARI', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN CEMPAKA, KEL. BANJAREJO, KEC. BATANGHARI, KOTA/KAB KAB.  LAMPUNG TIMUR, KODE POS 34181', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'178', N'7206835707', N'1665992600_00001', N'81149599', N'REFLY ADITIA GIRALDI', N'1997-06-05', N'1805180506970009', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BLOK XI-34-03, KEL. BUMI D MAKMUR, KEC. RAWA JITU TIMUR, KOTA/KAB KAB. TULANG BAWANG , KODE POS 34590', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'179', N'7206840174', N'1665992600_00001', N'88766953', N'ALMAARIF PRAMUDIA PRATAMA', N'1990-06-13', N'1607061306900002', N'KCP BANDAR LAMPUNG ANTASARI', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. MARGO MULYO RT. 017 RW. 001, KEL. SUNGAI PINANG, KEC. RAMBUTAN, KOTA/KAB KAB. BANYUASIN, KODE POS 30967', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'180', N'7206842789', N'1665992600_00001', N'88767166', N'ALDI HENDRITA', N'1997-11-16', N'1803101611970005', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL JENDRAL SUDIRMAN GG DADALI IV NO 23, KEL. TANJUNG AMAN, KEC. KOTABUMI SELATAN, KOTA/KAB KAB. LAMPUNG UTARA , KODE POS 34511', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'181', N'7207058477', N'1665992600_00001', N'88781596', N'AHMAD HAZRURI', N'1987-08-01', N'1609090107870113', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PADANG MANIS, KEL. PADANG MANIS, KEC. WONOSOBO, KOTA/KAB KAB. TANGGAMUS , KODE POS 35385', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'182', N'7207155289', N'1665992600_00001', N'80665595', N'SARMAULI SIHITE', N'1985-09-03', N'1871014309850007', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL NUSANTARA GG GARUDA II NO 4, KEL. LABUHAN RATU RAYA, KEC. LABUHAN RATU, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35141', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'183', N'7207060865', N'1665992600_00001', N'88781724', N'MUCHAMMAD FARIZI', N'1996-08-14', N'1803021408960003', N'KCP BANDAR LAMPUNG ANTASARI', N'2', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. SRIWIJAYA NO. 195, KEL. KOTABUMI PASAR, KEC. KOTABUMI, KOTA/KAB KAB. LAMPUNG UTARA , KODE POS 34518', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'184', N'7207057325', N'1665992600_00001', N'88781520', N'FEBRI', N'1996-02-14', N'1871011402960003', N'KCP BANDAR LAMPUNG ANTASARI', N'3', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL NUSANTARA LK. I , KEL. LABUHAN RATU RAYA, KEC. LABUHAN, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35142', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'185', N'7205824574', N'1665992600_00001', N'88698770', N'MAHARANI DWI PUTRI', N'2001-03-16', N'1872015603010001', N'KCP BANDAR LAMPUNG NATAR', N'3', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL RADEN INTAN NO 14, KEL. IMOPURO, KEC. METRO PUSAT, KOTA/KAB KOTA  METRO , KODE POS 34111', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'186', N'7205772884', N'1665992600_00001', N'88694873', N'SENA BAYU PUTRA', N'1998-10-12', N'1802111210980005', N'KCP BANDAR LAMPUNG NATAR', N'3', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REJOSARI MATARAM, KEL. REJOSARI MATARAM, KEC. SEPUTIH MATARAM, KOTA/KAB KAB. LAMPUNG TENGAH , KODE POS 34164', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'187', N'7205540967', N'1665992600_00001', N'88679599', N'FARHAN REZA PURNAMA', N'1998-11-04', N'1809010411980002', N'KCP BANDAR LAMPUNG NATAR', N'3', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PRAMUKA NO 122 DUSUN VII, KEL. SUKARAJA, KEC. GEDONG TATAAN, KOTA/KAB KAB. PESAWARAN, KODE POS 35371', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'188', N'7206336484', N'1665992600_00001', N'88732966', N'FEBRIANTO WIKAN JAYA ALI', N'1997-02-25', N'1805022502970002', N'KCP BANDAR LAMPUNG NATAR', N'1', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL MEGA NO 206, KEL. UJUNG GUNUNG, KEC. MENGGALA, KOTA/KAB KAB. TULANG BAWANG , KODE POS 34611', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'189', N'7206394287', N'1665992600_00001', N'88736840', N'EZIO MARADILA', N'1996-01-28', N'1804032801960001', N'KCP BANDAR LAMPUNG NATAR', N'1', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PASAR PULAU PISANG, KEL. PASAR PULAU PISANG, KEC. PULAU PISANG, KOTA/KAB KAB. LAMPUNG BARAT , KODE POS 34896', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'190', N'7206379965', N'1665992600_00001', N'88733882', N'ELYSA NOVITA SARI', N'1991-09-10', N'1803025009910004', N'KCP BANDAR LAMPUNG NATAR', N'1', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BEDENG DUA RT 003 RW 007, KEL. KOTABUMI UDIK, KEC. KOTABUMI, KOTA/KAB KAB. LAMPUNG UTARA , KODE POS 34518', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'191', N'7206350487', N'1665992600_00001', N'88733912', N'ANDRIAN SAPUTRA', N'1997-09-16', N'1871021609970004', N'KCP BANDAR LAMPUNG NATAR', N'2', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL P SERIBU NO 109 LK I, KEL. WAY DADI, KEC. SUKARAME, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35131', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'192', N'7206350177', N'1665992600_00001', N'61598739', N'AGUS SUPRIYANTO', N'1987-08-28', N'1871022808870003', N'KCP BANDAR LAMPUNG NATAR', N'2', N'FLPP', NULL, N'2022-08-24', N'2037-08-24', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL P TEGAL GG BAROKAH NO 86 LK I, KEL. WAY DADI, KEC. SUKARAME, KOTA/KAB KOTA BANDAR LAMPUNG , KODE POS 35131', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'193', N'7196722011', N'1665992600_00001', N'87969297', N'CATUR EDO YULIANTO', N'1998-07-27', N'1801162208980002', N'KCP BANDAR LAMPUNG NATAR', N'2', N'FLPP', NULL, N'2022-08-31', N'2037-08-31', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN III WAY MULI TIMUR, KEL. WAI MULI TIMUR, KEC. RAJABASA, KOTA/KAB KAB. LAMPUNG SELATAN , KODE POS 35551', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'194', N'7206095037', N'1665992600_00001', N'84493906', N'PRAYOGO PUJO HARYONO', N'1996-09-02', N'3216060209960032', N'KCP BANJAR PATROMAN', N'2', N'FLPP', NULL, N'2022-08-15', N'2037-08-15', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PAPANMAS BLOK F 8 NO 14 RT 002 RW 005, KEL. SETIAMEKAR, KEC. TAMBUN SELATAN, KOTA/KAB KAB. BEKASI , KODE POS 17510', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'195', N'7206614352', N'1665992600_00001', N'88752016', N'TOIP NUROHMAN', N'1989-11-03', N'3207210311890002', N'KCP BANJAR PATROMAN', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN EMPLAK RT 003 RW 001, KEL. EMPLAK, KEC. KALIPUCANG, KOTA/KAB KAB. PANGANDARAN, KODE POS 46397', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'196', N'7206896428', N'1665992600_00001', N'88770708', N'GALANG PERMANA', N'1998-05-24', N'3207221310000001', N'KCP BANJAR PATROMAN', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KARANG SALAM, KEL. PANANJUNG, KEC. PANGANDARAN, KOTA/KAB KAB. CIAMIS , KODE POS 46396', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'197', N'7205981823', N'1665992600_00001', N'88709493', N'AGUS PRIANTO', N'1992-10-06', N'3207350610920001', N'KCP BANJAR PATROMAN', N'3', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN BABAKAN , KEL. PARIGI, KEC. PARIGI, KOTA/KAB KAB. CIAMIS , KODE POS 46393', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'198', N'7205852845', N'1665992600_00001', N'88700770', N'SUPRIAMAN', N'1983-12-25', N'3207222512830001', N'KCP BANJAR PATROMAN', N'3', N'FLPP', NULL, N'2022-08-31', N'2037-08-31', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN WONOHARJO RT 001 RW 012, KEL. WONOHARJO, KEC. PANGANDARAN, KOTA/KAB KAB. CIAMIS , KODE POS 46396', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'199', N'7207363744', N'1665992600_00001', N'88801903', N'ARIEF WIDODO', N'1994-11-04', N'3207220411940003', N'KCP BANJAR PATROMAN', N'1', N'FLPP', NULL, N'2022-08-31', N'2032-08-31', N'120', N'7837500', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN CIRATEUN RT 003 RW 003, KEL. PUTRAPINGGAN, KEC. KALIPUCANG, KOTA/KAB KAB. CIAMIS , KODE POS 46397', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'200', N'7203352055', N'1665992600_00001', N'88439916', N'RAHMAD', N'1998-10-03', N'3202030310980003', N'KCP BANJAR PATROMAN', N'1', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN PANGANDARAN BARAT, KEL. PANGANDARAN, KEC. PANGANDARAN, KOTA/KAB KAB. CIAMIS , KODE POS 46396', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'201', N'7207221947', N'1665992600_00001', N'88792432', N'AGUNG MAULANA', N'2000-10-13', N'3207222405980004', N'KCP BANJAR PATROMAN', N'1', N'FLPP', NULL, N'2022-08-31', N'2037-08-31', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. KALEN BUAYA, KEL. PANGANDARAN, KEC. PANGANDARAN, KOTA/KAB KAB. CIAMIS , KODE POS 46396', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'202', N'7207389368', N'1665992600_00001', N'88803559', N'FAISHAL ABDUL FATAH', N'1998-06-01', N'3207230106980001', N'KCP BANJAR PATROMAN', N'2', N'FLPP', NULL, N'2022-08-31', N'2037-08-31', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN DESA , KEL. CIKEMBULAN, KEC. SIDAMULIH, KOTA/KAB KAB. CIAMIS , KODE POS 46365', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'203', N'7198004213', N'1665992600_00001', N'88074548', N'EDI SUSANTO', N'1988-07-01', N'3510040107880018', N'KCP BANYUWANGI GENTENG', N'2', N'FLPP', NULL, N'2022-08-03', N'2037-08-03', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN PALUAGUNG, KEL. KENDALREJO, KEC. TEGALDLIMO, KOTA/KAB KAB. BANYUWANGI , KODE POS 68484', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'204', N'7202594624', N'1665992600_00001', N'88387262', N'SITI MUYASSAROH', N'1999-05-07', N'3510184705990006', N'KCP BANYUWANGI GENTENG', N'2', N'FLPP', NULL, N'2022-08-02', N'2037-08-02', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KEBUNREJO, KEL. ALASREJO, KEC. WONGSOREJO, KOTA/KAB KAB. BANYUWANGI , KODE POS 68453', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'205', N'7202437715', N'1665992600_00001', N'62377941', N'MUTHIA KHONSA PUTRI SUKMAWATI', N'1997-01-11', N'3510215101970002', N'KCP BANYUWANGI GENTENG', N'2', N'FLPP', NULL, N'2022-08-02', N'2037-08-02', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PERUM GRIYA GIRI MULYA ST 19, KEL. KLATAK, KEC. KALIPURO, KOTA/KAB KAB. BANYUWANGI , KODE POS 68421', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'206', N'7202440082', N'1665992600_00001', N'88376497', N'YOSEPHINE MAYASARI USBOKO', N'1988-07-03', N'3578074307880002', N'KCP BANYUWANGI GENTENG', N'3', N'FLPP', NULL, N'2022-08-02', N'2037-08-02', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL IKAN WADER PARI, KEL. KARANGREJO, KEC. BANYUWANGI, KOTA/KAB KAB. BANYUWANGI , KODE POS 68411', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'207', N'7206471349', N'1665992600_00001', N'88742142', N'RIMA CAROLINA', N'1991-09-28', N'3510166809910003', N'KCP BANYUWANGI GENTENG', N'3', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL BRAWIJAYA GG CEMARA, KEL. KEBALENAN, KEC. BANYUWANGI, KOTA/KAB KAB. BANYUWANGI , KODE POS 68417', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'208', N'7206346781', N'1665992600_00001', N'88733518', N'SEPTIAN RINO WANDIARMIN', N'1997-09-10', N'3510201009970003', N'KCP BANYUWANGI GENTENG', N'3', N'FLPP', NULL, N'2022-08-19', N'2032-08-19', N'120', N'7150000', N'130000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DSN KLONTANG, KEL. GENDOH, KEC. SEMPU, KOTA/KAB KAB. BANYUWANGI , KODE POS 68468', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'209', N'7206347818', N'1665992600_00001', N'62308136', N'YULIA AYU PRADITA', N'1999-07-25', N'3510056507990001', N'KCP BANYUWANGI GENTENG', N'3', N'FLPP', NULL, N'2022-08-19', N'2037-08-19', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DSN KRAJAN RT002 RW005, KEL. TEMBOKREJO, KEC. MUNCAR, KOTA/KAB KAB. BANYUWANGI , KODE POS 68472', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'210', N'7206470482', N'1665992600_00001', N'80948455', N'SITI NURUL HIDAYATI', N'1977-03-27', N'3510096703770002', N'KCP BANYUWANGI GENTENG', N'1', N'FLPP', NULL, N'2022-08-23', N'2035-08-23', N'156', N'10188750', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN JALEN I, KEL. SETAIL, KEC. GENTENG, KOTA/KAB KAB. BANYUWANGI , KODE POS 68465', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'211', N'7206471276', N'1665992600_00001', N'88742171', N'NOVAL HARIYADI', N'1978-11-16', N'3510161611780003', N'KCP BANYUWANGI GENTENG', N'1', N'FLPP', NULL, N'2022-08-25', N'2032-08-25', N'120', N'7837500', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SINDURO NO 35, KEL. SINGOTRUNAN, KEC. BANYUWANGI, KOTA/KAB KAB. BANYUWANGI , KODE POS 68415', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'212', N'7207334957', N'1665992600_00001', N'88797998', N'NINA AGUSTINA', N'1998-08-31', N'3510147108980003', N'KCP BANYUWANGI GENTENG', N'1', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DSN LUGONTO, KEL. ROGOJAMPI, KEC. ROGOJAMPI, KOTA/KAB KAB. BANYUWANGI , KODE POS 68462', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'213', N'7205955512', N'1665992600_00001', N'88707693', N'JUNAEDI', N'1977-05-25', N'2171042605779006', N'KCP BATUAJI', N'1', N'FLPP', NULL, N'2022-08-18', N'2032-08-18', N'120', N'8177125', N'148675000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BATU BESAR, KEL. BATU BESAR, KEC. NONGSA, KOTA/KAB KOTA BATAM, KODE POS 29466', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'214', N'7203629936', N'1665992600_00001', N'79532181', N'MARFAD LAPADI', N'1994-05-07', N'7472060705940006', N'KCP BAUBAU YOS SUDARSO', N'2', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'12375000', N'150000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'LINGK BARIYA, KEL. BAADIA, KEC. MURHUM, KOTA/KAB KOTA BAU-BAU , KODE POS 93727', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'215', N'7206079015', N'1665992600_00001', N'82819980', N'MULIYAWATI MUKSIN ZAI', N'1997-06-20', N'7472066007970002', N'KCP BAUBAU YOS SUDARSO', N'3', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'12375000', N'150000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL.GAJAH MADA, KEL. LAMANGGA, KEC. MURHUM, KOTA/KAB KOTA BAU-BAU , KODE POS 93725', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'216', N'7206574032', N'1665992600_00001', N'88749022', N'MIFTA NURUL HASANAH', N'1999-09-19', N'7472065909990006', N'KCP BAUBAU YOS SUDARSO', N'3', N'FLPP', NULL, N'2022-08-23', N'2037-08-23', N'180', N'12375000', N'150000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL.GAJAH MADA, KEL. LAMANGGA, KEC. MURHUM, KOTA/KAB KOTA BAU-BAU , KODE POS 93725', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'217', N'7206842673', N'1665992600_00001', N'63335240', N'IRWANSYAH', N'1988-02-20', N'5206132002881003', N'KCP BERTAIS MANDALIKA', N'2', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'13158750', N'159500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN TODO LAUK, KEL. BENTEK, KEC. GANGGA, KOTA/KAB KAB. LOMBOK UTARA, KODE POS 83353', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'218', N'7206807924', N'1665992600_00001', N'88764793', N'MUHAMMAD RIZKI RIZALDI', N'1998-03-17', N'5272011703980003', N'KCP BERTAIS MANDALIKA', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'13158750', N'159500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. GAJAH MADA, KEL. MONGGONAO, KEC. MPUNDA, KOTA/KAB KOTA. BIMA, KODE POS 84111', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'219', N'7206895947', N'1665992600_00001', N'88770699', N'SITI LOLIGA HAMITAYANI', N'1997-01-17', N'5271044701970003', N'KCP BERTAIS MANDALIKA', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'13158750', N'159500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. SULTAN SALAHUDIN BENDEGA, KEL. TANJUNG KARANG, KEC. SEKARBELA, KOTA/KAB KOTA MATARAM , KODE POS 83115', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'220', N'7207196686', N'1665992600_00001', N'78207611', N'HIZRIANSYAH', N'1992-01-22', N'5206132201920002', N'KCP BERTAIS MANDALIKA', N'3', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'13158750', N'159500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DENA, KEL. DENA, KEC. MADAPANGGA, KOTA/KAB KAB. BIMA , KODE POS 84161', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'221', N'7205906384', N'1665992600_00001', N'87106692', N'SRIWIJAYA', N'1988-10-05', N'7308104510880001', N'KCP BONE JEPPEE', N'2', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'12251250', N'148500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN LACUCO, KEL. ARASOE, KEC. CINA, KOTA/KAB KAB. BONE , KODE POS 92772', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'222', N'7205894537', N'1665992600_00001', N'87411189', N'NUR SAM SAM', N'1999-03-31', N'7401097103990001', N'KCP BONE JEPPEE', N'3', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'12251250', N'148500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KEL.ATULA, KEL. ATULA, KEC. LADONGI, KOTA/KAB KAB. KOLAKA , KODE POS 93573', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'223', N'7205891824', N'1665992600_00001', N'82375005', N'EMAYANTI', N'1993-08-11', N'7308155108930002', N'KCP BONE JEPPEE', N'2', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'12251250', N'148500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN URE KAJUE, KEL. USA, KEC. PALAKKA, KOTA/KAB KAB. BONE , KODE POS 92761', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'224', N'7205736508', N'1665992600_00001', N'50241377', N'SUPRIADI', N'1979-09-07', N'7309010709790002', N'KCP BONE JEPPEE', N'1', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'12251250', N'148500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL G KERINCI NO 21, KEL. WATAMPONE, KEC. TANETE RIATTANG, KOTA/KAB KAB. BONE , KODE POS 92713', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'225', N'7206325717', N'1665992600_00001', N'88731494', N'FAHRUL AQIL', N'1998-02-18', N'7303071802980001', N'KCP BONE JEPPEE', N'1', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'12251250', N'148500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL BALLA PARANG JAYA I, KEL. BALLAPARANG, KEC. RAPPOCINI, KOTA/KAB KOTA MAKASSAR, KODE POS 90222', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'226', N'7206425193', N'1665992600_00001', N'88739042', N'METRO BAITURAHMI', N'1991-10-10', N'1306141010910007', N'KCP CIANJUR ABDULLAH BIN NUH', N'2', N'FLPP', NULL, N'2022-08-19', N'2032-08-19', N'120', N'7947500', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'NUR KAPALO KOTO, KEL. SARIAK LAWEH, KEC. AKABILURU, KOTA/KAB KAB. LIMAPULUH KOTO , KODE POS 26252', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'227', N'7205669145', N'1665992600_00001', N'88679565', N'TAN SIAUW LUNG', N'1988-11-11', N'3203011111880018', N'KCP CIANJUR ABDULLAH BIN NUH', N'3', N'FLPP', NULL, N'2022-08-24', N'2037-08-24', N'180', N'11921250', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KP. SOLOKPANDAN RT 01 RW 10, KEL. SOLOKPANDAN, KEC. CIANJUR, KOTA/KAB KAB. CIANJUR , KODE POS 43214', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'228', N'7207008898', N'1665992600_00001', N'88778431', N'INDRA ANDRIAN', N'2002-07-27', N'3203042707020010', N'KCP CIANJUR ABDULLAH BIN NUH', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11921250', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KP. SARONGGE , KEL. SIRNAGALIH, KEC. CILAKU, KOTA/KAB KAB. CIANJUR , KODE POS 43285', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'229', N'7207011406', N'1665992600_00001', N'50525806', N'SATRIANA SANDI SP', N'1976-06-14', N'3203011406760009', N'KCP CIANJUR ABDULLAH BIN NUH', N'2', N'FLPP', NULL, N'2022-08-26', N'2032-08-26', N'120', N'7947500', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KP. PASAREAN NO.404A, KEL. PAMOYANAN, KEC. CIANJUR, KOTA/KAB KAB. CIANJUR , KODE POS 43211', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'230', N'7207319998', N'1665992600_00001', N'88798934', N'KUSMAN', N'1993-02-11', N'3203151102930005', N'KCP CIANJUR ABDULLAH BIN NUH', N'1', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11921250', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KP. KUBANG, KEL. GIRIJAYA, KEC. PUDING, KOTA/KAB KAB. CIANJUR , KODE POS 43271', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'231', N'7207320026', N'1665992600_00001', N'88798927', N'ALDI MUHAMAD BUDIMAN', N'1990-01-31', N'3203013101900019', N'KCP CIANJUR ABDULLAH BIN NUH', N'2', N'FLPP', NULL, N'2022-08-30', N'2032-08-30', N'120', N'7947500', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PERUM MUSLIM ISTANA KEMBAR, KEL. SUKASARI, KEC. KARANGTENGAH, KOTA/KAB KAB. CIANJUR , KODE POS 43281', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'232', N'7207124022', N'1665992600_00001', N'79232733', N'MUHAMAD RIJAL FAJARI', N'1990-02-08', N'3203300802900002', N'KCP CIANJUR ABDULLAH BIN NUH', N'1', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11921250', N'144500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KP. SUKARAMA, KEL. PUSAKASARI, KEC. LELES, KOTA/KAB KAB. CIANJUR , KODE POS 43273', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'233', N'7203708925', N'1665992600_00001', N'88463904', N'ANDIKA RAHMAN', N'1988-05-30', N'1607031107880007', N'KCP INDRALAYA', N'2', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'10972500', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. MERDEKA, KEL. PANGKALAN BALAI, KEC. BANYUASIN III, KOTA/KAB KAB. BANYUASIN, KODE POS 30753', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'234', N'7203712345', N'1665992600_00001', N'88464092', N'AZWAR ANAZ', N'1990-03-28', N'1503032803900001', N'KCP INDRALAYA', N'2', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'10972500', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. SEKOJO NO 036, KEL. MULIA AGUNG, KEC. BANYUASIN III, KOTA/KAB KAB. BANYUASIN, KODE POS 30911', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'235', N'7205902621', N'1665992600_00001', N'88704179', N'ARDIANSYAPUTRA', N'1995-04-04', N'1607110404950001', N'KCP INDRALAYA', N'2', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'10972500', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DESA TANJUNG TIGA, KEL. TANJUNG TIGA, KEC. RANTAU BAYUR, KOTA/KAB KAB. BANYUASIN, KODE POS 30753', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'236', N'7205902176', N'1665992600_00001', N'88704073', N'TITIS KURNIAWATI', N'1984-09-20', N'1607036009840002', N'KCP INDRALAYA', N'2', N'FLPP', NULL, N'2022-08-11', N'2037-08-11', N'180', N'10972500', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL.KH.SULAIMAN RT 11 RW 03, KEL. KEDONDONG RAYE, KEC. BANYUASIN III, KOTA/KAB KAB. BANYUASIN, KODE POS 30753', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'237', N'7206914402', N'1665992600_00001', N'88771658', N'JUSAH', N'1991-02-05', N'1605150502910001', N'KCP INDRALAYA', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DESA BUKIT ULU , KEL. BUKIT ULU, KEC. KARANG JAYA, KOTA/KAB KAB. MUSI RAWAS , KODE POS 31654', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'238', N'7206705161', N'1665992600_00001', N'88758362', N'RINI KARDILA', N'1997-08-26', N'1610016608970002', N'KCP INDRALAYA', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN II NAGASARI, KEL. NAGASARI, KEC. MUARA KUANG, KOTA/KAB KAB. OGAN ILIR, KODE POS 30665', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'239', N'7206908496', N'1665992600_00001', N'88771438', N'EFRINO ZUANDAF', N'1989-04-02', N'1671090204890005', N'KCP INDRALAYA', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'MULTIWAHANA BLOK N 3 NO 02 RT 14 RW 04, KEL. LEBUNG GAJAH, KEC. SEMATANG BORANG, KOTA/KAB KOTA PALEMBANG , KODE POS 30163', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'240', N'7206916723', N'1665992600_00001', N'88771946', N'HERWANDES', N'1984-06-10', N'1671061006840012', N'KCP INDRALAYA', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL.DAMAI I, KEL. KEBUN BUNGA, KEC. SUKARAMI, KOTA/KAB KOTA PALEMBANG , KODE POS 30152', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'241', N'7206917398', N'1665992600_00001', N'88772077', N'HENDRA FAUZA', N'1990-10-30', N'1611043010900003', N'KCP INDRALAYA', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BATANG HARI, KEL. BATANG HARI, KEC. SEMIDANG AJI, KOTA/KAB KOTA PALEMBANG , KODE POS 30152', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'242', N'7206905063', N'1665992600_00001', N'88771183', N'TRI RAMADHAN', N'1999-12-10', N'1671041012990019', N'KCP INDRALAYA', N'1', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11795438', N'142975000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'MUHAJIRIN III NO 1424 RT 25 RW 07, KEL. LOROK PAKJO, KEC. ILIR BARAT I, KOTA/KAB KOTA PALEMBANG , KODE POS 30137', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'243', N'7207305795', N'1665992600_00001', N'88797876', N'INDRA CAKRA WIJAYA', N'1996-05-28', N'1607032805960003', N'KCP INDRALAYA', N'1', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'10972500', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. BUKIT INDAH LR BAHAGIA NO 46, KEL. KEDONDONG RAYE, KEC. BANYUASIN III, KOTA/KAB KAB. BANYUASIN, KODE POS 30911', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'244', N'7207250648', N'1665992600_00001', N'88794374', N'YOSKADONAL', N'1993-12-02', N'1607050202930001', N'KCP INDRALAYA', N'2', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'10972500', N'133000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DESA KELUANG RT 04 RW 02, KEL. KELUANG, KEC. TUNGKAL ILIR, KOTA/KAB KAB. BANYUASIN, KODE POS 30956', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'245', N'1055154647', N'1665992600_00001', N'54119296', N'SURYADI ABDAR', N'1990-08-01', N'1115011305870001', N'KCP JOHAN PAHLAWAN', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11880000', N'144000000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN COT SELAMAT DESA BLANG MUKO , KEL. BLANG MUKO, KEC. KUALA, KOTA/KAB KAB. NAGAN RAYA, KODE POS 23661', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'246', N'7205686082', N'1665992600_00001', N'88689387', N'TRI INDRA UTAMI', N'1991-06-16', N'3508105606910001', N'KCP KENCONG ', N'2', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. LETKOL SLAMET WARDOYO, KEL. LABRUK LOR, KEC. LUMAJANG, KOTA/KAB KAB. LUMAJANG , KODE POS 67311', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'247', N'7205693038', N'1665992600_00001', N'88689742', N'LUKI JEMI HENDRIK', N'1986-09-05', N'3522060509860002', N'KCP KENCONG ', N'2', N'FLPP', NULL, N'2022-08-26', N'2034-08-26', N'144', N'9431400', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. HAYAM WURUK GG. KLENGKENG 1 NO. II, KEL. KEPUHARJO, KEC. LUMAJANG, KOTA/KAB KAB. LUMAJANG , KODE POS 67316', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'248', N'7205812142', N'1665992600_00001', N'88409455', N'YULIANA FIFI ANGGRAENI', N'1995-07-25', N'3508046507950002', N'KCP KENCONG ', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN GEDONGSARI, KEL. LABRUK KIDUL, KEC. SUMBERSUKO, KOTA/KAB KAB. LUMAJANG , KODE POS 67316', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'249', N'7205809826', N'1665992600_00001', N'88697627', N'AKHMAD RAVI', N'1997-06-09', N'3508100906970005', N'KCP KENCONG ', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. JEND SUTOYO, KEL. ROGOTRUNAN, KEC. LUMAJANG, KOTA/KAB KAB. LUMAJANG , KODE POS 67316', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'250', N'7206805204', N'1665992600_00001', N'88764684', N'DWI KURNIAWAN NURDIANSYAH', N'1996-06-16', N'3508101606960004', N'KCP KENCONG ', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. MAYOR KOMARI SAMPURNO NO. 8, KEL. DITROTUNAN, KEC. LUMAJANG, KOTA/KAB KAB. LUMAJANG , KODE POS 67313', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'251', N'7206636062', N'1665992600_00001', N'88753659', N'RIZKA MUNING SAMPURNAWATI', N'1995-01-15', N'3508125501950003', N'KCP KENCONG ', N'3', N'FLPP', NULL, N'2022-08-31', N'2032-08-31', N'120', N'7859500', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KRAJAN I, KEL. BURNO, KEC. SENDURO, KOTA/KAB KAB. LUMAJANG , KODE POS 67316', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'252', N'7206609073', N'1665992600_00001', N'64665353', N'ANITA DIAN FRANSISCA', N'1991-01-14', N'3508125401910004', N'KCP KENCONG ', N'1', N'FLPP', NULL, N'2022-08-31', N'2037-08-31', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KARANGANYAR, KEL. BURNO, KEC. SENDURO, KOTA/KAB KAB. LUMAJANG , KODE POS 67316', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'253', N'7205426358', N'1665992600_00001', N'88671680', N'IRA PURNAMAYATI', N'1983-02-19', N'1608125902830002', N'KCP KOTA MARTAPURA', N'1', N'FLPP', NULL, N'2022-08-05', N'2037-08-05', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN MARTAPURA, KEL. DUSUN MARTAPURA, KEC. MARTAPURA, KOTA/KAB KAB. OGAN KOMEING ULU TIMUR, KODE POS 32181', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'254', N'7205533081', N'1665992600_00001', N'88679057', N'BOB TOTO POLI', N'1984-06-05', N'1104030506840003', N'KCP KOTA MARTAPURA', N'1', N'FLPP', NULL, N'2022-08-08', N'2032-08-08', N'120', N'7837500', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BELAMBANGAN, KEL. BELAMBANGAN, KEC. PENGANDONAN, KOTA/KAB KAB. OGAN KOMERING ULU , KODE POS 32154', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'255', N'7205589303', N'1665992600_00001', N'88682609', N'MUHAMAD NURHADI', N'1975-08-16', N'1608021608750004', N'KCP KOTA MARTAPURA', N'2', N'FLPP', NULL, N'2022-08-09', N'2032-08-09', N'120', N'7859500', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SUKARAJA, KEL. SUKARAJA, KEC. BUAY PEMUKA PELIUNG, KOTA/KAB KAB. OGAN KOMEING ULU TIMUR, KODE POS 32361', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'256', N'7206052613', N'1665992600_00001', N'83653792', N'DIANRAFI ALPHATIO WIJAYA', N'1998-03-20', N'1608092003980002', N'KCP KOTA MARTAPURA', N'2', N'FLPP', NULL, N'2022-08-12', N'2032-08-12', N'120', N'7859500', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JAYA MULYA OKU TIMUR, KEL. JAYA MULYA, KEC. SEMENDAWAI SUKU III, KOTA/KAB KAB. OGAN KOMEING ULU TIMUR, KODE POS 32185', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'257', N'7206368645', N'1665992600_00001', N'88735108', N'CHOIRUL ANAM', N'1982-01-11', N'1608021101820001', N'KCP KOTA MARTAPURA', N'2', N'FLPP', NULL, N'2022-08-18', N'2037-08-18', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'TERUKIS RAHAYU, KEL. TERUKIS RAHAYU, KEC. MARTAPURA, KOTA/KAB KAB. OGAN KOMEING ULU TIMUR, KODE POS 32181', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'258', N'7206404347', N'1665992600_00001', N'88737491', N'MAGREFA EPTANASIA PUTRI', N'1996-05-28', N'1611046805960084', N'KCP KOTA MARTAPURA', N'2', N'FLPP', NULL, N'2022-08-18', N'2032-08-18', N'120', N'7859500', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KOTA BARU, KEL. KOTA BARU, KEC. MARTAPURA, KOTA/KAB KAB. OGAN KOMEING ULU TIMUR, KODE POS 32181', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'259', N'7206930588', N'1665992600_00001', N'88772850', N'AMALIA', N'1980-05-18', N'1608015805800001', N'KCP KOTA MARTAPURA', N'3', N'FLPP', NULL, N'2022-08-25', N'2035-08-25', N'156', N'10217350', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'TERUKIS RAHAYU, KEL. TERUKIS RAHAYU, KEC. MARTAPURA, KOTA/KAB KAB. OGAN KOMEING ULU TIMUR, KODE POS 32181', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'260', N'7207261216', N'1665992600_00001', N'88795053', N'DESLI NATALIA', N'1987-12-26', N'1607106612870003', N'KCP KOTA MARTAPURA', N'3', N'FLPP', NULL, N'2022-08-30', N'2032-08-30', N'120', N'7859500', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'BATURAJA, KEL. AIR PAOH, KEC. BATURAJA TIMUR, KOTA/KAB KAB. OGAN KOMERING ULU , KODE POS 32111', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'261', N'7203221767', N'1665992600_00001', N'88430871', N'ROMASANTI MARPAUNG', N'1977-08-28', N'9202126808770002', N'KCP KOTA PINANG', N'3', N'FLPP', NULL, N'2022-08-02', N'2036-08-02', N'168', N'10972500', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL SEROJA RAYA PESANGGRAHAN BLOK C, KEL. TANJUNG SELAMAT, KEC. MEDAN TUNTUNGAN, KOTA/KAB KOTA MEDAN , KODE POS 20134', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'262', N'7205754037', N'1665992600_00001', N'74044046', N'DIPO BAGINDA SOJUANGON', N'1982-03-05', N'1271030503820004', N'KCP KOTA PINANG', N'3', N'FLPP', NULL, N'2022-08-12', N'2037-08-12', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL PULO MAS BLOK B-07, KEL. SOSOPAN, KEC. KOTAPINANG, KOTA/KAB KAB. LABUHANBATU SELATAN, KODE POS 21464', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'263', N'7206148343', N'1665992600_00001', N'88717315', N'DEDI SYAHPUTRA', N'1982-10-20', N'1222032010820006', N'KCP KOTA PINANG', N'1', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'11793375', N'142950000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'CIKAMPAK IA, KEL. AEK BATU, KEC. TORGAMBA, KOTA/KAB KAB. LABUHANBATU SELATAN, KODE POS 21464', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'264', N'7206163423', N'1665992600_00001', N'88721324', N'NICO JUNANDA', N'1987-06-09', N'1222010906870003', N'KCP KOTA PINANG', N'1', N'FLPP', NULL, N'2022-08-16', N'2037-08-16', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'KAMP. TUJUH JADI, KEL. KOTAPINANG, KEC. KOTAPINANG, KOTA/KAB KAB. LABUHANBATU SELATAN, KODE POS 21464', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'265', N'7206670309', N'1665992600_00001', N'88755577', N'SYAHMI SUHAEMI', N'1990-06-01', N'1223074106900001', N'KCP KOTA PINANG', N'1', N'FLPP', NULL, N'2022-08-24', N'2033-08-24', N'132', N'8621250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'PANGKALAN, KEL. PANGKALAN, KEC. AEK NATAS, KOTA/KAB KAB. LABUHANBATU SELATAN, KODE POS 21455', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'266', N'7202210532', N'1665992600_00001', N'88361262', N'WINDY SUKMI RAKHMATIAN', N'1999-04-25', N'3508106504990002', N'KCP LUMAJANG IMAM BONJOL', N'3', N'FLPP', NULL, N'2022-08-30', N'2037-08-30', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KRAJAN 02 , KEL. BANJARWARU, KEC. LUMAJANG, KOTA/KAB KAB. LUMAJANG , KODE POS 67351', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'267', N'7136436613', N'1665992600_00001', N'83261198', N'FIDYA YANUAR RAMADHANI', N'1997-01-09', N'3508194901970002', N'KCP LUMAJANG IMAM BONJOL', N'3', N'FLPP', NULL, N'2022-08-29', N'2037-08-29', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. PESANTREN RT 11 RW 05, KEL. KLAKAH, KEC. KLAKAH, KOTA/KAB KAB. LUMAJANG , KODE POS 67356', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'268', N'7207055667', N'1665992600_00001', N'88275879', N'LAFRIDHA ALYAZAHARI', N'1999-02-07', N'3508104702990001', N'KCP LUMAJANG SUDIRMAN', N'1', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'JL. GRAND ZAM ZAM TOGA TOWN HOUSE, KEL. TOMPOKERSAN, KEC. LUMAJANG, KOTA/KAB KAB. LUMAJANG , KODE POS 67311', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'269', N'7206799565', N'1665992600_00001', N'88764274', N'SANDY YUDHA BAYU RIANTO PUTRA', N'1990-03-19', N'3508101903900007', N'KCP LUMAJANG SUDIRMAN', N'2', N'FLPP', NULL, N'2022-08-24', N'2037-08-24', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KRAJAN, KEL. DENOK , KEC. TEKUNG, KOTA/KAB KAB. LUMAJANG , KODE POS 67381', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'270', N'7207066073', N'1665992600_00001', N'88782118', N'ADITYA YULIANTO', N'1991-07-31', N'3508173107910002', N'KCP LUMAJANG SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-26', N'2037-08-26', N'180', N'11789250', N'142900000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KRAJAN, KEL. JATIROTO, KEC. JATIROTO, KOTA/KAB KAB. LUMAJANG , KODE POS 67159', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_pengajuan_klaim_peralihan] ([id], [norek], [no_klaim], [no_cif], [nama_debitur], [tanggal_lahir], [no_ktp], [kantor_cabang], [jenis_klaim], [fasilitas_kredit], [tanggal_kejadian], [jangka_waktu_mulai_kredit], [jangka_waktu_akhir_kredit], [tenor_kredit], [premi_asuransi], [nilai_plafond_kredit], [nilai_pokok_pengajuan_klaim], [nilai_bunga_pengajuan_klaim], [nilai_total_pengajuan_klaim], [nilai_persetujuan_klaim], [nilai_klaim_yg_dbayar], [tanggal_pembayaran_klaim], [bukti_pembayaran], [keterangan], [status_klaim], [create_by], [create_date], [update_by], [update_date], [p_klaim_1], [p_klaim_2], [dokumen_klaim], [sk_meninggal], [fc_surat_tagihan], [slik_ojk], [sk_terjadi_kebakaran], [syarat_lain], [alamat], [sebab_klaim], [dokumen_tambahan], [file_ktp], [file_kk], [file_akad], [file_norek], [file_realisasi], [file_skpengangkatan], [file_surat_pengajuan], [file_meninggal], [file_macet], [file_kebakaran], [file_slik]) VALUES (N'271', N'7206791521', N'1665992600_00001', N'88763793', N'EL VARIDZI MANGKU ALAM', N'2001-04-08', N'3508040804010009', N'KCP LUMAJANG SUDIRMAN', N'3', N'FLPP', NULL, N'2022-08-25', N'2037-08-25', N'180', N'11756250', N'142500000', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1', N'2022-10-17 14:43:20.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'DUSUN KALIBENDO UTARA, KEL. KALIBENDO, KEC. PASIRIAN, KOTA/KAB KAB. LUMAJANG , KODE POS 67372', N'Lorem ipsum dolor sit amet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[tm_pengajuan_klaim_peralihan] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_status_klaim
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_status_klaim]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_status_klaim]
GO

CREATE TABLE [dbo].[tm_status_klaim] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [status_klaim] varchar(255) COLLATE Latin1_General_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tm_status_klaim] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tm_status_klaim
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_status_klaim] ON
GO

INSERT INTO [dbo].[tm_status_klaim] ([id], [status_klaim]) VALUES (N'1', N'Pengajuan Klaim')
GO

INSERT INTO [dbo].[tm_status_klaim] ([id], [status_klaim]) VALUES (N'2', N'Semua Dokumen Belum Dikirim')
GO

INSERT INTO [dbo].[tm_status_klaim] ([id], [status_klaim]) VALUES (N'3', N'Dokumen Belum Lengkap')
GO

INSERT INTO [dbo].[tm_status_klaim] ([id], [status_klaim]) VALUES (N'4', N'Menunggu Persetujuan Asuransi')
GO

INSERT INTO [dbo].[tm_status_klaim] ([id], [status_klaim]) VALUES (N'5', N'Klaim Paid')
GO

INSERT INTO [dbo].[tm_status_klaim] ([id], [status_klaim]) VALUES (N'6', N'Ditolak')
GO

INSERT INTO [dbo].[tm_status_klaim] ([id], [status_klaim]) VALUES (N'7', N'Banding')
GO

SET IDENTITY_INSERT [dbo].[tm_status_klaim] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tm_user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tm_user]') AND type IN ('U'))
	DROP TABLE [dbo].[tm_user]
GO

CREATE TABLE [dbo].[tm_user] (
  [id_user] int  IDENTITY(1,1) NOT NULL,
  [id_groups] int  NULL,
  [nama] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [username] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [password] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [avatar] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [status] int  NULL,
  [kode_user] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [token] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [id_cabang] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [id_region] int  NULL,
  [id_area] int  NULL
)
GO

ALTER TABLE [dbo].[tm_user] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tm_user
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tm_user] ON
GO

INSERT INTO [dbo].[tm_user] ([id_user], [id_groups], [nama], [username], [password], [avatar], [status], [kode_user], [token], [id_cabang], [id_region], [id_area]) VALUES (N'1', N'1', N'Adminis', N'admin', N'haha', N'admin_1657783865.png', N'1', NULL, N'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImdhcnliYWxkaTA1QGdtYWlsLmNvbSIsImtvZGVPVFAiOiIxOTkxOTkifQ.phURRsaxIB05zNSMV_7NpALE_vETQHrVIGQvSbjWV6M', NULL, NULL, NULL)
GO

INSERT INTO [dbo].[tm_user] ([id_user], [id_groups], [nama], [username], [password], [avatar], [status], [kode_user], [token], [id_cabang], [id_region], [id_area]) VALUES (N'2', N'2', N'Fitri Yuliani', N'test', N'password', NULL, N'1', N'BSI-001', NULL, NULL, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[tm_user] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tt_menu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tt_menu]') AND type IN ('U'))
	DROP TABLE [dbo].[tt_menu]
GO

CREATE TABLE [dbo].[tt_menu] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [id_menu] int  NULL,
  [id_groups] int  NULL
)
GO

ALTER TABLE [dbo].[tt_menu] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tt_menu
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tt_menu] ON
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'1', N'1', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'2', N'2', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'3', N'3', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'4', N'4', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'6', N'5', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'7', N'6', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'8', N'7', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'9', N'8', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'10', N'9', N'1')
GO

INSERT INTO [dbo].[tt_menu] ([id], [id_menu], [id_groups]) VALUES (N'11', N'10', N'1')
GO

SET IDENTITY_INSERT [dbo].[tt_menu] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tt_pengajuan_klaim_approval
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tt_pengajuan_klaim_approval]') AND type IN ('U'))
	DROP TABLE [dbo].[tt_pengajuan_klaim_approval]
GO

CREATE TABLE [dbo].[tt_pengajuan_klaim_approval] (
  [id_pengajuan_klaim] int  NULL,
  [app_1_status] int  NULL,
  [app_1_keterangan] text COLLATE Latin1_General_CI_AS  NULL,
  [app_1_create_by] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [app_1_create_date] datetime  NULL,
  [app_2_status] int  NULL,
  [app_2_keterangan] text COLLATE Latin1_General_CI_AS  NULL,
  [app_2_create_by] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [app_2_create_date] datetime  NULL,
  [app_3_status] int  NULL,
  [app_3_keterangan] text COLLATE Latin1_General_CI_AS  NULL,
  [app_3_create_by] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [app_3_create_date] datetime  NULL,
  [id_tt] int  IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE [dbo].[tt_pengajuan_klaim_approval] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kekurangn kelengkapan dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tt_pengajuan_klaim_approval',
'COLUMN', N'app_1_status'
GO

EXEC sp_addextendedproperty
'MS_Description', N'1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kekurangn kelengkapan dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tt_pengajuan_klaim_approval',
'COLUMN', N'app_2_status'
GO

EXEC sp_addextendedproperty
'MS_Description', N'1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kekurangn kelengkapan dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tt_pengajuan_klaim_approval',
'COLUMN', N'app_3_status'
GO


-- ----------------------------
-- Records of tt_pengajuan_klaim_approval
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tt_pengajuan_klaim_approval] ON
GO

INSERT INTO [dbo].[tt_pengajuan_klaim_approval] ([id_pengajuan_klaim], [app_1_status], [app_1_keterangan], [app_1_create_by], [app_1_create_date], [app_2_status], [app_2_keterangan], [app_2_create_by], [app_2_create_date], [app_3_status], [app_3_keterangan], [app_3_create_by], [app_3_create_date], [id_tt]) VALUES (N'1', N'1', N'Data telah disubmit oleh Adminis', N'1', N'2023-02-01 15:27:01.000', N'4', N'oke sipp', N'1', N'2023-02-01 16:15:21.000', NULL, NULL, NULL, NULL, N'1')
GO

SET IDENTITY_INSERT [dbo].[tt_pengajuan_klaim_approval] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for tt_pengajuan_klaim_approval_peralihan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tt_pengajuan_klaim_approval_peralihan]') AND type IN ('U'))
	DROP TABLE [dbo].[tt_pengajuan_klaim_approval_peralihan]
GO

CREATE TABLE [dbo].[tt_pengajuan_klaim_approval_peralihan] (
  [id_pengajuan_klaim] int  NULL,
  [app_1_status] int  NULL,
  [app_1_keterangan] text COLLATE Latin1_General_CI_AS  NULL,
  [app_1_create_by] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [app_1_create_date] datetime  NULL,
  [app_2_status] int  NULL,
  [app_2_keterangan] text COLLATE Latin1_General_CI_AS  NULL,
  [app_2_create_by] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [app_2_create_date] datetime  NULL,
  [app_3_status] int  NULL,
  [app_3_keterangan] text COLLATE Latin1_General_CI_AS  NULL,
  [app_3_create_by] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [app_3_create_date] datetime  NULL,
  [id_tt] int  IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE [dbo].[tt_pengajuan_klaim_approval_peralihan] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kekurangn kelengkapan dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tt_pengajuan_klaim_approval_peralihan',
'COLUMN', N'app_1_status'
GO

EXEC sp_addextendedproperty
'MS_Description', N'1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kekurangn kelengkapan dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tt_pengajuan_klaim_approval_peralihan',
'COLUMN', N'app_2_status'
GO

EXEC sp_addextendedproperty
'MS_Description', N'1: proses klaim, 2: klaim ditolak, 3: klaim dibayar, 4: kekurangn kelengkapan dokumen',
'SCHEMA', N'dbo',
'TABLE', N'tt_pengajuan_klaim_approval_peralihan',
'COLUMN', N'app_3_status'
GO


-- ----------------------------
-- Records of tt_pengajuan_klaim_approval_peralihan
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[tt_pengajuan_klaim_approval_peralihan] ON
GO

INSERT INTO [dbo].[tt_pengajuan_klaim_approval_peralihan] ([id_pengajuan_klaim], [app_1_status], [app_1_keterangan], [app_1_create_by], [app_1_create_date], [app_2_status], [app_2_keterangan], [app_2_create_by], [app_2_create_date], [app_3_status], [app_3_keterangan], [app_3_create_by], [app_3_create_date], [id_tt]) VALUES (N'5', N'1', N'Data telah disubmit oleh Adminis', N'1', N'2022-10-17 15:12:52.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'4')
GO

SET IDENTITY_INSERT [dbo].[tt_pengajuan_klaim_approval_peralihan] OFF
GO

COMMIT
GO


-- ----------------------------
-- Auto increment value for tm_asuransi
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_asuransi]', RESEED, 2)
GO


-- ----------------------------
-- Primary Key structure for table tm_asuransi
-- ----------------------------
ALTER TABLE [dbo].[tm_asuransi] ADD CONSTRAINT [PK__tm_asura__3213E83F2A3A6AED] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_cabang
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_cabang]', RESEED, 293)
GO


-- ----------------------------
-- Primary Key structure for table tm_cabang
-- ----------------------------
ALTER TABLE [dbo].[tm_cabang] ADD CONSTRAINT [PK__tm_caban__3213E83F732A02CE] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_dokumen_klaim
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_dokumen_klaim]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table tm_dokumen_klaim
-- ----------------------------
ALTER TABLE [dbo].[tm_dokumen_klaim] ADD CONSTRAINT [PK__tm_dokum__3213E83FF7174C0E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_groups
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_groups]', RESEED, 5)
GO


-- ----------------------------
-- Auto increment value for tm_jenis_klaim
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_jenis_klaim]', RESEED, 3)
GO


-- ----------------------------
-- Primary Key structure for table tm_jenis_klaim
-- ----------------------------
ALTER TABLE [dbo].[tm_jenis_klaim] ADD CONSTRAINT [PK__tm_jenis__3213E83FCE58F185] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_menu
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_menu]', RESEED, 10)
GO


-- ----------------------------
-- Primary Key structure for table tm_menu
-- ----------------------------
ALTER TABLE [dbo].[tm_menu] ADD CONSTRAINT [PK__tm_menu__68A1D9DB47DC9D5B] PRIMARY KEY CLUSTERED ([id_menu])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_pengajuan_klaim
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_pengajuan_klaim]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table tm_pengajuan_klaim
-- ----------------------------
ALTER TABLE [dbo].[tm_pengajuan_klaim] ADD CONSTRAINT [PK__tm_penga__3213E83F1EBEF9BA] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_pengajuan_klaim_peralihan
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_pengajuan_klaim_peralihan]', RESEED, 271)
GO


-- ----------------------------
-- Primary Key structure for table tm_pengajuan_klaim_peralihan
-- ----------------------------
ALTER TABLE [dbo].[tm_pengajuan_klaim_peralihan] ADD CONSTRAINT [PK__tm_penga__3213E83F84BC8B5C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_status_klaim
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_status_klaim]', RESEED, 7)
GO


-- ----------------------------
-- Primary Key structure for table tm_status_klaim
-- ----------------------------
ALTER TABLE [dbo].[tm_status_klaim] ADD CONSTRAINT [PK__tm_statu__3213E83F49D4F0F3] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tm_user
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tm_user]', RESEED, 589)
GO


-- ----------------------------
-- Primary Key structure for table tm_user
-- ----------------------------
ALTER TABLE [dbo].[tm_user] ADD CONSTRAINT [PK__tm_user__D2D14637F9CE3D8B] PRIMARY KEY CLUSTERED ([id_user])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tt_menu
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tt_menu]', RESEED, 11)
GO


-- ----------------------------
-- Primary Key structure for table tt_menu
-- ----------------------------
ALTER TABLE [dbo].[tt_menu] ADD CONSTRAINT [PK__tt_menu__3213E83F65A386EF] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tt_pengajuan_klaim_approval
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tt_pengajuan_klaim_approval]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table tt_pengajuan_klaim_approval
-- ----------------------------
ALTER TABLE [dbo].[tt_pengajuan_klaim_approval] ADD CONSTRAINT [PK__tt_penga__3213E83FE4CA3F87] PRIMARY KEY CLUSTERED ([id_tt])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tt_pengajuan_klaim_approval_peralihan
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tt_pengajuan_klaim_approval_peralihan]', RESEED, 4)
GO


-- ----------------------------
-- Primary Key structure for table tt_pengajuan_klaim_approval_peralihan
-- ----------------------------
ALTER TABLE [dbo].[tt_pengajuan_klaim_approval_peralihan] ADD CONSTRAINT [PK__tt_penga__3213E83F12EFAFCC] PRIMARY KEY CLUSTERED ([id_tt])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

