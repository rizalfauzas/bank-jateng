<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class empat extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');       
        }
    }

	function index()
	{
        $data['title']  		= "404 Page Not Found";
        // $data['sub_menu']       = 0;
        // $data['page_id']        = 1;

		// $this->template->load('template','contents',$data);
        $this->load->view('404');
	}

}

