<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Klaim_peralihan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Excel');

        $this->load->model('Login_m');
        $this->load->model('Klaim_peralihan_m');

        if (!$this->Login_m->logged_id()) {
            session_destroy();
            redirect('login');
        }
    }

    function index()
    {
        $data['title']          = "Pengajuan Klaim Peralihan";
        $data['sub_menu']       = 2;
        $data['page_id']        = 4;

        $this->template->load('template', 'klaim_peralihan/index', $data);
    }

    function getData()
    {
        header('Content-Type: application/json');

        $results = $this->Klaim_peralihan_m->getDataTable();
        $data = [];
        $no = $_POST['start'];
        foreach ($results as $val) {

            if ($this->session->userdata('id_groups') == 3) {
                if ($val->status_klaim == 1) {
                    $statusna = '<span class="badge bg-primary text-info"><i class="bx bx-loader-circle bx-spin font-size-16 align-middle"></i> Pengajuan_klaim Klaim</span>';
                } else if ($val->status_klaim == 2) {
                    $statusna = '<span class="badge bg-danger"><i class="bx bx-loader-circle font-size-16 align-middle me-2"></i> Proses Klaim</span>';
                } else if ($val->status_klaim == 3) {
                    $statusna = '<span class="badge bg-warning text-danger"><i class="bx bx-loader-circle font-size-16 align-middle"></i> Proses Klaim</span>';
                } else if ($val->status_klaim == 4) {
                    $statusna = '<span class="badge bg-warning text-danger"><i class="bx bx-loader-circle font-size-16 align-middle"></i> Proses Klaim</span>';
                } else if ($val->status_klaim == 5) {
                    $statusna = '<span class="badge bg-success text-success"><i class="bx bx-loader-circle font-size-16 align-middle"></i> Klaim Paid</span>';
                } else if ($val->status_klaim == 6) {
                    $statusna = '<span class="badge bg-danger text-danger"><i class="bx bx-blockfont-size-16 align-middle"></i> Klaim Ditolak</span>';
                } else if ($val->status_klaim == 7) {
                    $statusna = '<span class="badge bg-primary text-primary"><i class="bx bx-rotate-left font-size-16 align-middle"></i> Klaim Banding</span>';
                } else if ($val->status_klaim == 0) {
                    $statusna = '<span class="badge bg-warning text-danger"><i class="bx bx-loader-circle font-size-16 align-middle"></i> Waiting</span>';
                }
            } else {
                if ($val->status_klaim == 1) {
                    $statusna = '<span class="badge bg-info "><i class="bx bx-loader-circle bx-spin font-size-16 align-middle"></i> Pengajuan_klaim Klaim</span>';
                } else if ($val->status_klaim == 2) {
                    $statusna = '<span class="badge bg-warning "><i class="bx bx-loader-circle font-size-16 align-middle me-2"></i> Semua Dokumen Belum Dikirim</span>';
                } else if ($val->status_klaim == 3) {
                    $statusna = '<span class="badge bg-warning "><i class="bx bx-loader-circle font-size-16 align-middle"></i> Dokumen Belum Lengkap</span>';
                } else if ($val->status_klaim == 4) {
                    $statusna = '<span class="badge bg-warning "><i class="bx bx-loader-circle font-size-16 align-middle"></i> Menunggu Persetujuan Asuransi</span>';
                } else if ($val->status_klaim == 5) {
                    $statusna = '<span class="badge bg-success "><i class="bx bx-loader-circle font-size-16 align-middle"></i> Klaim Paid</span>';
                } else if ($val->status_klaim == 6) {
                    $statusna = '<span class="badge bg-danger "><i class="bx bx-blockfont-size-16 align-middle"></i> Klaim Ditolak</span>';
                } else if ($val->status_klaim == 7) {
                    $statusna = '<span class="badge bg-primary "><i class="bx bx-rotate-left font-size-16 align-middle"></i> Klaim Banding</span>';
                } else if ($val->status_klaim == 0) {
                    $statusna = '<span class="badge bg-warning "><i class="bx bx-loader-circle font-size-16 align-middle"></i> Waiting</span>';
                }
            }

            if ($val->app_1_status == 1 && $val->app_2_status == 1 && $val->app_3_status == 3) {
                $statusnya = '<span class="badge bg-success "><i class="bx bx-check-shield font-size-16 align-middle" style="color:green" ></i> Approved</span>';
            } else if ($val->app_1_status == 1 && $val->app_2_status == 2) {
                $statusnya = '<span class="badge bg-danger "><i class="bx bx-block font-size-16 align-middle me-2"></i> Klaim Ditolak</span>';
            } else {
                $statusnya = '<span class="badge bg-warning "><i class="bx bx-loader bx-spin font-size-16 align-middle" style="color:warning" ></i> Pending</span>';
            }

            if ($val->jenis_klaim == 1) {
                $jinisna = 'Meninggal dunia';
            } else if ($val->jenis_klaim == 2) {
                $jinisna = 'Kebakaran';
            } else if ($val->jenis_klaim == 3) {
                $jinisna = 'Macet';
            }

            $row = array();
            // if ($this->session->userdata('id_groups') == 1 or $this->session->userdata('id_groups') == 2 ) {
            $row[] = '<a href="' . base_url('klaim_peralihan/edit/'.$val->idna). '" type="button" class="btn btn-sm btn-soft-success waves-effect waves-success">
                        <i class="ri-edit-line font-size-16 align-middle" style="color:blue"></i>
                    </a>';
                // }
            $row[] = $statusna;
            $row[] = $val->norek;
            $row[] = $val->no_cif;
            $row[] = $val->nama_debitur;
            $row[] = date('d F Y', strtotime($val->tanggal_lahir));
            $row[] = $val->no_ktp;
            $row[] = $val->kantor_cabang;
            $row[] = $jinisna;
            // $row[] = $statusnya ;
            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Klaim_peralihan_m->count_all(),
            "recordsFiltered"   => $this->Klaim_peralihan_m->count_filtered(),
            "data"              => $data
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

    function add()
    {
        $data['title']          = "Form Pengajuan Klaim";
        $data['sub_menu']       = 2;
        $data['page_id']        = 4;

        $data['j_klaim']        = $this->Klaim_peralihan_m->jenis_klaim()->result_array();
        $data['asuransi']       = $this->Klaim_peralihan_m->asuransi()->result_array();
        $data['noklaim']        = $this->Klaim_peralihan_m->kodena();

        $this->template->load('template', 'klaim_peralihan/add', $data);
    }

    function edit($id)
    {
        $data['title']          = "Form Pengajuan Klaim Peralihan";
        $data['sub_menu']       = 2;
        $data['page_id']        = 4;

        // $data['j_klaim']        = $this->Klaim_peralihan_m->jenis_klaim()->result_array();
        $data['asuransi']       = $this->Klaim_peralihan_m->asuransi()->result_array();
        $data['stat']           = $this->Klaim_peralihan_m->status_klaimStep2()->result_array();
        $data['stat3']           = $this->Klaim_peralihan_m->status_klaimStep3()->result_array();
        $data['d']              = $this->Klaim_peralihan_m->pengajuan_klaim($id)->row();
        $data['noklaim']        = $this->Klaim_peralihan_m->kodena();

        $this->template->load('template', 'klaim_peralihan/edit', $data);
    }

    function save()
    {
        extract($_POST);

        // $ceks = $this->db->query("select status_klaim from tm_pengajuan_klaim_peralihan where no_klaim = '".$no_klaim."' ");

        // // var_dump($ceks);
        // // die();

        // if ($ceks->num_rows() < 1) {
        //     $statusNa = 1;
        // } else {
        //     $statusNa = $ceks->row()->status_klaim;
        // }

        $data = [
            'no_klaim'                          => $no_klaim,
            'norek'                             => $norek,
            'no_cif'                            => $no_cif,
            'alamat'                            => $alamat,
            'sebab_klaim'                       => $sebab_klaim,
            'nama_debitur'                      => $nama_debitur,
            'tanggal_lahir'                     => date('Y-m-d', strtotime($tanggal_lahir)),
            'no_ktp'                            => $no_ktp,
            'kantor_cabang'                     => $kantor_cabang,
            'jenis_klaim'                       => $jenis_klaim,
            'fasilitas_kredit'                  => $fasilitas_kredit,
            // 'tanggal_kejadian'                  => date('Y-m-d', strtotime($tanggal_kejadian)),
            'jangka_waktu_mulai_kredit'         => date('Y-m-d', strtotime($jangka_waktu_mulai_kredit)),
            'jangka_waktu_akhir_kredit'         => date('Y-m-d', strtotime($jangka_waktu_akhir_kredit)),
            'tenor_kredit'                      => $tenor_kredit,
            'premi_asuransi'                    => str_replace('.', '', $premi_asuransi),
            'nilai_plafond_kredit'              => str_replace('.', '', $nilai_plafond_kredit),
            'nilai_pokok_pengajuan_klaim'       => str_replace('.', '', $nilai_pokok_pengajuan_klaim),
            // 'nilai_bunga_pengajuan_klaim'       => str_replace('.','',$nilai_bunga_pengajuan_klaim),
            'nilai_total_pengajuan_klaim'       => (int)str_replace('.', '', $nilai_total_pengajuan_klaim),
            'status_klaim'                      => 1, //diajukan
            'create_by'                         => $this->session->userdata('id_user'),
            'create_date'                        => date('Y-m-d H:i:s')
        ];

            $this->db->where('norek', $norek);
            $this->db->update('tm_pengajuan_klaim_peralihan', $data);

            // $se = $this->db->query("select * from tm_pengajuan_klaim_peralihan where no_klaim = '".$no_klaim."' ");

            $dadang = [
                'id_pengajuan_klaim'    => $idna,
                'app_1_status'          => 1, //telah disubmit
                'app_1_keterangan'      => 'Data telah disubmit oleh ' . $this->session->userdata('nama'),
                'app_1_create_by'       => $this->session->userdata('id_user'),
                'app_1_create_date'     => date('Y-m-d H:i:s')
            ];

            if ($this->db->insert('tt_pengajuan_klaim_approval_peralihan', $dadang) > 0) {
                $message['status'] = 'success insert persetujuan klaim';
                $message['idna']     = $idna;
            } else {
                $message['status'] = 'failed insert persetujuan klaim';
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        
    }

    function update()
    {
        extract($_POST);

        $data = [
            'no_klaim'                          => $no_klaim,
            'norek'                             => $norek,
            'no_akad'                           => $no_akad,
            'no_cif'                            => $no_cif,
            'nama_debitur'                      => $nama_debitur,
            'tanggal_lahir'                     => date('Y-m-d', strtotime($tanggal_lahir)),
            'no_ktp'                            => $no_ktp,
            'no_sertifikat_polis'               => $no_sertifikat_polis,
            'kantor_cabang'                     => $kantor_cabang,
            'nama_perusahaan_asuransi'          => $nama_perusahaan_asuransi,
            'jenis_klaim'                       => $jenis_klaim,
            'fasilitas_kredit'                  => $fasilitas_kredit,
            'tanggal_kejadian'                  => date('Y-m-d', strtotime($tanggal_kejadian)),
            'jangka_waktu_mulai_kredit'         => date('Y-m-d', strtotime($jangka_waktu_mulai_kredit)),
            'jangka_waktu_akhir_kredit'         => date('Y-m-d', strtotime($jangka_waktu_akhir_kredit)),
            'tenor_kredit'                      => $tenor_kredit,
            'premi_asuransi'                    => str_replace('.', '', $premi_asuransi),
            'nilai_plafond_kredit'              => str_replace('.', '', $nilai_plafond_kredit),
            'nilai_pokok_pengajuan_klaim'       => str_replace('.', '', $nilai_pokok_pengajuan_klaim),
            'nilai_total_pengajuan_klaim'       => (int)str_replace('.', '', $nilai_total_pengajuan_klaim),
            'status_klaim'                      => 1,
            'create_by'                         => $this->session->userdata('id_user'),
            'create_date'                        => date('Y-m-d H:i:s')
        ];

        $table = 'tm_pengajuan_klaim_peralihan';
        $q = $this->Klaim_peralihan_m->update($id, $table, $data);

        if ($q) {
            $message['status'] = 'success';
        } else {
            $message['status'] = 'failed';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($message));
    }

    function save_persetujuandokumen()
    {
        extract($_POST);

        $get = $this->db->query("select * from tm_pengajuan_klaim_peralihan where id = $id ")->row();

        

        // $lokasina = str_split($get->file_ktp);

        // var_dump($lokasina);
        // die();

        $upload1    = isset($_FILES['file_surat_pengajuan']['name']) ? $_FILES['surat_pengajuan_klaim']['name'] : '';
        $ext1        = pathinfo($upload1, PATHINFO_EXTENSION);

        if ($upload1 !== "") {

            $nmfile1 = "SuratPengajuanKlaim" . time() . '.' . $ext1;
            $nmfile1 = str_replace(" ", "_", $nmfile1);
            $nmfile1 = trim($nmfile1);

            $config1['upload_path']         = './upload/klaim_peralihan/';
            $config1['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config1['max_size']            = 50000;
            $config1['overwrite']           = true;
            $config1['file_name']           = $nmfile1;

            $this->load->library('upload', $config1);
            $this->upload->initialize($config1);

            $this->upload->do_upload('surat_pengajuan_klaim');
            $this->upload->data();
        } else {
            $nmfile1 = $get->surat_pengajuan_klaim;
        }

        $upload2    = isset($_FILES['fc_ktp']['name']) ? $_FILES['fc_ktp']['name'] : '';
        $ext2        = pathinfo($upload2, PATHINFO_EXTENSION);

        if ($upload2 !== "") {

            $nmfile2 = "FcKTP_" . time() . '.' . $ext2;
            $nmfile2 = str_replace(" ", "_", $nmfile2);
            $nmfile2 = trim($nmfile2);

            $config2['upload_path']         = './upload/klaim_peralihan/';
            $config2['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config2['max_size']            = 50000;
            $config2['overwrite']           = true;
            $config2['file_name']           = $nmfile2;

            $this->load->library('upload', $config2);
            $this->upload->initialize($config2);

            $this->upload->do_upload('fc_ktp');
            $this->upload->data();
        } else {
            $nmfile2 = $get->fc_ktp;
        }

        $upload3    = isset($_FILES['fc_kk']['name']) ? $_FILES['fc_kk']['name'] : '';
        $ext3        = pathinfo($upload3, PATHINFO_EXTENSION);

        if ($upload3 !== "") {

            $nmfile3 = "FcKK_" . time() . '.' . $ext3;
            $nmfile3 = str_replace(" ", "_", $nmfile3);
            $nmfile3 = trim($nmfile3);

            $config3['upload_path']         = './upload/klaim_peralihan/';
            $config3['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config3['max_size']            = 50000;
            $config3['overwrite']           = true;
            $config3['file_name']           = $nmfile3;

            $this->load->library('upload', $config3);
            $this->upload->initialize($config3);

            $this->upload->do_upload('fc_kk');
            $this->upload->data();
        } else {
            $nmfile3 = $get->fc_kk;
        }

        $upload4    = isset($_FILES['fc_akad']['name']) ? $_FILES['fc_akad']['name'] : '';
        $ext4        = pathinfo($upload4, PATHINFO_EXTENSION);

        if ($upload4 !== "") {

            $nmfile4 = "FcAkad_" . time() . '.' . $ext4;
            $nmfile4 = str_replace(" ", "_", $nmfile4);
            $nmfile4 = trim($nmfile4);

            $config4['upload_path']         = './upload/klaim_peralihan/';
            $config4['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config4['max_size']            = 50000;
            $config4['overwrite']           = true;
            $config4['file_name']           = $nmfile4;

            $this->load->library('upload', $config4);
            $this->upload->initialize($config4);

            $this->upload->do_upload('fc_akad');
            $this->upload->data();
        } else {
            $nmfile4 = $get->fc_akad;
        }

        $upload5    = isset($_FILES['fc_realisasi_pencairan_pembiayaan']['name']) ? $_FILES['fc_realisasi_pencairan_pembiayaan']['name'] : '';
        $ext5        = pathinfo($upload5, PATHINFO_EXTENSION);

        if ($upload5 !== "") {

            $nmfile5 = "FcRealisasiPencairan_" . time() . '.' . $ext5;
            $nmfile5 = str_replace(" ", "_", $nmfile5);
            $nmfile5 = trim($nmfile5);

            $config5['upload_path']         = './upload/klaim_peralihan/';
            $config5['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config5['max_size']            = 50000;
            $config5['overwrite']           = true;
            $config5['file_name']           = $nmfile5;

            $this->load->library('upload', $config5);
            $this->upload->initialize($config5);

            $this->upload->do_upload('fc_realisasi_pencairan_pembiayaan');
            $this->upload->data();
        } else {
            $nmfile5 = $get->fc_realisasi_pencairan_pembiayaan;
        }

        $upload6    = isset($_FILES['fc_rekening']['name']) ? $_FILES['fc_rekening']['name'] : '';
        $ext6        = pathinfo($upload6, PATHINFO_EXTENSION);

        if ($upload6 !== "") {

            $nmfile6 = "FcRekening_" . time() . '.' . $ext6;
            $nmfile6 = str_replace(" ", "_", $nmfile6);
            $nmfile6 = trim($nmfile6);

            $config6['upload_path']         = './upload/klaim_peralihan/';
            $config6['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config6['max_size']            = 50000;
            $config6['overwrite']           = true;
            $config6['file_name']           = $nmfile6;

            $this->load->library('upload', $config6);
            $this->upload->initialize($config6);

            $this->upload->do_upload('fc_rekening');
            $this->upload->data();
        } else {
            $nmfile6 = $get->fc_rekening;
        }

        $upload7    = isset($_FILES['fc_sk_pengakatanpeg']['name']) ? $_FILES['fc_sk_pengakatanpeg']['name'] : '';
        $ext7        = pathinfo($upload7, PATHINFO_EXTENSION);

        if ($upload7 !== "") {

            $nmfile7 = "FcSKPengangkatan_" . time() . '.' . $ext7;
            $nmfile7 = str_replace(" ", "_", $nmfile7);
            $nmfile7 = trim($nmfile7);

            $config7['upload_path']         = './upload/klaim_peralihan/';
            $config7['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config7['max_size']            = 50000;
            $config7['overwrite']           = true;
            $config7['file_name']           = $nmfile7;

            $this->load->library('upload', $config7);
            $this->upload->initialize($config7);

            $this->upload->do_upload('fc_sk_pengakatanpeg');
            $this->upload->data();
        } else {
            $nmfile7 = $get->fc_sk_pengakatanpeg;
        }

        $upload8    = isset($_FILES['sk_meninggal']['name']) ? $_FILES['sk_meninggal']['name'] : '';
        $ext8        = pathinfo($upload8, PATHINFO_EXTENSION);

        if ($upload8 !== "") {

            $nmfile8 = "SKMeninggal_" . time() . '.' . $ext8;
            $nmfile8 = str_replace(" ", "_", $nmfile8);
            $nmfile8 = trim($nmfile8);

            $config8['upload_path']         = './upload/klaim_peralihan/';
            $config8['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config8['max_size']            = 50000;
            $config8['overwrite']           = true;
            $config8['file_name']           = $nmfile8;

            $this->load->library('upload', $config8);
            $this->upload->initialize($config8);

            $this->upload->do_upload('sk_meninggal');
            $this->upload->data();
        } else {
            $nmfile8 = $get->sk_meninggal;
        }

        $upload9    = isset($_FILES['fc_surat_tagihan']['name']) ? $_FILES['fc_surat_tagihan']['name'] : '';
        $ext9        = pathinfo($upload9, PATHINFO_EXTENSION);

        if ($upload9 !== "") {

            $nmfile9 = "FcSuratTagihan_" . time() . '.' . $ext9;
            $nmfile9 = str_replace(" ", "_", $nmfile9);
            $nmfile9 = trim($nmfile9);

            $config9['upload_path']         = './upload/klaim_peralihan/';
            $config9['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config9['max_size']            = 50000;
            $config9['overwrite']           = true;
            $config9['file_name']           = $nmfile9;

            $this->load->library('upload', $config9);
            $this->upload->initialize($config9);

            $this->upload->do_upload('fc_surat_tagihan');
            $this->upload->data();
        } else {
            $nmfile9 = $get->fc_surat_tagihan;
        }

        $upload10   = isset($_FILES['slik_ojk']['name']) ? $_FILES['slik_ojk']['name'] : '';
        $ext10        = pathinfo($upload10, PATHINFO_EXTENSION);

        if ($upload10 !== "") {

            $nmfile10 = "SlikOjk_" . time() . '.' . $ext10;
            $nmfile10 = str_replace(" ", "_", $nmfile10);
            $nmfile10 = trim($nmfile10);

            $config10['upload_path']         = './upload/klaim_peralihan/';
            $config10['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config10['max_size']            = 50000;
            $config10['overwrite']           = true;
            $config10['file_name']           = $nmfile10;

            $this->load->library('upload', $config10);
            $this->upload->initialize($config10);

            $this->upload->do_upload('slik_ojk');
            $this->upload->data();
        } else {
            $nmfile10 = $get->slik_ojk;
        }

        $upload11   = isset($_FILES['sk_terjadi_kebakaran']['name']) ? $_FILES['sk_terjadi_kebakaran']['name'] : '';
        $ext11        = pathinfo($upload11, PATHINFO_EXTENSION);

        if ($upload11 !== "") {

            $nmfile11 = "SKTerjadiKebakaran_" . time() . '.' . $ext11;
            $nmfile11 = str_replace(" ", "_", $nmfile11);
            $nmfile11 = trim($nmfile11);

            $config11['upload_path']         = './upload/klaim_peralihan/';
            $config11['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config11['max_size']            = 50000;
            $config11['overwrite']           = true;
            $config11['file_name']           = $nmfile11;

            $this->load->library('upload', $config11);
            $this->upload->initialize($config11);

            $this->upload->do_upload('sk_terjadi_kebakaran');
            $this->upload->data();
        } else {
            $nmfile11 = $get->sk_terjadi_kebakaran;
        }

        $upload12   = isset($_FILES['syarat_lain']['name']) ? $_FILES['syarat_lain']['name'] : '';
        $ext12        = pathinfo($upload12, PATHINFO_EXTENSION);

        if ($upload12 !== "") {

            $nmfile12 = "SyaratLain_" . time() . '.' . $ext12;
            $nmfile12 = str_replace(" ", "_", $nmfile12);
            $nmfile12 = trim($nmfile12);

            $config12['upload_path']         = './upload/klaim_peralihan/';
            $config12['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config12['max_size']            = 50000;
            $config12['overwrite']           = true;
            $config12['file_name']           = $nmfile12;

            $this->load->library('upload', $config12);
            $this->upload->initialize($config12);

            $this->upload->do_upload('syarat_lain');
            $this->upload->data();
        } else {
            $nmfile12 = $get->syarat_lain;
        }

        $dadang = [
            'surat_pengajuan_klaim'                 => $nmfile1,
            'fc_ktp'                                => $nmfile2,
            'fc_kk'                                 => $nmfile3,
            'fc_akad'                               => $nmfile4,
            'fc_realisasi_pencairan_pembiayaan'     => $nmfile5,
            'fc_rekening'                           => $nmfile6,
            'fc_sk_pengakatanpeg'                   => $nmfile7,
            'sk_meninggal'                          => $nmfile8,
            'fc_surat_tagihan'                      => $nmfile9,
            'slik_ojk'                              => $nmfile10,
            'sk_terjadi_kebakaran'                  => $nmfile11,
            'syarat_lain'                           => $nmfile12,
            'keterangan'                            => $keterangan,
            'status_klaim'                          => $status_klaim
        ];

        $this->db->where('id', $id);
        $this->db->update('tm_pengajuan_klaim_peralihan', $dadang);

        $dadang = [
            'id_pengajuan_klaim'    => $id,
            'app_2_status'          => $status_klaim,
            'app_2_keterangan'      => 'Data telah disubmit oleh ' . $this->session->userdata('nama'),
            'app_2_create_by'       => $this->session->userdata('id_user'),
            'app_2_create_date'     => date('Y-m-d H:i:s')
        ];

        $this->db->where('id_pengajuan_klaim', $id);
        $this->db->update('tt_pengajuan_klaim_approval_peralihan', $dadang);
    }

    function save_pembayaranklaim()
    {
        extract($_POST);

        $cek = $this->db->query("select * from tm_pengajuan_klaim_peralihan where id = $id_pengajuan_klaim")->row();

        $upload12 = isset($_FILES['bukti_pembayaran']['name']) ? $_FILES['bukti_pembayaran']['name'] : '';

        if ($upload12 !== "") {

            $nmfile12 = "BuktiPembayran_" . time() . $upload12;
            $nmfile12 = str_replace(" ", "_", $nmfile12);
            $nmfile12 = trim($nmfile12);

            $config12['upload_path']         = './upload/klaim_peralihan/';
            $config12['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config12['max_size']            = 50000;
            $config12['overwrite']           = true;
            $config12['file_name']           = $nmfile12;

            $this->load->library('upload', $config12);
            $this->upload->initialize($config12);

            $this->upload->do_upload('bukti_pembayaran');
            $this->upload->data();
        } else {
            $nmfile12 = $cek->bukti_pembayaran;
        }

        $dadang = [
            'bukti_pembayaran'              => $nmfile12,
            'nilai_total_pengajuan_klaim'   => str_replace('.', '', $nilai_total_pengajuan_klaim),
            'nilai_persetujuan_klaim'       => str_replace('.', '', $nilai_persetujuan_klaim),
            'nilai_klaim_yg_dbayar'         => str_replace('.', '', $nilai_klaim_yg_dbayar),
            'tanggal_pembayaran_klaim'      => date('Y-m-d', strtotime($tanggal_pembayaran_klaim)),
            'status_klaim'                  => 3 //klaim dibayar
        ];

        $this->db->where('id', $id_pengajuan_klaim);
        $this->db->update('tm_pengajuan_klaim_peralihan', $dadang);

        $dadang = [
            'id_pengajuan_klaim'    => $id_pengajuan_klaim,
            'app_2_status'          => 3, //klaim dibayar 
            'app_3_status'          => 3, //klaim dibayar 
            'app_3_keterangan'      => 'Data telah disubmit oleh ' . $this->session->userdata('nama'),
            'app_3_create_by'       => $this->session->userdata('id_user'),
            'app_3_create_date'     => date('Y-m-d H:i:s')
        ];

        $this->db->where('id_pengajuan_klaim', $id_pengajuan_klaim);
        $this->db->update('tt_pengajuan_klaim_approval_peralihan', $dadang);
    }

    function statusdokwi($id) //with id
    {

        $data['d']            = $this->Klaim_peralihan_m->klaim_peralihan($id)->row();
        $this->load->view('klaim_peralihan/statusdok', $data);
    }

    function statusdokni() //non id
    {

        $data['d']            = $this->Klaim_peralihan_m->pengajuan_klaim_noid()->row();
        $this->load->view('klaim_peralihan/statusdok', $data);
    }

    public function j_klaim($norek)
    {

        $merk = $this->Klaim_peralihan_m->j_klaim($norek)->result();

        $lists = "<option></option>";
        foreach ($merk as $data) {
            $lists .= "<option value='" . $data->id . "'>" . $data->jenis_klaim . "</option>";
        }
        $callback = array('jenis_klaimna' => $lists);
        echo json_encode($callback);
    }

    function printSuratPengajuan($id)
    {
        $data['q']  = $this->db->query("select * from tm_pengajuan_klaim_peralihan where id = $id")->row();

        // $this->load->library('Pdfgenerator');
        // $file_pdf = 'SuratPengajuanKlaim'.'_'.$data['q']->norek.'';
        // $paper = 'A4';
        // $orientation = "portrait";
        // $html = $this->load->view('print/suratpengajuan',$data, true);     

        // $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);

        if ($data['q']->jenis_klaim == 2 or $data['q']->jenis_klaim == 3) {
            $this->load->view('print/pengajuanKlaim', $data);
        } else {
            $this->load->view('print/formSantunan', $data);
        }
    }

    function getAsu($nama_asuransi)
    {
        $q = $this->db->query("select id from tm_asuransi where asuransi = '" . $nama_asuransi . "' ")->row();

        $message['idna'] = $q->id;

        echo json_encode($message);
    }

    function printKebakaran()
    {
        $this->load->view('print/formKebakaran');
    }

    function uploadData()
    {
        ini_set('max_execution_time', '0'); //300 seconds = 5 minutes
        ini_set('memory_limit', '-1'); // for infinite time of execution 
        extract($_POST);

        $filename                   = "KlaimPeralihan_" . date('Y-m-d') . "";
        $config['upload_path']      = './upload/klaimPeralihan/';
        $config['allowed_types']    = 'xlsx|csv';
        $config['max_size']         = '2000000';
        $config['overwrite']        = true;
        $config['file_name']        = $filename;

        $this->load->library('upload', $config);
        $this->upload->do_upload('dokumen_klaim');
        $this->upload->data();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('./upload/klaimPeralihan/' . $filename . '.xlsx');
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

        $data = array();
        $numrow = 2;
        $validdebitur = 0;
        $nonvaliddebitur = 0;

        foreach ($sheet as $row) {

            if ($numrow > 2) {

                array_push($data, array(
                    'norek'                      => (float)$row['A'],
                    'no_klaim'          => $this->Klaim_peralihan_m->kodena(),
                    'no_cif'                      => (int)$row['B'],
                    'nama_debitur'             => $row['C'],
                    'tanggal_lahir'               => date('Y-m-d', strtotime($row['D'])),
                    'no_ktp'                      => (int)$row['E'],
                    'alamat'                     => $row['F'],
                    'kantor_cabang'                      => $row['G'],
                    'jenis_klaim'                      => $row['H'],
                    'fasilitas_kredit'              => $row['I'],
                    'jangka_waktu_mulai_kredit'                      => date('Y-m-d', strtotime($row['J'])),
                    'jangka_waktu_akhir_kredit'                      => date('Y-m-d', strtotime($row['K'])),
                    'tenor_kredit'                      => $row['L'],
                    'premi_asuransi'                      => $row['M'],
                    'nilai_pokok_pengajuan_klaim'                      => 0,
                    'nilai_plafond_kredit'                      => $row['O'],
                    'sebab_klaim'                      => $row['P'],
                    'create_date'                => date('Y-m-d H:i:s'),
                    'create_by'                 => $this->session->userdata('id_user'),
                    'status_klaim'                => 1,
                ));
            }

            $numrow++;
        }

        $dicek = $this->db->query("select * from tm_pengajuan_klaim_peralihan where norek = '" . (int)$row['A'] . "' ");


        if ($dicek->num_rows() < 1) {

            // var_dump($data);
            // die();
            $q = $this->db->insert_batch('tm_pengajuan_klaim_peralihan', $data);

            $return = array(
                'statuscode' => 'success',
            );

            return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($return));
            die;
        } else {
            $return = array(
                'statuscode' => 'false',
                'keterangan'    => 'Data allready exist'
            );

            return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($return));
            die;
        }
    }

    function uploadDataTambahan()
    {
        ini_set('max_execution_time', '0');
        ini_set("memory_limit", "-1");

        try {
            $createdby = strtoupper($this->session->userdata('id_user'));

            $file = $_FILES['dokumen_tambahan']['name'];


            $fileName = str_replace(' ', '', $file);
            $explode = explode(".", $fileName);
            $date = date("YmdHis");
            $fileexp = $date.'_'.$file;
            $norek = explode("_", $explode[0]);
            $norek = $norek[0];
            $klaim_dir_path = "";

            $test = realpath(FCPATH . 'upload/klaimPeralihan/dokeun');
            $config['upload_path'] = $test;
            $config['file_name'] = $fileexp;
            // $config['allowed_types'] = 'xls|xlsx|csv';
            $config['allowed_types'] = '*';
            $config['max_size'] = 50000;

            $this->load->library('upload');
            $this->upload->initialize($config);

            // var_dump($explode[0].'_'.$date.'.'.$explode[1]);
            // var_dump($file);
            // die();



            if (!$this->upload->do_upload('dokumen_tambahan')) {

                // var_dump($this->upload->display_errors());
                // exit();
                // $this->session->set_flashdata('error', 'Please check your file (.txt)');
                //     redirect('Klaim');
                $returnData = array(
                    "status" => '500', "message" => 'failed',
                   
                );

                header('Content-Type: application/json');
                echo json_encode($returnData, FALSE);
            } else {
                $uploadData = $this->upload->data();
                $filenames = $uploadData['file_name'];

                $datefile = date("Ymd_His");
                ## Extract the zip file ---- start
                $zip = new ZipArchive;
                $res = $zip->open("./upload/klaimPeralihan/dokeun/" . $filenames);
                $fxpath = './upload/klaimPeralihan/dokeun/klaim_' . $datefile;
                if ($res === TRUE) {
                    ## Check directory data
                    if (!file_exists($fxpath)) {
                        mkdir($fxpath, 0777, true);
                    }

                    // Unzip path
                    $extractpath = $fxpath;

                    // Extract file
                    $zip->extractTo($extractpath);
                    $zip->close();

                    $klaim_dir_path = $fxpath;
                }
            }

            $inputFileName = './upload/klaimPeralihan/dokeun/' . $fileexp;

            $DirFiles = array_diff(scandir($klaim_dir_path), array('.', '..'));

            //var_dump($DirFiles);exit();

            date_default_timezone_set('Asia/Jakarta');
            $todays = date("Y-m-d H:i:s");

            $tanggal_pengajuan = date("Y-m-d");
            $batch = date("Y-m", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-1 month"));

            $this->db->trans_off();
            $this->db->trans_begin();
            $this->db->trans_strict(TRUE);
            $returnError = array();
            $returnSuccess = array();
            $successno = 0;
            $errorno = 0;

            foreach ($DirFiles as $value) {
                if (strpos(strtoupper($value), 'MACOSX') === false) {
                    $valdokexp = explode(".", $value);
                    $valexp = explode("_", $valdokexp[0]);
                    $norek = $valexp[0];
                    $filedata = $valexp[1];
                    // $namaReplace = str_replace(' ','',$valexp[1]);
                    // $namaReplace = str_replace("'",'',$namaReplace);
                    // $namaReplace = str_replace(".",'',$namaReplace);
                    // $namaReplace = str_replace(",",'',$namaReplace);
                    // $tgl_kejadian = $valexp[1];
                    // $kolek = $valexp[2];
                    // $filedata = $valexp[3];
                    $tempfilename = $valexp[0] . "_" . $filedata;
                    $dirfile = $klaim_dir_path . "/" . $tempfilename;

                    rename($klaim_dir_path . "/" . $value, $dirfile . ".pdf");

                    $handleKlaim = $this->db->query("SELECT id, nama_debitur FROM tm_pengajuan_klaim_peralihan WHERE norek like '%$norek%' ")->row();
                    if (!empty($handleKlaim)) {
                        $query = "
                                    UPDATE tm_pengajuan_klaim_peralihan
                                    SET

                                ";
                        $dirValue = $dirfile . ".pdf";

                        if ($filedata == 'KTP') {
                            $query .= "file_ktp = '$dirValue' ";
                        } else if ($filedata == 'KK') {
                            $query .= "file_kk = '$dirValue' ";
                        } else if ($filedata == 'AKAD') {
                            $query .= "file_akad = '$dirValue' ";
                        } else if ($filedata == 'REKENING') {
                            $query .= "file_norek = '$dirValue' ";
                        } else if ($filedata == 'REALISASI') {
                            $query .= "file_realisasi = '$dirValue' ";
                        } else if ($filedata == 'SK') {
                            $query .= "file_skpengangkatan = '$dirValue' ";
                        } else if ($filedata == 'SURATPENGAJUAN') {
                            $query .= "file_surat_pengajuan = '$dirValue' ";
                        } else if ($filedata == 'SKMENINGGAL') {
                            $query .= "file_meninggal = '$dirValue' ";
                        } else if ($filedata == 'STAGIHAN') {
                            $query .= "file_macet = '$dirValue' ";
                        } else if ($filedata == 'SKKEBAKARAN') {
                            $query .= "file_kebakaran = '$dirValue' ";
                        } else if ($filedata == 'SLIK') {
                            $query .= "file_slik = '$dirValue' ";
                        }


                        $query .= " WHERE id = '$handleKlaim->id' ";

                        $this->db->query($query);

                        $statusklaimna = $this->db->query("select * from tm_pengajuan_klaim_peralihan where id = '".$handleKlaim->id."' ")->row();

                        if ($statusklaimna->jenis_klaim == 1) {
                            if ($statusklaimna->file_surat_pengajuan == null && 
                                $statusklaimna->file_ktp == null && 
                                $statusklaimna->file_kk == null && 
                                $statusklaimna->file_akad == null && 
                                $statusklaimna->file_norek == null && 
                                $statusklaimna->file_realisasi == null && 
                                $statusklaimna->file_skpengangkatan == null && 
                                $statusklaimna->file_meninggal == null ) {
                                $status_klaim = 2; //semua dokumen belum dikirim 
                            } else if ($statusklaimna->file_ktp == null or 
                                $statusklaimna->file_kk == null or 
                                $statusklaimna->file_akad == null or 
                                $statusklaimna->file_norek == null or 
                                $statusklaimna->file_realisasi == null or 
                                $statusklaimna->file_skpengangkatan == null or 
                                $statusklaimna->file_surat_pengajuan == null or 
                                $statusklaimna->file_meninggal == null ) {
                                $status_klaim = 3; //dokumen belum lengkap
                            } else {
                                $status_klaim = 4; //menunggu persetujuan asuransi
                            }
                        } else if ($statusklaimna->jenis_klaim == 2) {
                            if ($statusklaimna->file_ktp == null && 
                                $statusklaimna->file_kk == null && 
                                $statusklaimna->file_akad == null && 
                                $statusklaimna->file_norek == null && 
                                $statusklaimna->file_realisasi == null && 
                                $statusklaimna->file_skpengangkatan == null && 
                                $statusklaimna->file_surat_pengajuan == null && 
                                $statusklaimna->file_kebakaran == null ) {
                                $status_klaim = 2;
                            } else if ($statusklaimna->file_ktp == null or 
                                $statusklaimna->file_kk == null or 
                                $statusklaimna->file_akad == null or 
                                $statusklaimna->file_norek == null or 
                                $statusklaimna->file_realisasi == null or 
                                $statusklaimna->file_skpengangkatan == null or 
                                $statusklaimna->file_surat_pengajuan == null or 
                                $statusklaimna->file_kebakaran == null ) {
                                $status_klaim = 3;
                            } else {
                                $status_klaim = 4;
                            }
                        } else if ($statusklaimna->jenis_klaim == 3) {
                            if ($statusklaimna->file_ktp == null && 
                                $statusklaimna->file_kk == null && 
                                $statusklaimna->file_akad == null && 
                                $statusklaimna->file_norek == null && 
                                $statusklaimna->file_realisasi == null && 
                                $statusklaimna->file_skpengangkatan == null && 
                                $statusklaimna->file_surat_pengajuan == null && 
                                $statusklaimna->file_macet == null && 
                                $statusklaimna->file_slik == null ) {
                                $status_klaim = 2;
                            } else if ($statusklaimna->file_ktp == null or 
                                $statusklaimna->file_kk == null or 
                                $statusklaimna->file_akad == null or 
                                $statusklaimna->file_norek == null or 
                                $statusklaimna->file_realisasi == null or 
                                $statusklaimna->file_skpengangkatan == null or
                                $statusklaimna->file_surat_pengajuan == null or 
                                $statusklaimna->file_macet == null or 
                                $statusklaimna->file_slik == null) {
                                $status_klaim = 3;
                            } else {
                                $status_klaim = 4;
                            }
                        }

                        $klaimna = [
                            'status_klaim'  => $status_klaim
                        ];

                        $this->db->where('id',$handleKlaim->id);
                        $this->db->update('tm_pengajuan_klaim_peralihan',$klaimna);


                        
                    }

                }
            }

            $dadang = [
                'id_pengajuan_klaim'    => $handleKlaim->id,
                'app_2_status'          => $status_klaim,
                'app_2_keterangan'      => 'Data telah disubmit oleh ' . $this->session->userdata('nama'),
                'app_2_create_by'       => $this->session->userdata('id_user'),
                'app_2_create_date'     => date('Y-m-d H:i:s')
            ];

            $cekhela = $this->db->query("select * from tt_pengajuan_klaim_approval_peralihan where id_pengajuan_klaim = ".$handleKlaim->id." ");

            if ($cekhela->num_rows() > 0) {
                $this->db->where('id_pengajuan_klaim', $handleKlaim->id);
                $this->db->insert('tt_pengajuan_klaim_approval_peralihan', $dadang);
            } else {
                $this->db->insert('tt_pengajuan_klaim_approval_peralihan', $dadang);
            }


            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $returnData = array(
                    "status" => '99', "message" => 'Error',
                    "dataError" => [], "dataSuccess" => [], "errorCount" => 0, "successCount" => 0
                );

                header('Content-Type: application/json');
                echo json_encode($returnData, FALSE);
            } else {

                $this->db->trans_commit();

                $data_session = array(
                    'repKlaimSuccess_' . $createdby => $returnSuccess,
                    'repKlaimError_' . $createdby => $returnError
                );
                $this->session->set_userdata($data_session);

                $returnData = array(
                    "status" => '00', "message" => 'Success',
                    "dataError" => $returnError, "dataSuccess" => $returnSuccess, "errorCount" => $errorno, "successCount" => $successno
                );

                header('Content-Type: application/json');
                echo json_encode($returnData, FALSE);
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();

            // $this->writeLog('UPLOAD-DATA-KLAIM','ERROR',TR_DATA,NULL,NULL);

            throw new DomainException($e->getMessage(), 0, $e);
        }
    }
}
