<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klaim extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Klaim_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');       
        }
    }

	function index()
	{
        $data['title']  		= "Klaim";
        $data['sub_menu']       = 2;
        $data['page_id']        = 3;

		$this->template->load('template','klaim/index',$data);
	}

    function getData()
    {
        header('Content-Type: application/json');

        $results = $this->Klaim_m->getDataTable();
        $data = [];
        $no = $_POST['start'];
        foreach ($results as $val) {

            if ($this->session->userdata('id_groups') == 3 or $this->session->userdata('id_groups') == 4 or $this->session->userdata('id_groups') == 5) {
                if ($val->status_klaim == 1) {
                    $statusna = '<span class="badge bg-soft-primary text-primary"><i class=" ri-loader-2-fill fa-spin font-size-16 align-middle"></i> Pengajuan Klaim</span>';
                } else if ($val->status_klaim == 2) {
                    $statusna = '<span class="badge bg-soft-warning text-danger"><i class=" ri-loader-2-fill fa-spin font-size-16 align-middle me-2"></i> Proses Klaim</span>';
                } else if ($val->status_klaim == 3) {
                    $statusna = '<span class="badge bg-soft-warning text-danger"><i class=" ri-loader-2-fill fa-spin font-size-16 align-middle"></i> Proses Klaim</span>';
                } else if ($val->status_klaim == 4) {
                    $statusna = '<span class="badge bg-soft-warning text-danger"><i class=" ri-loader-2-fill fa-spin font-size-16 align-middle"></i> Proses Klaim</span>';
                } else if ($val->status_klaim == 5) {
                    $statusna = '<span class="badge bg-soft-success text-success"><i class="ri-check-double-fill font-size-16 align-middle"></i> Klaim Paid</span>';
                } else if ($val->status_klaim == 6) {
                    $statusna = '<span class="badge bg-soft-danger text-danger"><i class="ri-spam-3-line font-size-16 align-middle"></i> Klaim Ditolak</span>';
                } else if ($val->status_klaim == 7) {
                    $statusna = '<span class="badge bg-soft-primary text-primary"><i class="ri-question-line font-size-16 align-middle"></i> Klaim Banding</span>';
                }
            } else {
                if ($val->status_klaim == 1) {
                    $statusna = '<span class="badge bg-soft-primary text-primary"><i class=" ri-loader-2-fill bx-spin font-size-16 align-middle"></i> Pengajuan Klaim</span>';
                } else if ($val->status_klaim == 2) {
                    $statusna = '<span class="badge bg-soft-warning text-danger"><i class="ri-spam-2-line font-size-16 align-middle me-2"></i> Semua Dokumen Belum Dikirim</span>';
                } else if ($val->status_klaim == 3) {
                    $statusna = '<span class="badge bg-soft-warning text-danger"><i class="ri-spam-2-line font-size-16 align-middle"></i> Dokumen Belum Lengkap</span>';
                } else if ($val->status_klaim == 4) {
                    $statusna = '<span class="badge bg-soft-warning text-danger"><i class="ri-refresh-line fa-spin font-size-16 align-middle"></i> Menunggu Persetujuan Asuransi</span>';
                } else if ($val->status_klaim == 5) {
                    $statusna = '<span class="badge bg-soft-success text-success"><i class="ri-check-double-fill font-size-16 align-middle"></i> Klaim Paid</span>';
                } else if ($val->status_klaim == 6) {
                    $statusna = '<span class="badge bg-soft-danger text-danger"><i class="ri-spam-3-line font-size-16 align-middle"></i> Klaim Ditolak</span>';
                } else if ($val->status_klaim == 7) {
                    $statusna = '<span class="badge bg-soft-primary text-primary"><i class="ri-question-linefont-size-16 align-middle"></i> Klaim Banding</span>';
                }
            }

            

            if ($val->app_1_status == 1 && $val->app_2_status == 1 && $val->app_3_status == 3) {
                $statusnya = '<span class="badge bg-soft-success text-success"><i class="bx bx-check-shield font-size-16 align-middle" style="color:green" ></i> Approved</span>';
            } else if ($val->app_1_status == 1 && $val->app_2_status == 2) {
                $statusnya = '<span class="badge bg-soft-danger text-danger"><i class="bx bx-block font-size-16 align-middle me-2"></i> Klaim Ditolak</span>';
            } else {
                $statusnya = '<span class="badge bg-soft-warning text-warning"><i class="bx bx-loader bx-spin font-size-16 align-middle" style="color:warning" ></i> Pending</span>';
            }

            $row = array();
            $row[] = 
                    '<a type="button" href="'.base_url('klaim/edit/'.$val->id).'" class="btn btn-sm btn-primary waves-effect waves-light">
                        <i class="dripicons-pencil"></i> Edit
                    </a>';

            // $row[] = 
            //         ($val->surat_pengajuan_klaim and
            //         $val->fc_ktp and 
            //         $val->fc_kk and 
            //         $val->fc_akad and 
            //         $val->fc_realisasi_pencairan_pembiayaan and 
            //         $val->fc_rekening and 
            //         $val->fc_sk_pengakatanpeg or 
            //         $val->sk_meninggal or 
            //         $val->fc_surat_tagihan or 
            //         $val->slik_ojk or 
            //         $val->sk_terjadi_kebakaran ) == null ? '<a type="button" class="btn btn-sm btn-soft-light waves-effect waves-light" data-bs-toggle="modal" onclick="cekDok('."'".$val->id."'".')"><i class="dripicons-warning font-size-16 align-middle " style="color:red"></i> Dokumen Belum Lengkap </a>' : '<a type="button" class="btn btn-sm btn-soft-success waves-effect waves-success" onclick="cekDok('."'".$val->id."'".')"><i class="bx bx-check-double font-size-16 align-middle" style="color:green"></i> Dokumen Lengkap</a>';

            $row[] = $statusna; 
            $row[] = $val->no_klaim;
            $row[] = date('d-m-Y',strtotime($val->create_date));
            $row[] = $val->norek;
            $row[] = $val->nama_debitur;
            $row[] = $val->asuransi;
            $row[] = $val->kantor_cabang;
            $row[] = number_format($val->nilai_total_pengajuan_klaim);
            $row[] = $val->no_akad;
            $row[] = $val->no_sertifikat_polis;
            $row[] = $val->no_cif;
            $row[] = date('d F Y',strtotime($val->tanggal_lahir));
            // $row[] = $statusnya ;
            

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Klaim_m->count_all(),
            "recordsFiltered"   => $this->Klaim_m->count_filtered(),
            "data"              => $data
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($output));

    }

    function add()
    {
        $data['title']          = "Klaim";
        $data['sub_menu']       = 2;
        $data['page_id']        = 3;

        $data['kodena']         = $this->Klaim_m->kodena();

        $this->template->load('template','klaim/add',$data);
    }

    function save()
    {
        extract($_POST);

        $ceks = $this->db->query("select status_klaim from tm_pengajuan_klaim where no_klaim = '".$no_klaim."' ");

        // var_dump($ceks);
        // die();

        if ($ceks->num_rows() < 1) {
            $statusNa = 1;
        } else {
            $statusNa = $ceks->row()->status_klaim;
        }

            $data = [
                'no_klaim'                          => $no_klaim,
                'norek'                             => $norek,
                'no_akad'                           => $no_akad,
                'no_cif'                            => $no_cif,
                // 'no_ld'                             => $no_ld,
                'alamat'                            => $alamat,
                'sebab_klaim'                       => $sebab_klaim,
                'nama_debitur'                      => $nama_debitur,
                'tanggal_lahir'                     => date('Y-m-d',strtotime($tanggal_lahir)),
                'no_ktp'                            => $no_ktp,
                'no_sertifikat_polis'               => $no_sertifikat_polis,
                'kantor_cabang'                     => $kantor_cabang,
                'nama_perusahaan_asuransi'          => $nama_perusahaan_asuransi,
                'jenis_klaim'                       => $jenis_klaim,
                'fasilitas_kredit'                  => $fasilitas_kredit,
                'tanggal_kejadian'                  => date('Y-m-d',strtotime($tanggal_kejadian)),
                'jangka_waktu_mulai_kredit'         => date('Y-m-d',strtotime($jangka_waktu_mulai_kredit)),
                'jangka_waktu_akhir_kredit'         => date('Y-m-d',strtotime($jangka_waktu_akhir_kredit)),
                'tenor_kredit'                      => $tenor_kredit,
                'premi_asuransi'                    => str_replace('.','',$premi_asuransi),
                'nilai_plafond_kredit'              => str_replace('.','',$nilai_plafond_kredit),
                'nilai_pokok_pengajuan_klaim'       => str_replace('.','',$nilai_pokok_pengajuan_klaim),
                // 'nilai_bunga_pengajuan_klaim'       => str_replace('.','',$nilai_bunga_pengajuan_klaim),
                'nilai_total_pengajuan_klaim'       => (int)str_replace('.','',$nilai_total_pengajuan_klaim),
                'status_klaim'                      => $statusNa, //diajukan
                'create_by'                         => $this->session->userdata('id_user'),
                'create_date'                        => date('Y-m-d H:i:s')
            ];

            // var_dump($data);
            // die();

            $cek = $this->db->query("select * from tm_pengajuan_klaim where norek = '$norek' ")->row();

            if (isset($cek->norek)) {
                $this->db->where('norek',$norek);
                $this->db->update('tm_pengajuan_klaim',$data);

            } else {
                $this->db->insert('tm_pengajuan_klaim',$data);
                $idna = $this->db->insert_id();

                $dadang = [
                    'id_pengajuan_klaim'    => $idna,
                    'app_1_status'          => $statusNa, //telah disubmit
                    'app_1_keterangan'      => 'Data telah disubmit oleh '.$this->session->userdata('nama'),
                    'app_1_create_by'       => $this->session->userdata('id_user'),
                    'app_1_create_date'     => date('Y-m-d H:i:s')
                ];

                // var_dump($dadang);
                // die();

                if ($this->db->insert('tt_pengajuan_klaim_approval',$dadang)) {
                   $message['status'] = 'success insert persetujuan klaim';
                   $message['idna']     = $idna;

                   // var_dump($message);
                   // die();
                } else {
                    $message['status'] = 'failed insert persetujuan klaim';
                }

                $this->output->set_content_type('application/json')->set_output(json_encode($message));
            }
        // }


    }

    function edit($id)
    {
        $data['title']          = "Form Pengajuan Klaim";
        $data['sub_menu']       = 2;
        $data['page_id']        = 3;    

        $data['statusKlaim']        = $this->Klaim_m->statusKlaim()->result_array();
        $data['d']              = $this->Klaim_m->klaim($id)->row();

        // echo "<pre>";
        // var_dump($data['d']);
        // die();
        // echo "</pre>";

        // $data['noklaim']        = $this->Klaim_m->kodena();

        $this->template->load('template','klaim/edit',$data);
    }

    function save_persetujuandokumen($idklaim)
    {
        extract($_POST);

        $get = $this->db->query("select * from tm_dokumen_klaim where idklaim = '".$idklaim."' ")->row();

        $upload1    = isset($_FILES['surat_pengajuan_klaim']['name']) ? $_FILES['surat_pengajuan_klaim']['name'] : '';
        $ext1        = pathinfo($upload1, PATHINFO_EXTENSION);

        if($upload1 !== ""){
            
            $nmfile1 = "SuratPermohonKlaim_".time().'.'.$ext1;
            $nmfile1 = str_replace(" ", "_", $nmfile1);
            $nmfile1 = trim($nmfile1);

            $config1['upload_path']         = './upload/klaim/';
            $config1['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config1['max_size']            = 50000;
            $config1['overwrite']           = true;
            $config1['file_name']           = $nmfile1;

            $this->load->library('upload', $config1);
            $this->upload->initialize($config1);

            $this->upload->do_upload('surat_pengajuan_klaim');
            $this->upload->data();

        } else {
            $nmfile1 = !empty($get->surat_pengajuan_klaim) ? $get->surat_pengajuan_klaim : null;
        }

        $upload2    = isset($_FILES['berita_acara_klaim']['name']) ? $_FILES['berita_acara_klaim']['name'] : '';
        $ext2        = pathinfo($upload2, PATHINFO_EXTENSION);

        if($upload2 !== ""){
            
            $nmfile2 = "BeritaAcaraKlaim_".time().'.'.$ext2;
            $nmfile2 = str_replace(" ", "_", $nmfile2);
            $nmfile2 = trim($nmfile2);

            $config2['upload_path']         = './upload/klaim/';
            $config2['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config2['max_size']            = 50000;
            $config2['overwrite']           = true;
            $config2['file_name']           = $nmfile2;

            $this->load->library('upload', $config2);
            $this->upload->initialize($config2);

            $this->upload->do_upload('berita_acara_klaim');
            $this->upload->data();

        } else {
            $nmfile2 = !empty($get->berita_acara_klaim) ? $get->berita_acara_klaim : null;
        }

        $upload3    = isset($_FILES['sertifikat_penjaminan']['name']) ? $_FILES['sertifikat_penjaminan']['name'] : '';
        $ext3        = pathinfo($upload3, PATHINFO_EXTENSION);

        if($upload3 !== ""){
            
            $nmfile3 = "SertifikatPenjaminan_".time().'.'.$ext3;
            $nmfile3 = str_replace(" ", "_", $nmfile3);
            $nmfile3 = trim($nmfile3);

            $config3['upload_path']         = './upload/klaim/';
            $config3['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config3['max_size']            = 50000;
            $config3['overwrite']           = true;
            $config3['file_name']           = $nmfile3;

            $this->load->library('upload', $config3);
            $this->upload->initialize($config3);

            $this->upload->do_upload('sertifikat_penjaminan');
            $this->upload->data();

        } else {
            $nmfile3 = !empty($get->sertifikat_penjaminan) ? $get->sertifikat_penjaminan : null;
        }

        $upload4    = isset($_FILES['ktp']['name']) ? $_FILES['ktp']['name'] : '';
        $ext4        = pathinfo($upload4, PATHINFO_EXTENSION);

        if($upload4 !== ""){
            
            $nmfile4 = "KTP_".time().'.'.$ext4;
            $nmfile4 = str_replace(" ", "_", $nmfile4);
            $nmfile4 = trim($nmfile4);

            $config4['upload_path']         = './upload/klaim/';
            $config4['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config4['max_size']            = 50000;
            $config4['overwrite']           = true;
            $config4['file_name']           = $nmfile4;

            $this->load->library('upload', $config4);
            $this->upload->initialize($config4);

            $this->upload->do_upload('ktp');
            $this->upload->data();

        } else {
            $nmfile4 = !empty($get->ktp) ? $get->ktp : null;
        }

        $upload5    = isset($_FILES['kartu_pegawai']['name']) ? $_FILES['kartu_pegawai']['name'] : '';
        $ext5        = pathinfo($upload5, PATHINFO_EXTENSION);

        if($upload5 !== ""){
            
            $nmfile5 = "KartuPegawai_".time().'.'.$ext5;
            $nmfile5 = str_replace(" ", "_", $nmfile5);
            $nmfile5 = trim($nmfile5);

            $config5['upload_path']         = './upload/klaim/';
            $config5['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config5['max_size']            = 50000;
            $config5['overwrite']           = true;
            $config5['file_name']           = $nmfile5;

            $this->load->library('upload', $config5);
            $this->upload->initialize($config5);

            $this->upload->do_upload('kartu_pegawai');
            $this->upload->data();

        } else {
            $nmfile5 = !empty($get->kartu_pegawai) ? $get->kartu_pegawai : null;
        }

        $upload6    = isset($_FILES['rekening']['name']) ? $_FILES['rekening']['name'] : '';
        $ext6        = pathinfo($upload6, PATHINFO_EXTENSION);

        if($upload6 !== ""){
            
            $nmfile6 = "Rekening_".time().'.'.$ext6;
            $nmfile6 = str_replace(" ", "_", $nmfile6);
            $nmfile6 = trim($nmfile6);

            $config6['upload_path']         = './upload/klaim/';
            $config6['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config6['max_size']            = 50000;
            $config6['overwrite']           = true;
            $config6['file_name']           = $nmfile6;

            $this->load->library('upload', $config6);
            $this->upload->initialize($config6);

            $this->upload->do_upload('rekening');
            $this->upload->data();

        } else {
            $nmfile6 = !empty($get->rekening) ? $get->rekening : null;
        }

        $upload7    = isset($_FILES['ket_kolektibilitas']['name']) ? $_FILES['ket_kolektibilitas']['name'] : '';
        $ext7        = pathinfo($upload7, PATHINFO_EXTENSION);

        if($upload7 !== ""){
            
            $nmfile7 = "KetKolektibilitas_".time().'.'.$ext7;
            $nmfile7 = str_replace(" ", "_", $nmfile7);
            $nmfile7 = trim($nmfile7);

            $config7['upload_path']         = './upload/klaim/';
            $config7['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config7['max_size']            = 50000;
            $config7['overwrite']           = true;
            $config7['file_name']           = $nmfile7;

            $this->load->library('upload', $config7);
            $this->upload->initialize($config7);

            $this->upload->do_upload('ket_kolektibilitas');
            $this->upload->data();

        } else {
            $nmfile7 = !empty($get->ket_kolektibilitas) ? $get->ket_kolektibilitas : null;
        }

        $upload8    = isset($_FILES['perjanjian_kredit']['name']) ? $_FILES['perjanjian_kredit']['name'] : '';
        $ext8        = pathinfo($upload8, PATHINFO_EXTENSION);

        if($upload8 !== ""){
            
            $nmfile8 = "PerjanjianKredit_".time().'.'.$ext8;
            $nmfile8 = str_replace(" ", "_", $nmfile8);
            $nmfile8 = trim($nmfile8);

            $config8['upload_path']         = './upload/klaim/';
            $config8['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config8['max_size']            = 50000;
            $config8['overwrite']           = true;
            $config8['file_name']           = $nmfile8;

            $this->load->library('upload', $config8);
            $this->upload->initialize($config8);

            $this->upload->do_upload('perjanjian_kredit');
            $this->upload->data();

        } else {
            $nmfile8 = !empty($get->perjanjian_kredit) ? $get->perjanjian_kredit : null;
        }

        $upload9    = isset($_FILES['kartu_pinjaman']['name']) ? $_FILES['kartu_pinjaman']['name'] : '';
        $ext9        = pathinfo($upload9, PATHINFO_EXTENSION);

        if($upload9 !== ""){
            
            $nmfile9 = "KartuPinjaman_".time().'.'.$ext9;
            $nmfile9 = str_replace(" ", "_", $nmfile9);
            $nmfile9 = trim($nmfile9);

            $config9['upload_path']         = './upload/klaim/';
            $config9['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config9['max_size']            = 50000;
            $config9['overwrite']           = true;
            $config9['file_name']           = $nmfile9;

            $this->load->library('upload', $config9);
            $this->upload->initialize($config9);

            $this->upload->do_upload('kartu_pinjaman');
            $this->upload->data();

        } else {
            $nmfile9 = !empty($get->kartu_pinjaman) ? $get->kartu_pinjaman : null;
        }

        $upload10   = isset($_FILES['berkas_lain']['name']) ? $_FILES['berkas_lain']['name'] : '';
        $ext10        = pathinfo($upload10, PATHINFO_EXTENSION);

        if($upload10 !== ""){
            
            $nmfile10 = "BerkasLain_".time().'.'.$ext10;
            $nmfile10 = str_replace(" ", "_", $nmfile10);
            $nmfile10 = trim($nmfile10);

            $config10['upload_path']         = './upload/klaim/';
            $config10['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config10['max_size']            = 50000;
            $config10['overwrite']           = true;
            $config10['file_name']           = $nmfile10;

            $this->load->library('upload', $config10);
            $this->upload->initialize($config10);

            $this->upload->do_upload('berkas_lain');
            $this->upload->data();

        } else {
            $nmfile10 = !empty($get->berkas_lain) ? $get->berkas_lain : null;
        }

        $upload11   = isset($_FILES['surat_klaim']['name']) ? $_FILES['surat_klaim']['name'] : '';
        $ext11        = pathinfo($upload10, PATHINFO_EXTENSION);

        if($upload11 !== ""){
            
            $nmfile11 = "SuratKlaim_".time().'.'.$ext11;
            $nmfile11 = str_replace(" ", "_", $nmfile11);
            $nmfile11 = trim($nmfile11);

            $config11['upload_path']         = './upload/klaim/';
            $config11['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config11['max_size']            = 50000;
            $config11['overwrite']           = true;
            $config11['file_name']           = $nmfile11;

            $this->load->library('upload', $config11);
            $this->upload->initialize($config11);

            $this->upload->do_upload('surat_klaim');
            $this->upload->data();

        } else {
            $nmfile11 = !empty($get->surat_klaim) ? $get->surat_klaim : null;
        }

        $upload12   = isset($_FILES['polis_asuransi_kredit']['name']) ? $_FILES['polis_asuransi_kredit']['name'] : '';
        $ext12        = pathinfo($upload10, PATHINFO_EXTENSION);

        if($upload12 !== ""){
            
            $nmfile12 = "PolisAsuransiKredit_".time().'.'.$ext12;
            $nmfile12 = str_replace(" ", "_", $nmfile12);
            $nmfile12 = trim($nmfile12);

            $config12['upload_path']         = './upload/klaim/';
            $config12['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config12['max_size']            = 50000;
            $config12['overwrite']           = true;
            $config12['file_name']           = $nmfile12;

            $this->load->library('upload', $config12);
            $this->upload->initialize($config12);

            $this->upload->do_upload('polis_asuransi_kredit');
            $this->upload->data();

        } else {
            $nmfile12 = !empty($get->polis_asuransi_kredit) ? $get->polis_asuransi_kredit : null;
        }

        $upload13   = isset($_FILES['sk_pegawai']['name']) ? $_FILES['sk_pegawai']['name'] : '';
        $ext13        = pathinfo($upload10, PATHINFO_EXTENSION);

        if($upload13 !== ""){
            
            $nmfile13 = "SKPegawai_".time().'.'.$ext13;
            $nmfile13 = str_replace(" ", "_", $nmfile13);
            $nmfile13 = trim($nmfile13);

            $config13['upload_path']         = './upload/klaim/';
            $config13['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config13['max_size']            = 50000;
            $config13['overwrite']           = true;
            $config13['file_name']           = $nmfile13;

            $this->load->library('upload', $config13);
            $this->upload->initialize($config13);

            $this->upload->do_upload('sk_pegawai');
            $this->upload->data();

        } else {
            $nmfile13 = !empty($get->sk_pegawai) ? $get->sk_pegawai : null;
        }

        $upload14   = isset($_FILES['persetujuan_kredit']['name']) ? $_FILES['persetujuan_kredit']['name'] : '';
        $ext14        = pathinfo($upload10, PATHINFO_EXTENSION);

        if($upload14 !== ""){
            
            $nmfile14 = "PersetujuanKredit_".time().'.'.$ext14;
            $nmfile14 = str_replace(" ", "_", $nmfile14);
            $nmfile14 = trim($nmfile14);

            $config14['upload_path']         = './upload/klaim/';
            $config14['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config14['max_size']            = 50000;
            $config14['overwrite']           = true;
            $config14['file_name']           = $nmfile14;

            $this->load->library('upload', $config14);
            $this->upload->initialize($config14);

            $this->upload->do_upload('persetujuan_kredit');
            $this->upload->data();

        } else {
            $nmfile14 = !empty($get->persetujuan_kredit) ? $get->persetujuan_kredit : null;
        }

        $upload15   = isset($_FILES['sk_kematian']['name']) ? $_FILES['sk_kematian']['name'] : '';
        $ext15        = pathinfo($upload10, PATHINFO_EXTENSION);

        if($upload15 !== ""){
            
            $nmfile15 = "PersetujuanKredit_".time().'.'.$ext15;
            $nmfile15 = str_replace(" ", "_", $nmfile15);
            $nmfile15 = trim($nmfile15);

            $config15['upload_path']         = './upload/klaim/';
            $config15['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config15['max_size']            = 50000;
            $config15['overwrite']           = true;
            $config15['file_name']           = $nmfile15;

            $this->load->library('upload', $config15);
            $this->upload->initialize($config15);

            $this->upload->do_upload('sk_kematian');
            $this->upload->data();

        } else {
            $nmfile15 = !empty($get->sk_kematian) ? $get->sk_kematian : null;
        }



        // $upload11   = isset($_FILES['sk_terjadi_kebakaran']['name']) ? $_FILES['sk_terjadi_kebakaran']['name'] : '';
        // $ext11        = pathinfo($upload11, PATHINFO_EXTENSION);

        // if($upload11 !== ""){
            
        //     $nmfile11 = "SKTerjadiKebakaran_".time().'.'.$ext11;
        //     $nmfile11 = str_replace(" ", "_", $nmfile11);
        //     $nmfile11 = trim($nmfile11);

        //     $config11['upload_path']         = './upload/klaim/';
        //     $config11['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
        //     $config11['max_size']            = 50000;
        //     $config11['overwrite']           = true;
        //     $config11['file_name']           = $nmfile11;

        //     $this->load->library('upload', $config11);
        //     $this->upload->initialize($config11);

        //     $this->upload->do_upload('sk_terjadi_kebakaran');
        //     $this->upload->data();

        // } else {
        //     $nmfile11 = $get->sk_terjadi_kebakaran;
        // }

        // $upload12   = isset($_FILES['syarat_lain']['name']) ? $_FILES['syarat_lain']['name'] : '';
        // $ext12        = pathinfo($upload12, PATHINFO_EXTENSION);

        // if($upload12 !== ""){
            
        //     $nmfile12 = "SyaratLain_".time().'.'.$ext12;
        //     $nmfile12 = str_replace(" ", "_", $nmfile12);
        //     $nmfile12 = trim($nmfile12);

        //     $config12['upload_path']         = './upload/klaim/';
        //     $config12['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
        //     $config12['max_size']            = 50000;
        //     $config12['overwrite']           = true;
        //     $config12['file_name']           = $nmfile12;

        //     $this->load->library('upload', $config12);
        //     $this->upload->initialize($config12);

        //     $this->upload->do_upload('syarat_lain');
        //     $this->upload->data();

        // } else {
        //     $nmfile12 = $get->syarat_lain;
        // }



        // if ($this->session->userdata('id_groups') == 3 or $this->session->userdata('id_groups') == 4 or $this->session->userdata('id_groups') == 5) {

        //     if ($get->jenis_klaim == 1) {
        //         if ($nmfile1 == null && 
        //             $nmfile2 == null && 
        //             $nmfile3 == null && 
        //             $nmfile4 == null && 
        //             $nmfile5 == null && 
        //             $nmfile6 == null && 
        //             $nmfile7 == null && 
        //             $nmfile8 == null ) {
        //             $status_klaim = 2; //semua dokumen belum dikirim
        //         } else if ($nmfile1 == null or 
        //             $nmfile2 == null or 
        //             $nmfile3 == null or 
        //             $nmfile4 == null or 
        //             $nmfile5 == null or 
        //             $nmfile6 == null or 
        //             $nmfile7 == null or 
        //             $nmfile8 == null ) {
        //             $status_klaim = 3;
        //         } else {
        //             $status_klaim = 4;
        //         }

        //     } else if ($get->jenis_klaim == 2) {
        //         if ($nmfile1 == null && 
        //             $nmfile2 == null && 
        //             $nmfile3 == null && 
        //             $nmfile4 == null && 
        //             $nmfile5 == null && 
        //             $nmfile6 == null && 
        //             $nmfile7 == null && 
        //             $nmfile11 == null ) {
        //             $status_klaim = 2;

        //         } else if ($nmfile1 != null or 
        //             $nmfile2 == null or 
        //             $nmfile3 == null or 
        //             $nmfile4 == null or 
        //             $nmfile5 == null or 
        //             $nmfile6 == null or 
        //             $nmfile7 == null or 
        //             $nmfile11 == null ) {
        //             $status_klaim = 3;
        //         } else {
        //             $status_klaim = 4;
        //         }

        //     } else if ($get->jenis_klaim == 3) {
        //         if ($nmfile1 == null && 
        //             $nmfile2 == null && 
        //             $nmfile3 == null && 
        //             $nmfile4 == null && 
        //             $nmfile5 == null && 
        //             $nmfile6 == null && 
        //             $nmfile7 == null && 
        //             $nmfile9 == null && 
        //             $nmfile10 == null ) {
        //             $status_klaim = 2;

        //         } else if ($nmfile1 == null or 
        //             $nmfile2 == null or 
        //             $nmfile3 == null or 
        //             $nmfile4 == null or 
        //             $nmfile5 == null or 
        //             $nmfile6 == null or 
        //             $nmfile7 == null or 
        //             $nmfile9 == null or 
        //             $nmfile10 == null ) {
        //             $status_klaim = 3;
        //         } else {
        //             $status_klaim = 4;
        //         }

        //     }

        //     $dadang = [
        //         'idklaim'                               => $idklaim,
        //         'surat_pengajuan_klaim'                 => $nmfile1,
        //         'fc_ktp'                                => $nmfile2,
        //         'fc_kk'                                 => $nmfile3,
        //         'fc_akad'                               => $nmfile4,
        //         'fc_realisasi_pencairan_pembiayaan'     => $nmfile5,
        //         'fc_rekening'                           => $nmfile6,
        //         'fc_sk_pengakatanpeg'                   => $nmfile7,
        //         'sk_meninggal'                          => $nmfile8,
        //         'fc_surat_tagihan'                      => $nmfile9,
        //         'slik_ojk'                              => $nmfile10,
        //         'sk_terjadi_kebakaran'                  => $nmfile11,
        //         'syarat_lain'                           => $nmfile12,
        //         'keterangan'                            => $keterangan,
        //         // 'status_klaim'                          => $status_klaims
        //     ];

        //     // var_dump($dadang);
        //     // die();
        // } 
        // else {
            $dadang = [
                'idklaim'                           => $idklaim,
                'surat_pengajuan_klaim'             => $nmfile1,
                'berita_acara_klaim'                => $nmfile2,
                'sertifikat_penjaminan'             => $nmfile3,
                'ktp'                               => $nmfile4,
                'kartu_pegawai'                     => $nmfile5,
                'rekening'                          => $nmfile6,
                'ket_kolektibilitas'                => $nmfile7,
                'perjanjian_kredit'                 => $nmfile8,
                'kartu_pinjaman'                    => $nmfile9,
                'berkas_lain'                       => $nmfile10,
                'keterangan'                        => $keterangan,
                // 'status_klaim'                          => $status_klaim
            ];
        // }

        // var_dump($dadang);
        // die();

        // if ($sessions == 1) {
        //     $statusKlaimnya = $status_klaims;
        // } else {
        //     $statusKlaimnya = $status_klaims;
        // }

        // var_dump($status_klaim);
        // die();

        if (!empty($get)) {
            $this->db->where('idklaim',$idklaim);
            $this->db->update('tm_dokumen_klaim',$dadang);

            $this->db->query("update tm_pengajuan_klaim set status_klaim = '".$status_klaim."' where id = $idklaim ");
        } else {
            $this->db->insert('tm_dokumen_klaim',$dadang);

            $this->db->query("update tm_pengajuan_klaim set status_klaim = '".$status_klaim."' where id = $idklaim ");
        }

        $dadang = [
            'id_pengajuan_klaim'    => $idklaim,
            'app_2_status'          => $status_klaim, 
            'app_2_keterangan'      => $keterangan,
            'app_2_create_by'       => $this->session->userdata('id_user'),
            'app_2_create_date'     => date('Y-m-d H:i:s')
        ];

        $this->db->where('id_pengajuan_klaim',$idklaim);
        $this->db->update('tt_pengajuan_klaim_approval',$dadang);

    }

    function save_pembayaranklaim()
    {
        extract($_POST);

        $cek = $this->db->query("select * from tm_pengajuan_klaim where id = $id_pengajuan_klaim")->row();

        $upload12 = isset($_FILES['bukti_pembayaran']['name']) ? $_FILES['bukti_pembayaran']['name'] : '';

        if($upload12 !== ""){
            
            $nmfile12 = "BuktiPembayran_".time().$upload12;
            $nmfile12 = str_replace(" ", "_", $nmfile12);
            $nmfile12 = trim($nmfile12);

            $config12['upload_path']         = './upload/klaim/';
            $config12['allowed_types']       = 'gif|jpg|png|jpeg|xls|xlsx|csv|pdf|doc|docx';
            $config12['max_size']            = 50000;
            $config12['overwrite']           = true;
            $config12['file_name']           = $nmfile12;

            $this->load->library('upload', $config12);
            $this->upload->initialize($config12);

            $this->upload->do_upload('bukti_pembayaran');
            $this->upload->data();

        } else {
            $nmfile12 = $cek->bukti_pembayaran;
        }

        $dadang = [
            'bukti_pembayaran'              => $nmfile12,
            'nilai_total_pengajuan_klaim'   => str_replace('.','',$nilai_total_pengajuan_klaim),
            'nilai_persetujuan_klaim'       => str_replace('.','',$nilai_persetujuan_klaim),
            'nilai_klaim_yg_dbayar'         => str_replace('.','',$nilai_klaim_yg_dbayar),
            'tanggal_pembayaran_klaim'      => date('Y-m-d',strtotime($tanggal_pembayaran_klaim)),
            'status_klaim'                  => 3 //klaim dibayar
        ];

        $this->db->where('id',$id_pengajuan_klaim);
        $this->db->update('tm_pengajuan_klaim',$dadang);

        $dadang = [
            'id_pengajuan_klaim'    => $id_pengajuan_klaim,
            'app_2_status'          => 3, //klaim dibayar 
            'app_3_status'          => 3, //klaim dibayar 
            'app_3_keterangan'      => 'Data telah disubmit oleh '.$this->session->userdata('nama'),
            'app_3_create_by'       => $this->session->userdata('id_user'),
            'app_3_create_date'     => date('Y-m-d H:i:s')
        ];

        $this->db->where('id_pengajuan_klaim',$id_pengajuan_klaim);
        $this->db->update('tt_pengajuan_klaim_approval',$dadang);
    }

    function uploadDokumen($id)
    {
        $data['statusKlaim']        = $this->Klaim_m->statusKlaim()->result_array();
        $data['d']  = $this->db->query("select a.*, b.nama_perusahaan_asuransi, b.jenis_klaim from tm_dokumen_klaim a left join tm_pengajuan_klaim b on a.idklaim = b.id where a.idklaim = $id")->row();
        // var_dump($data['d']);
        // die();
        $this->load->view('klaim/uploadDokumen',$data);
    }

}

