<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');       
        }
    }

	function index()
	{
        $data['title']  		= "Main Dashboard";
        $data['sub_menu']       = 1;
        $data['page_id']        = 5;

		$this->template->load('template','beranda/index',$data);
	}

    function permitra()
    {
        $data['title']          = "Klaim Per Mitra Asuransi";
        $data['sub_menu']       = 1;
        $data['page_id']        = 6;

        $this->template->load('template','beranda/permitra',$data);
    }

    function percabang()
    {
        $data['title']          = "Klaim Per Cabang";
        $data['sub_menu']       = 1;
        $data['page_id']        = 7;

        $this->template->load('template','beranda/percabang',$data);
    }

    function klaimditolak()
    {
        $data['title']          = "Klaim Ditolak";
        $data['sub_menu']       = 1;
        $data['page_id']        = 8;

        $this->template->load('template','beranda/klaimditolak',$data);
    }

}

