<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klaim_peralihan_m extends CI_Model
{

	var $table = 'tm_pengajuan_klaim_peralihan';
	var $column_order = array('a.id','a.nama_debitur', 'a.kantor_cabang', 'a.norek', 'a.jangka_waktu_mulai_kredit', 'jangka_waktu_akhir_kredit');
    var $column_search = array('a.id','a.nama_debitur', 'a.kantor_cabang', 'a.norek', 'a.jangka_waktu_mulai_kredit', 'jangka_waktu_akhir_kredit');
    var $order = array('a.id','a.nama_debitur', 'a.kantor_cabang', 'a.norek', 'a.jangka_waktu_mulai_kredit', 'jangka_waktu_akhir_kredit');

	private function _get_datatables_query()
    {   
        extract($_POST);

        $ti     = date('Y-m-d',strtotime($this->input->post('dari')));
        $tepi   = date('Y-m-d',strtotime($this->input->post('sampai')));

        if($this->input->post('region'))
        {
            $this->db->where('f.id_region', trim($this->input->post('region')));
        }

        if($this->input->post('area'))
        {
            $this->db->where('f.id_area', $this->input->post('area') );
        }

        if($this->input->post('cabang'))
        {
            $this->db->where('a.kantor_cabang', trim($this->input->post('cabang')));
        }
        if($this->input->post('statusna'))
        {
            $this->db->where('status_klaim', $this->input->post('statusna'));
        }
        if($this->input->post('nama_debitur'))
        {
            $this->db->like('nama_debitur', $this->input->post('nama_debitur'));
        }
        if($this->input->post('no_cif'))
        {
            $this->db->like('no_cif ', ''.$this->input->post('no_cif').'');
        }
        if($this->input->post('no_akad'))
        {
            $this->db->like('no_akad', ''.$this->input->post('no_akad').'');
        }

        if($this->input->post('norek'))
        {
            $this->db->like('norek', ''.$this->input->post('norek').'');
        }
        if($this->input->post('dari') && $this->input->post('sampai'))
        {
            // $this->db->where('CAST(create_date As date) ', ''.date('Y-m-d',strtotime($this->input->post('dari'))).'');

            $this->db->where("CAST(create_date As date) BETWEEN '".$ti."' AND '".$tepi."' ");
        }

        $this->db->select('a.id as idna, a.status_klaim, a.nama_debitur, a.norek, a.kantor_cabang, a.no_cif, a.tanggal_lahir, a.no_ktp, a.jenis_klaim, b.*');
        $this->db->from('tm_pengajuan_klaim_peralihan a');
        $this->db->join('tt_pengajuan_klaim_approval b ','a.id = b.id_pengajuan_klaim','left');
        $this->db->join('tm_cabang f','a.kantor_cabang = f.kantor_cabang');


        if ($this->session->userdata('id_groups') == 3) { //REGION
            $regionna = $this->session->userdata('id_region');

            $this->db->where('f.id_region', $regionna );
        }
        if ($this->session->userdata('id_groups') == 4) { //AREA
            $areana = $this->session->userdata('id_area');
            $this->db->where('f.id_area', $areana );
        }
        if ($this->session->userdata('id_groups') == 5) { //CABANG
            $cabangna = $this->session->userdata('id_cabang');
            $this->db->where('f.code_cabang', $cabangna );
        }

        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if ($_POST['search']['value']) 
            {

                if ($i === 0) 
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getDataTable()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	function datana()
	{
		$q = $this->db->query("select a.*, b.* from tm_pengajuan_klaim_peralihan a join tm_penutupan b on a.norek = b.norek");
		return $q;
	}

	function pengajuan_klaim($id)
	{
		$q = $this->db->query("select a.*, b.* from tm_pengajuan_klaim_peralihan a 
            left join tt_pengajuan_klaim_approval b on a.id = b.id_pengajuan_klaim
            where a.id = $id");
		return $q;
	}

    function pengajuan_klaim_noid()
    {
        $q = $this->db->query("select a.* from tm_pengajuan_klaim_peralihan a 
            ");
        return $q;
    }

	public function kodena()
	{
	    $this->db->select('RIGHT(tm_pengajuan_klaim_peralihan.no_klaim,5) as kode', FALSE);
	    $this->db->order_by('no_klaim','DESC');    
	    $this->db->limit(1);    
	    $query = $this->db->get('tm_pengajuan_klaim_peralihan');
	        if($query->num_rows() <> 0){      
	             $data = $query->row();
	             $kode = intval($data->kode) + 1; 
	        }
	        else{      
	             $kode = 1;  
	        }
	    $batas = str_pad($kode, 5, "0", STR_PAD_LEFT);    
	    $kodetampil = time().'_'.$batas;
	    return $kodetampil;  
	}

    function status_klaimStep2()
    {
        $q = $this->db->query("select * from tm_status_klaim where id in(2,3,4) ");
        return $q;
    }

    function status_klaimStep3()
    {
        $q = $this->db->query("select * from tm_status_klaim where id in(5,6,7) ");
        return $q;
    }

    function jenis_klaim()
    {
        $q = $this->db->query("select * from tm_jenis_klaim  ");
        return $q;
    }

    function saveValidator($id,$data)
    {
        $this->db->where('id_pengajuan_klaim',$id);
        $this->db->update('tt_pengajuan_klaim_approval',$data);
        return $this->db->affected_rows();
    }

    function asuransi()
    {
        $q = $this->db->query("select * from tm_asuransi where status = 1");
        return $q;
    }

    function klaim_excel()
    {
        $ti     = date('Y-m-d',strtotime($this->input->post('dari')));
        $tepi   = date('Y-m-d',strtotime($this->input->post('sampai')));

        if($this->input->post('kantor_cabang'))
        {
            $this->db->where('a.kantor_cabang', $this->input->post('kantor_cabang'));
        }
        if($this->input->post('statusna')) //status_klaim
        {
            $this->db->where('a.status_klaim', $this->input->post('statusna'));
        }
        if($this->input->post('sampai'))
        {
            $this->db->where("CAST(create_date As date) BETWEEN '".$ti."' AND '".$tepi."' ");
        }

        $this->db->select('a.*, b.asuransi as asuransina, c.jenis_klaim as jenisklaim');
        $this->db->from('tm_pengajuan_klaim_peralihan a');
        $this->db->join('tm_asuransi b','a.nama_perusahaan_asuransi = b.id');
        $this->db->join('tm_jenis_klaim c','a.jenis_klaim = c.id');
        return $this->db->get();

    }

    function os_total()
    {
        $q = $this->db->query("SELECT SUM(nilai_total_pengajuan_klaim) as os_tagihan from tm_pengajuan_klaim_peralihan");
        return $q;
    }

    function deb_total()
    {
        $q = $this->db->query("SELECT count(id) as totaldeb from tm_pengajuan_klaim_peralihan");
        return $q;
    }

    function os_bulan()
    {
        $bulanini = date('m');
        $tahunini = date('Y');
        $q = $this->db->query("SELECT SUM(nilai_total_pengajuan_klaim) as os_tagihan from tm_pengajuan_klaim_peralihan where month(create_date) = '$bulanini' and year(create_date) = '$tahunini'");
        return $q;
    }

    function os_tagihan_peryear()
    {
        $q = $this->db->query("SELECT sum(nilai_total_pengajuan_klaim) os, year(create_date) yearna FROM tm_pengajuan_klaim_peralihan
                    GROUP BY year(create_date) ");
        return $q;
    }

    function klaim_total()
    {
        $q = $this->db->query("select count(id) total from tm_pengajuan_klaim_peralihan ");
        return $q;
    }

    function klaim_bulan()
    {
        $bulanini = date('m');
        $tahunini = date('Y');
        $q = $this->db->query("select count(id) total from tm_pengajuan_klaim_peralihan where month(create_date) = '$bulanini' and year(create_date) = '$tahunini' ");
        return $q;
    }

    function pembayaran_total()
    {
        $q = $this->db->query("SELECT count(nilai_klaim_yg_dbayar) as os_tagihan from tm_pengajuan_klaim_peralihan");
        return $q;
    }

    function pembayaran_bulan()
    {
        $bulanini = date('m');
        $tahunini = date('Y');
        $q = $this->db->query("SELECT SUM(nilai_klaim_yg_dbayar) as os_tagihan from tm_pengajuan_klaim_peralihan where month(create_date) = '$bulanini' and year(create_date) = '$tahunini'");
        return $q;
    }

    function j_klaim($norek)
    {
        $q = $this->db->query("select * from tm_pengajuan_klaim_peralihan where norek = '$norek' ")->row();

        if (isset($q->jenis_klaim) == 1) {
            $qq = $this->db->query("select * from tm_jenis_klaim where id = 2");
        } else if (isset($q->jenis_klaim ) == 2) {
            $qq = $this->db->query("select * from tm_jenis_klaim where id in(1,3)");
        } else if (isset($q->jenis_klaim ) == 3) {
            $qq = $this->db->query("select * from tm_jenis_klaim where id = 2");
        } else {
            $qq = $this->db->query("select * from tm_jenis_klaim");
        }

        return $qq;
    }

    function update($id,$table,$data)
    {
        $this->db->where('id',$id);
        $this->db->update($table,$data);
        return $this->db->affected_rows();
    }

    function sla()
    {
        $q = $this->db->query("SELECT 
        y.asuransi,
        SUM(x.val1) as kurangtilupuluh,
        SUM(x.val2) as tilupuhgeneppuluh,
        SUM(x.val3) as genepsalapan,
        SUM(x.val4) as lebihsalapan
            FROM
                (
                    SELECT 
                        z.nama_perusahaan_asuransi,
                        CASE 
                            WHEN z.Datediff < 30 
                                THEN 1
                            END as val1,

                        CASE 
                            WHEN z.Datediff > 30 AND z.Datediff < 60
                                THEN 1
                                ELSE 0
                            END as val2,

                        CASE 
                            WHEN z.Datediff > 60 AND z.Datediff < 90
                                THEN 1
                                ELSE 0
                            END as val3,
                        
                        CASE 
                            WHEN z.Datediff > 90
                                THEN 1
                                ELSE 0
                            END as val4

                        FROM
                            ( 
                                SELECT nama_perusahaan_asuransi, CASE 
                                    WHEN tanggal_pembayaran_klaim is null THEN
                                        DATEDIFF(day, tanggal_kejadian, GETDATE())
                                    ELSE 
                                        DATEDIFF(day, tanggal_kejadian, tanggal_pembayaran_klaim)
                                    END as Datediff
                                FROM tm_pengajuan_klaim_peralihan
                            ) z
                ) x
                    join tm_asuransi y on x.nama_perusahaan_asuransi = y.id
            WHERE x.nama_perusahaan_asuransi is not null
            GROUP BY y.asuransi");

        return $q;
    }

    function rekap_klaim()
    {
        if (!empty($bulan)) {
            $q = $this->db->query("SELECT 
                         b.id, b.asuransi, month(a.create_date) periode,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 0 and month(aa.create_date) = '09'
                        ) as pengajuan_klaim,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 0 and month(aa.create_date) = '09'
                        ) as nilai_pengajuan_klaim,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim in(1,4) and month(aa.create_date) = '09'
                        ) as proses_klaim,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim in(1,4) and month(aa.create_date) = '09'
                        ) as nilai_proses_klaim,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 3 and month(aa.create_date) = '09'
                        ) as klaim_dibayar,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 3 and month(aa.create_date) = '09'
                        ) as nilai_klaim_dibayar,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 2 and month(aa.create_date) = '09'
                        ) as ditolak,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 2 and month(aa.create_date) = '09'
                        ) as nilai_klaim_ditolak
                FROM tm_pengajuan_klaim_peralihan a
                LEFT JOIN tm_asuransi b on a.nama_perusahaan_asuransi = b.id
                WHERE nama_perusahaan_asuransi is not null and month(a.create_date) = '09'
                GROUP BY b.id, b.asuransi,month(a.create_date)
                ORDER BY b.id ASC");
        } else {
            $q = $this->db->query("SELECT 
                         b.id, b.asuransi, month(a.create_date) periode,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 0 
                        ) as pengajuan_klaim,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 0 
                        ) as nilai_pengajuan_klaim,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim in(1,4) 
                        ) as proses_klaim,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim in(1,4) 
                        ) as nilai_proses_klaim,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 3 
                        ) as klaim_dibayar,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 3 
                        ) as nilai_klaim_dibayar,
                        (
                                SELECT count(aa.id) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 2 
                        ) as ditolak,
                        (
                                SELECT sum(aa.nilai_total_pengajuan_klaim) FROM tm_pengajuan_klaim_peralihan aa
                                WHERE aa.nama_perusahaan_asuransi = b.id AND aa.status_klaim = 2 
                        ) as nilai_klaim_ditolak
                FROM tm_pengajuan_klaim_peralihan a
                LEFT JOIN tm_asuransi b on a.nama_perusahaan_asuransi = b.id
                WHERE nama_perusahaan_asuransi is not null 
                GROUP BY b.id, b.asuransi,month(a.create_date)
                ORDER BY b.id ASC");
        }

        return $q;
    }

    function rekapPenutupan()
    {
        $q = $this->db->query("SELECT 
                     b.id, b.asuransi,a.periode,
                    (
                            SELECT count(aa.id) FROM tm_penutupan aa
                            WHERE aa.nama_asuransi = b.asuransi 
       
                    ) as jumlah_debitur,
                    (
                            SELECT sum(aa.plafond) FROM tm_penutupan aa
                            WHERE aa.nama_asuransi = b.asuransi
       
                    ) as nilai_plafond,
                    (
                            SELECT sum(aa.premi) FROM tm_penutupan aa
                            WHERE aa.nama_asuransi = b.asuransi 
                    ) as nilai_premi
            FROM tm_penutupan a
            LEFT JOIN tm_asuransi b on a.nama_asuransi = b.asuransi
            WHERE nama_asuransi is not null
            GROUP BY b.id, b.asuransi,a.periode
            ORDER BY b.id ASC");

        return $q;

    }
    
}