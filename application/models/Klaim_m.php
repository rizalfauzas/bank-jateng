<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klaim_m extends CI_Model
{

	var $table = 'tm_pengajuan_klaim';
	var $column_order = array('a.id','a.nama_debitur', 'a.kantor_cabang', 'a.no_akad', 'a.jangka_waktu_mulai_kredit', 'jangka_waktu_akhir_kredit');
	var $column_search = array('a.id','a.nama_debitur', 'a.kantor_cabang', 'a.no_akad', 'a.jangka_waktu_mulai_kredit', 'jangka_waktu_akhir_kredit');
	var $order = array('a.id','a.nama_debitur', 'a.kantor_cabang', 'a.no_akad', 'a.jangka_waktu_mulai_kredit', 'jangka_waktu_akhir_kredit');

	private function _get_datatables_query()
    {   
        extract($_POST);

        $ti     = date('Y-m-d',strtotime($this->input->post('dari')));
        $tepi   = date('Y-m-d',strtotime($this->input->post('sampai')));

        if($this->input->post('region'))
        {
            $this->db->where('f.id_region', trim($this->input->post('region')));
        }

        if($this->input->post('area'))
        {
            $this->db->where('f.id_area', $this->input->post('area') );
        }

        if($this->input->post('cabang'))
        {
            $this->db->where('a.kantor_cabang', trim($this->input->post('cabang')));
        }
        if($this->input->post('statusna'))
        {
            $this->db->where('status_klaim', $this->input->post('statusna'));
        }
        if($this->input->post('nama_debitur'))
        {
            $this->db->like('nama_debitur', $this->input->post('nama_debitur'));
        }
        if($this->input->post('no_cif'))
        {
            $this->db->like('no_cif ', ''.$this->input->post('no_cif').'');
        }
        if($this->input->post('no_akad'))
        {
            $this->db->like('no_akad', ''.$this->input->post('no_akad').'');
        }
        if($this->input->post('norek'))
        {
            $this->db->like('norek', ''.$this->input->post('norek').'');
        }
        
        if($this->input->post('dari') && $this->input->post('sampai'))
        {
            $this->db->where("CAST(create_date As date) BETWEEN '".$ti."' AND '".$tepi."' ");
        }

        if ($this->session->userdata('id_groups') == 3) { //REGION
            $regionna = $this->session->userdata('id_region');

            $this->db->where('f.id_region', $regionna );
        }
        if ($this->session->userdata('id_groups') == 4) { //AREA
            $areana = $this->session->userdata('id_area');
            $this->db->where('f.id_area', $areana );
        }
        if ($this->session->userdata('id_groups') == 5) { //CABANG
            $cabangna = $this->session->userdata('id_cabang');
            $this->db->where('f.code_cabang', $cabangna );
        }

        $this->db->select('a.*, b.*, c.asuransi');
        $this->db->from('tm_pengajuan_klaim a');
        $this->db->join('tt_pengajuan_klaim_approval b ','a.id = b.id_pengajuan_klaim','left');
        $this->db->join('tm_asuransi c','a.nama_perusahaan_asuransi = c.id','left');
        $this->db->join('tm_cabang f','a.kantor_cabang = f.kantor_cabang','left');

        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if ($_POST['search']['value']) 
            {

                if ($i === 0) 
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getDataTable()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function kodena()
    {
        $this->db->select('RIGHT(tm_pengajuan_klaim.no_klaim,5) as kode', FALSE);
        $this->db->order_by('no_klaim','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('tm_pengajuan_klaim');
            if($query->num_rows() <> 0){      
                 $data = $query->row();
                 $kode = intval($data->kode) + 1; 
            }
            else{      
                 $kode = 1;  
            }
        $batas = str_pad($kode, 5, "0", STR_PAD_LEFT);    
        $kodetampil = time().'_'.$batas;
        return $kodetampil;  
    }

    function klaim($id)
    {
        $q = $this->db->query("select a.*, b.* from tm_pengajuan_klaim a 
            left join tt_pengajuan_klaim_approval b on a.id = b.id_pengajuan_klaim
            where a.id = $id");
        return $q;
    }

    function statusKlaim()
    {
        $q = $this->db->query("select * from tm_status_klaim");
        return $q;
    }

}