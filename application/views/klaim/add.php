<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0 font-size-18"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Klaim</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">

        <div class="col-xl-12">
                           
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- <h4 class="card-title mb-4">Wizard with progressbar</h4> -->

                            <div id="progrss-wizard" class="twitter-bs-wizard">
                                <ul class="twitter-bs-wizard-nav nav-justified">
                                    <li class="nav-item">
                                        <a href="#progress-seller-details" class="nav-link" data-toggle="tab">
                                            <span class="step-number">01</span>
                                            <span class="step-title">Pengajuan Klaim</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a onclick="warning('warning','Harap Selesaikan Step 1')" class="nav-link" >
                                            <span class="step-number">02</span>
                                            <span class="step-title">Upload Dokumen</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a onclick="warning('warning','Harap Selesaikan Step 1')" class="nav-link" >
                                            <span class="step-number">03</span>
                                            <span class="step-title">Pembayaran Klaim</span>
                                        </a>
                                    </li>
                                </ul>

                                <div id="bar" class="progress mt-4">
                                    <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"></div>
                                </div>
                                <div class="tab-content twitter-bs-wizard-tab-content">
                                    <div class="tab-pane" id="progress-seller-details">

                                        <form id="form1" action="<?=base_url('pengajuan_klaim/save')?>" method="post">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="mb-3">
                                                        <label for="firstName5">Nomor Rekening<code> *</code></label>
                                                            <input type="text" class="form-control" id="norek" name="norek" value="<?php if (isset($d->norek)) {
                                                        echo $d->norek; }?>" placeholder="Masukan No Rekening">
                                                        <div class="err-norek" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">No. Klaim <code>*</code></label>
                                                        <input type="text" name="no_klaim" class="form-control" placeholder="Masukan No. Klaim" value="<?=$kodena?>" required=""  readonly>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label>No. Akad <code>*</code></label>
                                                        <input type="text" name="no_akad" id="no_akad" class="form-control" placeholder="Masukan No. Akad" required="" value="<?php if (isset($d->no_akad)) {
                                                        echo $d->no_akad; } ?>" >
                                                        <div class="err-no_akad" style="display:none;"><code><i>* Wajib Diisi</i></code></div>

                                                    </div>
                                                </div>
                                               <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">No. CIF <code>*</code></label>
                                                        <input type="text" id="no_cif"name="no_cif" class="form-control" placeholder="Masukan No. CIF" required="" value="<?php if (isset($d->no_cif)) {
                                                        echo $d->no_cif; } ?>" >
                                                        <div class="err-no_cif" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6" style="display:none">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">No. LD <code>*</code></label>
                                                        <input type="hidden" id="no_ld"name="no_ld" class="form-control" placeholder="Masukan No. LD" required="" value="<?php if (isset($d->no_ld)) {
                                                        echo $d->no_ld; } ?>" >
                                                        <div class="err-no_ld" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Nama Debitur <code>*</code></label>
                                                        <input type="text" name="nama_debitur" id="nama_debitur" class="form-control" placeholder="Masukan Nama Debitur" required="" value="<?php if (isset($d->nama_debitur)) {
                                                        echo $d->nama_debitur; } ?>" >
                                                        <div class="err-nama_debitur" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                               <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Tanggal Lahir <code>*</code></label>
                                                        <input type="date" name="tanggal_lahir" id="datepicker-basic" class="tgl_lahir form-control" placeholder="Masukan Tanggal lahir"  value="<?php if (isset($d->tanggal_lahir)) {
                                                        echo date('d-m-Y',strtotime($d->tanggal_lahir)); } ?>" required="" >
                                                        <div class="err-tanggal_lahir" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">No. KTP <code>*</code></label>
                                                        <input type="text" name="no_ktp" id="no_ktp" class="form-control" placeholder="Masukan No. KTP" required="" value="<?php if (isset($d->no_ktp)) {
                                                        echo $d->no_ktp; } ?>" >
                                                        <div class="err-no_ktp" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Alamat <code>*</code></label>
                                                        <textarea name="alamat" id="alamat" class="form-control" placeholder="Masukan Alamat" required="" ></textarea>
                                                        <div class="err-alamat" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                               <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">No. Sertifikat Polis <code>*</code></label>
                                                        <input type="text" name="no_sertifikat_polis" class="form-control" id="no_sertifikat_polis" placeholder="Masukan No. Sertifikat Polis" required="" value="<?php if (isset($d->no_sertifikat_polis)) {
                                                        echo $d->no_sertifikat_polis; } ?>" >
                                                        <div class="err-no_sertifikat_polis" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Kantor Cabang (KC) <code>*</code></label>
                                                        <input type="text" name="kantor_cabang" id="kantor_cabang" class="form-control" placeholder="Masukan Nama Cabang" value="<?php if (isset($d->kantor_cabang)) {
                                                        echo $d->kantor_cabang; } ?>" required="" >
                                                        <div class="err-kantor_cabang" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                             <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Nama Perusahaan Asuransi <code>*</code></label>
                                                        <select class="form-select" name="nama_perusahaan_asuransi" id="nama_perusahaan_asuransi">
                                                            <option></option>
                                                            <?php
                                                            
                                                            $asu = $this->db->query("select * from tm_asuransi");

                                                            foreach ($asu->result_array() as $value) {
                                                                echo"<option value='".$value['id']."'>".$value['asuransi']."</option>"; 
                                                            }
                                                            ?> 
                                                        </select>
                                                        <div class="err-nama_perusahaan_asuransi" style="display:none"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                
                                               <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Jenis Klaim <code>*</code></label>
                                                        <select class="form-select" name="jenis_klaim" id="jenis_klaim" >
                                                            <option></option> 
                                                            <option value="1">Meninggal Dunia</option> 
                                                            <option value="2">Kebakaran</option> 
                                                            <option value="3">Macet</option> 
                                                        </select>
                                                        <div class="err-jenis_klaim" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Fasilitas Kredit <code>*</code></label>
                                                        <input type="text" name="fasilitas_kredit" class="form-control"required="" value="<?php if (isset($d->fasilitas_kredit)) {
                                                        echo $d->fasilitas_kredit; } else {echo 'TEST';} ?>" placeholder="Masukan Fasilitas Kredit" >
                                                        <div class="err-fasilitas_kredit" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                                
                                               <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Tanggal Kejadian <code>*</code></label>
                                                        <input type="date" name="tanggal_kejadian" id="tanggal_kejadian" class="form-control" required="" value="<?php if(isset($d->tanggal_kejadian)){echo $d->tanggal_kejadian;}?>">
                                                        <div class="err-tanggal_kejadian" style="display:none;"><code><i>* Wajib Diisi</i></code></div>

                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Jangka Waktu Mulai Kredit <code>*</code></label>
                                                        <input type="date" name="jangka_waktu_mulai_kredit" id="tgl_awal_tenor" class="form-control" required="" value="<?php if (isset($d->jangka_waktu_mulai_kredit)) {
                                                        echo date('d-m-Y', strtotime($d->jangka_waktu_mulai_kredit)); } ?>"  placeholder="Masukan Jangka Waktu Mulai Kredit">
                                                        <div class="err-jangka_waktu_mulai_kredit" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                                
                                               <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Jangka Waktu Akhir Kredit <code>*</code></label>
                                                        <input type="date" name="jangka_waktu_akhir_kredit" id="tgl_akhir_tenor" class="form-control" required="" value="<?php if (isset($d->jangka_waktu_akhir_kredit)) {
                                                        echo date('d-m-Y',strtotime($d->jangka_waktu_akhir_kredit)); } ?>"  placeholder="Masukan Jangka Waktu Akhir Kredit">
                                                        <div class="err-jangka_waktu_akhir_kredit" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Tenor Kredit <code>*</code></label>
                                                        <input type="text" name="tenor_kredit" id="tenor" class="form-control" placeholder="Masukan Tenor Kredit"  required="" value="<?php if (isset($d->tenor_kredit)) {
                                                        echo $d->tenor_kredit; } ?>">
                                                        <div class="err-tenor_kredit" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                                
                                               <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Premi Asuransi<code>*</code></label>
                                                        <input type="text" name="premi_asuransi" id="premi_asuransi" class="numberformat form-control" placeholder="0" required="" value="<?php if (isset($d->premi_asuransi)) {
                                                        echo $d->premi_asuransi; } ?>"  >
                                                        <div class="err-premi_asuransi" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Nilai Plafond Kredit <code>*</code></label>
                                                        <input type="text" name="nilai_plafond_kredit" id="plafond" class="numberformat form-control" placeholder="0" required="" value="<?php if (isset($d->nilai_plafond_kredit)) {
                                                        echo $d->nilai_plafond_kredit; } ?>" >
                                                        <div class="err-nilai_plafond_kredit" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Nilai Pokok Pengajuan Klaim <code>*</code> <i id="ketPokok" style="display:none">(Min 75%, Max 100% dari Nilai Plafond)</i> </label>
                                                        <input type="text" name="nilai_pokok_pengajuan_klaim" id="nilai_pokok_pengajuan_klaim" class=" numberformat form-control" placeholder="0" required="" value="<?php if (isset($d->nilai_pokok_pengajuan_klaim)) {
                                                        echo $d->nilai_pokok_pengajuan_klaim; } ?>" >
                                                        <div class="err-nilai_pokok_pengajuan_klaim" style="display:none;"><code><i>* Wajib Diisi</i></code></div>

                                                    </div>
                                                </div>

                                                <!-- <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Nilai Bunga Pengajuan Klaim <code>*</code></label>
                                                        <input type="text" name="nilai_bunga_pengajuan_klaim" id="nilai_bunga_pengajuan_klaim" class=" numberformat form-control" placeholder="0" required="" value="<?php if (isset($d->nilai_bunga_pengajuan_klaim)) {
                                                        echo $d->nilai_bunga_pengajuan_klaim; } ?>" >
                                                        <div class="err-nilai_bunga_pengajuan_klaim" style="display: none;"><code>Wajib Diisi</code></div>

                                                    </div>
                                                </div> -->

                                                 <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Nilai Total Pengajuan Klaim <code>*</code></label>
                                                        <input type="text" name="nilai_total_pengajuan_klaim" id="nilai_total_pengajuan_klaim" class=" form-control" placeholder="0" required="" value="<?php if (isset($d->nilai_total_pengajuan_klaim)) {
                                                        echo $d->nilai_total_pengajuan_klaim; } ?>" >
                                                        <div class="err-nilai_total_pengajuan_klaim" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                 <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-firstname-input">Sebab Klaim <code>*</code></label>
                                                        <input type="text" name="sebab_klaim" id="sebab_klaim" class=" form-control" required="" value="<?php if (isset($d->sebab_klaim)) {
                                                        echo $d->sebab_klaim; } ?>" >
                                                        <div class="err-sebab_klaim" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    
                                </div>
                                <ul class="pager wizard twitter-bs-wizard-pager-link">
                                   <button type="button" onclick="saveHiji()" class="btn btn-success float-start" >Simpan <i class="mdi mdi-content-save-outline"></i>
                                        
                                    </button>
                                    <li class="next"><a onclick="warning('warning','Harap Selesaikan Step 1')">Selanjutnya <i class="ri-arrow-right-fill"></i> </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- end col -->
    </div><!-- end row -->

</div> <!-- container-fluid -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

    function saveHiji()
    {
        let err = 0;

        var norek = $("#norek").val();
        if (norek == '') {
            $(".err-norek").show();
            err = 1;
        } else{
            $('.err-norek').hide();
        }

        var noklaim = $("#no_klaim").val();
        if (noklaim == '') {
            $(".err-no_klaim").show();
            err = 1;
        } else{
            $('.err-no_klaim').hide();
        }

        var noakad = $("#no_akad").val();
        if (noakad == '') {
            $(".err-no_akad").show();
            err = 1;
        } else{
            $('.err-no_akad').hide();
        }

        var nocif = $("#no_cif").val();
        if (nocif == '') {
            $(".err-no_cif").show();
            err = 1;
        } else{
            $('.err-no_cif').hide();
        }

        var debitur = $("#nama_debitur").val();
        if (debitur == '') {
            $(".err-nama_debitur").show();
            err = 1;
        } else{
            $('.err-nama_debitur').hide();
        }

        var tgllahir = $("#tanggal_lahir").val();
        if (tgllahir == '') {
            $(".err-tanggal_lahir").show();
            err = 1;
        } else{
            $('.err-tanggal_lahir').hide();
        }

        var noktp = $("#no_ktp").val();
        if (noktp == '') {
            $(".err-no_ktp").show();
            err = 1;
        } else{
            $('.err-no_ktp').hide();
        }

        var alamat = $("#alamat").val();
        if (alamat == '') {
            $(".err-alamat").show();
            err = 1;
        } else{
            $('.err-alamat').hide();
        }

        var no_sertifikat_polis = $("#no_sertifikat_polis").val();
        if (no_sertifikat_polis == '') {
            $(".err-no_sertifikat_polis").show();
            err = 1;
        } else{
            $('.err-no_sertifikat_polis').hide();
        }

        var kantor_cabang = $("#kantor_cabang").val();
        if (kantor_cabang == '') {
            $(".err-kantor_cabang").show();
            err = 1;
        } else{
            $('.err-kantor_cabang').hide();
        }

        var nama_perusahaan_asu = $("#nama_perusahaan_asuransi").val();
        if (nama_perusahaan_asu == '') {
            $(".err-nama_perusahaan_asuransi").show();
            err = 1;
        } else{
            $('.err-nama_perusahaan_asuransi').hide();
        }

         var jenis_klaim = $("#jenis_klaim").val();
        if (jenis_klaim == '') {
            $(".err-jenis_klaim").show();
            err = 1;
        } else{
            $('.err-jenis_klaim').hide();
        }

        var fasilitas_kredit = $("#fasilitas_kredit").val();
        if (fasilitas_kredit == '') {
            $(".err-fasilitas_kredit").show();
            err = 1;
        } else{
            $('.err-fasilitas_kredit').hide();
        }

        var tanggal_kejadian = $("#tanggal_kejadian").val();
        if (tanggal_kejadian == '') {
            $(".err-tanggal_kejadian").show();
            err = 1;
        } else{
            $('.err-tanggal_kejadian').hide();
        }

        var jwawal = $("#jangka_waktu_awal_kredit").val();
        if (jwawal == '') {
            $(".err-jangka_waktu_awal_kredit").show();
            err = 1;
        } else{
            $('.err-jangka_waktu_awal_kredit').hide();
        }

        var jwakhir = $("#jangka_waktu_akhir_kredit").val();
        if (jwakhir == '') {
            $(".err-jangka_waktu_akhir_kredit").show();
            err = 1;
        } else{
            $('.err-jangka_waktu_akhir_kredit').hide();
        }




        var sebab = $("#sebab_klaim").val();
        if (sebab == '') {
            $(".err-sebab_klaim").show();
            err = 1;
        } else{
            $('.err-sebab_klaim').hide();
        }

        var nilai_pokok_pengajuan_klaim = $("#nilai_pokok_pengajuan_klaim").val();
        if (nilai_pokok_pengajuan_klaim == '') {
            $(".err-nilai_pokok_pengajuan_klaim").show();
            err = 1;
        } else{
            $('.err-nilai_pokok_pengajuan_klaim').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '<?=base_url('klaim/save')?>',
            data: $("#form1").serialize(),
            beforeSend: function(){
                Swal.fire({
                    title: 'Wait',
                    html: 'Data Saving in progress',// add html attribute if you want or remove
                    allowOutsideClick : false,
                    showConfirmButton : false,
                    didOpen: () => {
                        Swal.showLoading()
                    }
                });
            },
            success: function(response) {
                // console(response.)
                swal.close();
                    message('success', 'Berhasil disimpan');
                    window.setTimeout(function(){ 
                        window.location.href = '<?=base_url('klaim/edit/')?>' +response.idna;
                    } ,2000);
            }, error: function(){
                 warning('error', 'Gagal disimpan');
            }
        });
    }

    var premis = document.getElementById('premi_asuransi');
    premis.addEventListener('keyup', function(e)
    {
        
      premis.value = formatRupiah(this.value);
    });

    var plafonds = document.getElementById('plafond');
    plafonds.addEventListener('keyup', function(e)
    {
        
      plafonds.value = formatRupiah(this.value);
    });

    var pengajuan = document.getElementById('nilai_pokok_pengajuan_klaim');
    pengajuan.addEventListener('keyup', function(e)
    {
        
      pengajuan.value = formatRupiah(this.value);
      cek();
    });


    var total = document.getElementById('nilai_total_pengajuan_klaim');
    total.addEventListener('keyup', function(e)
    {
        total.value = formatRupiah(this.value, 'Rp. ');
    });

    function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

</script>



