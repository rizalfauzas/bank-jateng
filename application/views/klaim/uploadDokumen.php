<?php if ($d->nama_perusahaan_asuransi == 1)  { ?>
    
    <form id="form2" method="post" action="<?=base_url('klaim/save_persetujuandokumen')?>" enctype="multipart/form-data">

        <h5>A. Syarat Umuum</h5>
        <p></p>
        <div class="row">
            
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Surat Klaim</label>
                    <input type="file" class="form-control" name="surat_klaim" id="surat_klaim"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6" id="statusDokKlaim">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->surat_klaim)) {
                        echo '<p style="font-style:italic;"><a href="'.base_url('upload/klaim/'.$d->surat_klaim).'" target="_blank">Surat Klaim</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Polis Asuransi Kredit</label>
                    <input type="file" class="form-control" name="polis_asuransi_kredit" accept=".pdf" >
                </div>
            </div>

            <div class="col-lg-6" >
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->polis_asuransi_kredit)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->polis_asuransi_kredit).'" target="_blank">Polis Asuransi Kredit</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">SK Pegawai</label>
                    <input type="file" class="form-control" name="sk_pegawai" accept=".pdf" >
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->sk_pegawai)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->sk_pegawai).'" target="_blank">SK Pegawai</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Perjanjian Kredit</label>
                    <input type="file" class="form-control" name="perjanjian_kredit" accept=".pdf" >
                    <!-- <br><i><code>*</code></i> -->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                   <?php if (isset($d->perjanjian_kredit)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->perjanjian_kredit).'" target="_blank">Perjanjian Kredit</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Persetujuan Kredit</label>
                    <input type="file" class="form-control" name="persetujuan_kredit"  accept=".pdf">
                    <!-- <br><i><code>*</code></i> -->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->persetujuan_kredit)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->persetujuan_kredit).'" target="_blank">Persetujuan Kredit</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <hr>

        <h5>B. Syarat Khusus</h5>
        <p></p>

        <?php if ($d->jenis_klaim == 1) { ?>

         <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Surat Keterangan kematiaan dari Rumah Sakit dan/atau pihak yang berwenang</label>
                    <input type="file" class="form-control" name="sk_kematian"  accept=".pdf">
                    <!-- <br><i><code>*</code></i> -->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->sk_kematian)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->sk_kematian).'" target="_blank">Surat Kematian</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

    <?php } else if ($d->jenis_klaim == 2) { ?>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Surat PHK</label>
                    <input type="file" class="form-control" name="surat_phk"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->surat_phk)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->surat_phk).'" target="_blank">Surat PHK 3</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

    <?php } ?>

        
        <hr>

        <h5>C. Syarat Lainnya</h5>
        <p></p>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Identitas Debitur (KTP)</label>
                    <input type="file" class="form-control" name="ktp"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->ktp)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->ktp).'" target="_blank">KTP</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Kartu Identitas Pekerjaan</label>
                    <p style="font-style: italic; font-size: 12px;">- Kartu Pegawai untuk Pegawai <br>- KARIP (Kartu Identitas Pensiun) untuk Pensiunan <br> - Sertifikat/Lisensi Profesi untuk Profesional <br> - Legalitas Usaha (Debitur Wira Usaha)</p>
                    <input type="file" class="form-control" name="kartu_identitas_pekerjaan"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->kartu_identitas_pekerjaan)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->kartu_identitas_pekerjaan).'" target="_blank">KTP</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Bukti bayar Premi</label>
                    <input type="file" class="form-control" name="bukti_bayar_premi"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->bukti_bayar_premi)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->bukti_bayar_premi).'" target="_blank">Bukti Bayar Premi</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">SK Pengangkatan Pegawai pertrama kali <i>(Khusus Pegawai)</i></label>
                    <input type="file" class="form-control" name="sk_pengangkatan"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->sk_pengangkatan)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->sk_pengangkatan).'" target="_blank">SK Pengangkatan Pegawai</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">SK Pengangkatan Pegawai tetap <i>(Khusus Pegawai)</i></label>
                    <input type="file" class="form-control" name="sk_pengangkatan_tetap"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->sk_pengangkatan_tetap)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->sk_pengangkatan_tetap).'" target="_blank">SK Pengangkatan Pegawai Tetap</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">SK Golongan/pangkat terakhir <i>(Khusus Pegawai)</i></label>
                    <input type="file" class="form-control" name="sk_golongan_terakhir"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->sk_golongan_terakhir)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->sk_golongan_terakhir).'" target="_blank">SK Golongan Terakhir</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Surat Kuasa Potong Gaji</label>
                    <input type="file" class="form-control" name="surat_kuasa_potgaji"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->surat_kuasa_potgaji)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->surat_kuasa_potgaji).'" target="_blank">Surat Kuasa Potong Gaji</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Surat Rekomendasi Pimpinan/Pejabat tempat bekerja (Jika payroll tidak melalui Bank Jateng)</label>
                    <input type="file" class="form-control" name="surat_rekomendasi"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->surat_rekomendasi)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->surat_rekomendasi).'" target="_blank">Surat Rekomendasi Pimpinan</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Slip Gaji</label>
                    <input type="file" class="form-control" name="slip_gaji"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->slip_gaji)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->slip_gaji).'" target="_blank">Slip Gaji</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

         <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Analisa Kredit</label>
                    <input type="file" class="form-control" name="analasi_kredit"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->analasi_kredit)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->analasi_kredit).'" target="_blank">Analisa Kredit</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Bukti (Nota) Pencairan Kredit</label>
                    <input type="file" class="form-control" name="bukti_pencairan"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->bukti_pencairan)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->bukti_pencairan).'" target="_blank">Bukti (Nota) Pencairan Kredit</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Bukti Agunan Tambahan <i>(Khusus Wiraswasta)</i></label>
                    <input type="file" class="form-control" name="bukti_agunan"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->bukti_agunan)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->bukti_agunan).'" target="_blank">Bukti Agunan Tambahan</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            
            <?php if ($this->session->userdata('id_groups') == 1 or $this->session->userdata('id_groups') == 2 ) { ?>

            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="progresspill-namecard-input" class="form-label">Status Klaim<code> *</code></label>
                    <select class="form-select" name="status_klaim" id="status_klaim" required="">
                        <?php
                        $cek = $this->db->query("select status_klaim from tm_pengajuan_klaim where id = $d->idklaim ")->row();
                      foreach ($statusKlaim as $val) {
                      if ($cek->status_klaim==$val['id']) {
                          echo"<option selected='selected' value='".$val['id']."'>".$val['status_klaim']."</option>"; 
                        } else {
                          echo"<option value='".$val['id']."'>".$val['status_klaim']."</option>"; 
                        }
                        }
                      ?> 
                    </select>
                    <div class="err-status_klaim" style="display: none;"><code><i>* Wajib Diisi</i></code></div>
                </div>
            </div>

        <?php } ?>

             <div class="col-lg-6">
                <div class="mb-3">
                    <label for="progresspill-namecard-input" class="form-label">Keterangan<code> *</code></label>
                    <textarea  class="form-control" name="keterangan" rows="3" placeholder="Keterangan"><?php if (isset($d->keterangan)) {
                        echo $d->keterangan;
                    }?></textarea>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <button type="button" onclick="submitForm2()" id="btnForm2" class="btn btn-success float-start" >Simpan 
                            <i class="mdi mdi-content-save-outline"></i>
                    </button>
                    <br>
                    <br>
                <div class="mb-3">

                    

                    <ul class="pager wizard twitter-bs-wizard-pager-link">
                        
                        <li class="previous"><a href="javascript: void(0);"><i class="ri-arrow-left-fill"></i> Sebelumnya </a></li>

                        <?php if (isset($d->app_2_status)) { ?>
                            <li class="next"><a href="javascript: void(0);">Selanjutnya <i class="ri-arrow-right-fill"></i></a></li>
                        <?php } else { ?>
                            <li class="float-end"><a onclick="warning('warning','Harap Selesaikan Step 2')">Selanjutnya <i class="ri-arrow-right-fill"></i></a></li>
                        <?php } ?>

                        
                    </ul>

                </div>

            </div>

              
        </div>

    </form>

<?php } else if ($d->nama_perusahaan_asuransi == 2) { ?>
    <form id="form2" method="post" action="<?=base_url('klaim/save_persetujuandokumen')?>" enctype="multipart/form-data">

        <div class="row">
            
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Surat Permohonan Klaim</label>
                    <input type="file" class="form-control" name="surat_pengajuan_klaim" id="surat_pengajuan_klaim"  accept=".pdf">
                </div>
            </div>

            <div class="col-lg-6" id="statusDokKlaim">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->surat_pengajuan_klaim)) {
                        echo '<p style="font-style:italic;"><a href="'.base_url('upload/klaim/'.$d->surat_pengajuan_klaim).'" target="_blank">Surat Permohonan Klaim</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Berita Acara Klaim</label>
                    <input type="file" class="form-control" name="berita_acara_klaim" accept=".pdf" >
                </div>
            </div>

            <div class="col-lg-6" id="statusBeritaAcaraKlaim">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->berita_acara_klaim)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->berita_acara_klaim).'" target="_blank">Berita Acara Klaim</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Sertifikat Penjaminan</label>
                    <input type="file" class="form-control" name="sertifikat_penjaminan" accept=".pdf" >
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->sertifikat_penjaminan)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->sertifikat_penjaminan).'" target="_blank">Sertifikat Penjaminan</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Kartu Tanda Penduduk (KTP)</label>
                    <input type="file" class="form-control" name="ktp" accept=".pdf" >
                    <!-- <br><i><code>*</code></i> -->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                   <?php if (isset($d->ktp)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->ktp).'" target="_blank">KTP</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Kartu Pegawai (NIP/NIK)</label>
                    <input type="file" class="form-control" name="kartu_pegawai"  accept=".pdf">
                    <!-- <br><i><code>*</code></i> -->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->kartu_pegawai)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->kartu_pegawai).'" target="_blank">Kartu Pegawai</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

         <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Rekening Pinjaman 6 Bulan terakhir</label>
                    <input type="file" class="form-control" name="rekening"  accept=".pdf">
                    <!-- <br><i><code>*</code></i> -->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->rekening)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->rekening).'" target="_blank">Rekening Pinjaman</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-3">
                    <label class="form-label">Keterangan Kolektibilitas 3</label>
                    <input type="file" class="form-control" name="ket_kolektibilitas"  accept=".pdf">
                    <!-- <br><i><code>*</code></i> -->
                </div>
            </div>

            <div class="col-lg-6">
                <div class="mb-3">
                    <br>
                    <label class="form-label">Uploaded File :</label>
                    
                    <?php if (isset($d->ket_kolektibilitas)) {
                        echo '<p style="font-style:italic"><a href="'.base_url('upload/klaim/'.$d->ket_kolektibilitas).'" target="_blank">Keterangan Kolektibilitas 3</a></p>';
                    } else {
                        echo '<p><code><i>File Belum Diupload</i></code></p>';
                    } ?>
                </div>
            </div>
        </div>

        
        <hr>

        <div class="row">
            
            <?php if ($this->session->userdata('id_groups') == 1 or $this->session->userdata('id_groups') == 2 ) { ?>

            <div class="col-lg-6">
                <div class="mb-3">
                    <label for="progresspill-namecard-input" class="form-label">Status Klaim<code> *</code></label>
                    <select class="form-select" name="status_klaim" id="status_klaim" required="">
                        <?php
                        $cek = $this->db->query("select status_klaim from tm_pengajuan_klaim where id = $d->idklaim ")->row();
                      foreach ($statusKlaim as $val) {
                      if ($cek->status_klaim==$val['id']) {
                          echo"<option selected='selected' value='".$val['id']."'>".$val['status_klaim']."</option>"; 
                        } else {
                          echo"<option value='".$val['id']."'>".$val['status_klaim']."</option>"; 
                        }
                        }
                      ?> 
                    </select>
                    <div class="err-status_klaim" style="display: none;"><code><i>* Wajib Diisi</i></code></div>
                </div>
            </div>

        <?php } ?>

             <div class="col-lg-6">
                <div class="mb-3">
                    <label for="progresspill-namecard-input" class="form-label">Keterangan<code> *</code></label>
                    <textarea  class="form-control" name="keterangan" rows="3" placeholder="Keterangan"><?php if (isset($d->keterangan)) {
                        echo $d->keterangan;
                    }?></textarea>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <button type="button" onclick="submitForm2()" id="btnForm2" class="btn btn-success float-start" >Simpan 
                            <i class="mdi mdi-content-save-outline"></i>
                    </button>
                    <br>
                    <br>
                <div class="mb-3">

                    

                    <ul class="pager wizard twitter-bs-wizard-pager-link">
                        
                        <li class="previous"><a href="javascript: void(0);"><i class="ri-arrow-left-fill"></i> Sebelumnya </a></li>

                        <?php if (isset($d->app_2_status)) { ?>
                            <li class="next"><a href="javascript: void(0);">Selanjutnya <i class="ri-arrow-right-fill"></i></a></li>
                        <?php } else { ?>
                            <li class="float-end"><a onclick="warning('warning','Harap Selesaikan Step 2')">Selanjutnya <i class="ri-arrow-right-fill"></i></a></li>
                        <?php } ?>

                        
                    </ul>

                </div>

            </div>

              
        </div>

    </form>
<?php } ?>
