<style type="text/css">
    th {
    white-space: nowrap;
}

td {
    white-space: nowrap;
}
</style>
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0 font-size-18"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Klaim</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="card">
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <i class="mdi mdi-arrow-right text-primary me-1"></i> Filter Data
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="text-muted">
                            <div class="mt-4 mt-lg-0">
                                <form id="form-filter" method="post">

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Region</label>
                                        <div class="col-sm-6">
                                            <select class="form-select" name="region" id="region" onchange="getArea()">
                                                <option></option>
                                                <?php

                                                $idregion = $this->session->userdata('id_region');
                                                $idcabang = $this->session->userdata('id_cabang');

                                                if ($this->session->userdata('id_groups') == 3) {
                                                   $gets = $this->db->query("select * from tm_cabang where id_region = '". $idregion."' ")->row();
                                                   $wherena = $gets->id_region;

                                                } else if ($this->session->userdata('id_groups') == 4) {
                                                   $gets = $this->db->query("select * from tm_cabang where id_region = '".$idregion."' ")->row();
                                                   $wherena = $gets->id_region;


                                                   // var_dump($idarea);
                                                   // die();

                                                } else if ($this->session->userdata('id_groups') == 5) {
                                                   $gets = $this->db->query("select * from tm_cabang where code_cabang = '".$idcabang."' ")->row();
                                                   $wherena = $gets->id_region;

                                                } else {
                                                    $gets = $this->db->query("select * from tm_cabang ")->row();
                                                }

                                                // $cab = $this->db->query();
                                                if ($this->session->userdata('id_groups') == 3 || $this->session->userdata('id_groups') == 4 || $this->session->userdata('id_groups') == 5 ) {
                                                    $q = $this->db->query("select id_region,region from tm_cabang where id_region = ".$gets->id_region." group by id_region,region ")->result_array();

                                                foreach ($q as $value) {
                                                     echo"<option value='".$value['id_region']."'>".$value['region']."</option>";
                                                }
                                                } else{
                                                $q = $this->db->query("select id_region,region from tm_cabang group by id_region,region ")->result_array();

                                                foreach ($q as $value) {
                                                     echo"<option value='".$value['id_region']."'>".$value['region']."</option>";
                                                }}
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Area</label>
                                        <div class="col-sm-6">
                                            <select class="form-select" name="area" id="area" onchange="getCabang()">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Cabang</label>
                                        <div class="col-sm-6">
                                            <select class="form-select" name="cabang" id="cabang" >
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Dari</label>
                                        <div class="col-sm-6">
                                            <input type="date" class="form-control" id="dari" name="dari">
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Sampai</label>
                                        <div class="col-sm-6">
                                            <input type="date" class="form-control" id="sampai" name="sampai">
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Status Klaim</label>
                                        <div class="col-sm-6">
                                           <select class="form-select" name="statusna" id="statusna" required>
                                                <option></option>
                                                <?php
                                                $q = $this->db->query("select id,status_klaim from tm_status_klaim")->result_array();
                                                foreach ($q as $value) {
                                                     echo"<option value='".$value['id']."'>".$value['status_klaim']."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="hstack gap-3  justify-content-center">
                                        <button type="button" class="btn btn-sm btn-primary" id="btn-filter"><i class="ri-filter-line"></i> Filter</button>
                                        <div class="vr"></div>
                                        <button type="reset" class="btn btn-sm btn-danger" id="btn-reset"><i class="ri-restart-line"></i> Reset</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="mb-4">
                            <a href="<?=base_url('klaim/add')?>" type="button" class="btn btn-sm btn-primary"><i class=" ri-file-add-line"></i> Buat Pengajuan</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                    <table id="tablena" class="table table-striped mb-0" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Aksi</th>
                                <!-- <th>Kelengkapan Dok.</th> -->
                                <th>Status Klaim</th>
                                <th>Nomor Klaim</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Nomor Rekening</th>
                                <th>Nama Debitur</th>
                                <th>Nama Mitra Asuransi</th>
                                <th>Nama Cabang</th>
                                <th>Outstanding Total Klaim</th>
                                <th>No. Akad</th>
                                <th>No. Polis Asuransi</th>
                                <th>CIF No.</th>
                                <th>Tanggal lahir</th>
                                <!-- <th>Status Approvall</th> -->

                            </tr>
                        </thead>

                        <tbody>
                           
                        </tbody>
                    </table>
                    </div>

                    <!-- <p><i>Keterangan : <i class="bx bx-edit-alt" style="color:blue"></i> Edit, <i class="bx bx-loader-circle fa-spin" style="color:red"></i> Dokumen Belum Lengkap </i>, <i class=" bx bxs-badge-check" style="color:green"></i> Dokumen Sudah Lengkap </i></p> -->
                </div>
            </div>
        </div><!-- end card-body -->
    </div><!-- end card -->
    </div><!-- end col -->

<div id="modalKetna" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" data-bs-scroll="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                        <div class="row mb-4">
                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">User</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control" name="create_by">
                            </div>
                        </div>
                         <div class="row mb-4">
                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control" name="status_klaim" >
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Keterangan</label>
                            <div class="col-sm-7">
                              <textarea class="form-control" name="keterangan"></textarea>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Tanggal</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control" name="create_date">
                            </div>
                        </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Tutup</button>
            </div>


        </div>
    </div>
</div>

<div id="modalDok" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" data-bs-scroll="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitlena"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="statusDokumen"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>




</div><!-- end row -->



<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready( function(){
        $("#tablena").DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": '<?=base_url('klaim/getData')?>',
                "type": "POST",
                "data": function ( data ) {
                    data.nama_debitur           = $('#nama_debitur').val();
                    data.no_akad                = $('#no_akad').val();
                    data.no_cif                 = $('#no_cif').val();
                    data.region         = $('#region').val();
                    data.area         = $('#area').val();
                    data.cabang         = $('#cabang').val();
                    data.statusna               = $('#statusna').val();
                    data.norek                  = $('#norek').val();
                    data.dari                  = $('#dari').val();
                    data.sampai                  = $('#sampai').val();
                    // data.address = $('#address').val();
                }
            },
            "columnDefs": [{
                "target": [-1],
                "orderable": false
            }],
            "scrollX": true
        });

        $('#btn-filter').click(function(){ 
            reloadTable();
        });
        $('#btn-reset').click(function(){ 
            $('#form-filter')[0].reset();
            reloadTable();
        });

    });

    // function ketna(id,app_1_status,keterangan,create_by,create_date,urutna)
    function ketna(id,urutna,status_klaim,app_1_keterangan,app_1_create_by,app_1_create_date)
    {
        var urlna = '<?=base_url('pengajuan_klaim/ketna/')?>' + id;

        $.ajax({
            type: "GET",
            url: urlna,
            dataType: "JSON",
            data: {urutna : urutna},
            success: function(response) {
                // var datena = moment(response.app_1_create_date).format('DD-MM-YY H:I:s');
                $('#modalTitle').text('Data Approvall Ke-' + urutna);
                $('[name ="status_klaim"]').val(response.status_klaim);
                $('[name ="keterangan"]').val(response.app_1_keterangan);
                $('[name ="create_by"]').val(response.app_1_create_by);
                $('[name ="create_date"]').val(response.app_1_create_date);
                $('#modalKetna').modal('show');
            }
        });

    }

    function ketna2(id,urutna,status_klaim,app_2_keterangan,app_2_create_by,app_2_create_date)
    {
        var urlna = '<?=base_url('pengajuan_klaim/ketna/')?>' + id;

        $.ajax({
            type: "GET",
            url: urlna,
            dataType: "JSON",
            data: {urutna : urutna},
            success: function(response) {
                // var datena = moment(response.app_1_create_date).format('DD-MM-YY H:I:s');
                $('#modalTitle').text('Data Approvall Ke-' + urutna);
                $('[name ="status_klaim"]').val(response.status_klaim);
                $('[name ="keterangan"]').val(response.app_2_keterangan);
                $('[name ="create_by"]').val(response.app_2_create_by);
                $('[name ="create_date"]').val(response.app_2_create_date);
                $('#modalKetna').modal('show');
            }
        });

    }

    function ketna3(id,urutna,status_klaim,app_3_keterangan,app_3_create_by,app_3_create_date)
    {
        var urlna = '<?=base_url('pengajuan_klaim/ketna/')?>' + id;

        $.ajax({
            type: "GET",
            url: urlna,
            dataType: "JSON",
            data: {urutna : urutna},
            success: function(response) {
                // var datena = moment(response.app_1_create_date).format('DD-MM-YY H:I:s');
                $('#modalTitle').text('Data Approvall Ke-' + urutna);
                $('[name ="status_klaim"]').val(response.status_klaim);
                $('[name ="keterangan"]').val(response.app_3_keterangan);
                $('[name ="create_by"]').val(response.app_3_create_by);
                $('[name ="create_date"]').val(response.app_3_create_date);
                $('#modalKetna').modal('show');
            }
        });

    }

    function cekDok(id) 
    {
            $.get('<?=base_url('pengajuan_klaim/statusdokwi/')?>' + id, function(data) {
            $('#statusDokumen').html(data)
                $.ajax({
                    type: 'get',
                    url: $(this).attr('href'),
                    success: function(response){
                        $('#modalTitlena').text('Status Dokumen ');
                        $('#modalDok').modal('show');
                    }

                });
        });
    }

    function getArea()
    {        
        $.ajax({
            type: "POST", 
            url: "<?php echo base_url("penutupan_asuransi_jiwa/listArea"); ?>", 
            data: {id_region : $("#region").val()}, 
            dataType: "json",
            beforeSend: function(e) {
            if(e && e.overrideMimeType) {
                e.overrideMimeType("application/json;charset=UTF-8");
                }
            },
            success: function(response){ // Ketika proses pengiriman berhasil
                if (response.status == 'failed') {
                    $('#area').empty();
                    $('#cabang').empty();

                } else {
                    $("#area").html(response.list_area).show();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
            }
        });
    }

    function getCabang()
    {        
        $.ajax({
            type: "POST", 
            url: "<?php echo base_url("penutupan_asuransi_jiwa/listCabang"); ?>", 
            data: {id_area : $("#area").val()}, 
            dataType: "json",
            beforeSend: function(e) {
            if(e && e.overrideMimeType) {
                e.overrideMimeType("application/json;charset=UTF-8");
                }
            },
            success: function(response){ // Ketika proses pengiriman berhasil
                if (response.status == 'failed') {
                    $('#cabang').empty();
                    $('#area').empty();
                } else {
                    $("#cabang").html(response.list_cabang).show();

                }
            },
            error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
            }
        });
    }

</script>



