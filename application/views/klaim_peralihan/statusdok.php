<div class="mb-12">
    <label for="progresspill-namecard-input" class="form-label">Status Dokumen :</label>
    <div class="row">
        <div class="col-md-6">
            
            <div class="col-md-12">
               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->surat_pengajuan_klaim)) {
                   echo 'checked onclick="return false;"';
               }?>>
              <label>Surat Pengajuan Klaim</label>  
            </div>

            <div class="col-md-12">
               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->fc_ktp)) {
                   echo 'checked onclick="return false;"';
               }?>>
              <label>Copy KTP</label>  
            </div>

            <div class="col-md-12">
               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->fc_kk)) {
                   echo 'checked onclick="return false;"';
               }?>>
              <label>Copy KK</label>  
            </div>

            <div class="col-md-12">
               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->fc_akad)) {
                   echo 'checked onclick="return false;"';
               }?>>
              <label>Copy Akad</label>  
            </div>

            <div class="col-md-12">
               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->fc_realisasi_pencairan_pembiayaan)) {
                   echo 'checked onclick="return false;"';
               }?>>
              <label>Copy Realisasi Pencairan Pembayaran</label>  
            </div>

            <div class="col-md-12">
               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->fc_rekening)) {
                   echo 'checked onclick="return false;"';
               }?>>
              <label>Copy Rekening</label>  
            </div>

           
        </div>

        <div class="col-md-6">
            
            <div class="col-md-12">
               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->fc_sk_pengakatanpeg)) {
                   echo 'checked onclick="return false;"';
               }?>>
              <label>Copy SK Pengangkatan </label>  
            </div>



            <?php if ($d->jenis_klaim == 1) { ?>
                <div class="col-md-12">
                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->sk_meninggal)) {
                       echo 'checked onclick="return false;"';
                   }?>>
                  <label>Copy SK Meninggal</label>  
                </div>
            <?php } else if ($d->jenis_klaim == 2) { ?>
                <div class="col-md-12">
                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->sk_terjadi_kebakaran)) {
                       echo 'checked onclick="return false;"';
                   }?>>
                  <label>SK terjadi kebakaran</label>  
                </div>
            <?php } else if ($d->jenis_klaim == 2) { ?>
                <div class="col-md-12">
                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->fc_surat_tagihan)) {
                       echo 'checked onclick="return false;"';
                   }?>>
                  <label>Copy Surat Tagihan</label>  
                </div>

                <div class="col-md-12">
                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->slik_ojk)) {
                       echo 'checked onclick="return false;"';
                   }?>>
                  <label>SLIK OJK</label>  
                </div>
            <?php } ?>
           
        </div>

        
    </div>

    </div>