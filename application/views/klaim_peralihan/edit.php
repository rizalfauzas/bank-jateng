<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0 font-size-18"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Klaim</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">

        <div class="col-xl-12">
                           
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                       
                        <div class="card-body">

                            <div id="progrss-wizard" class="twitter-bs-wizard">
                                 <ul class="twitter-bs-wizard-nav nav-justified">
                                    <li class="nav-item">
                                        <a href="#progress-seller-details" class="nav-link" data-toggle="tab">
                                            <span class="step-number">01</span>
                                            <span class="step-title">Pengajuan Klaim</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">

                                        <?php if ($d->app_1_status) { ?>
                                            <a href="#progress-company-document" class="nav-link" data-toggle="tab">
                                                <span class="step-number">02</span>
                                                <span class="step-title">Upload Dokumen</span>
                                            </a>
                                       <?php } else { ?>
                                            <a onclick="warning('warning','Harap Selesaikan Step 1')" class="nav-link" >
                                                <span class="step-number">02</span>
                                                <span class="step-title">Upload Dokumen</span>
                                            </a>
                                        <?php  } ?>

                                        
                                    </li>

                                    <li class="nav-item">
                                        <?php if ($d->app_2_status) { ?>
                                        <a href="#progress-bank-detail" class="nav-link" data-toggle="tab">
                                            <span class="step-number">03</span>
                                            <span class="step-title">Pembayaran Klaim</span>
                                        </a>
                                    <?php } else { ?>
                                        <a onclick="warning('warning','Harap Selesaikan Step 2')" class="nav-link" >
                                            <span class="step-number">03</span>
                                            <span class="step-title">Pembayaran Klaim</span>
                                        </a>
                                   <?php } ?>
                                    </li>
                                    
                                </ul>

                                <div id="bar" class="progress mt-4">
                                    <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"></div>
                                </div>

                                
                                    <div class="tab-content twitter-bs-wizard-tab-content">
                                        <div class="tab-pane" id="progress-seller-details">
                                            
                                            <form id="form1" action="<?=base_url('klaim_peralihan/save')?>" method="post">
                                                <div class="row">
                                                    
                                               
                                                    <?php if (isset($d->norek)) { ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group validate">
                                                            <label for="firstName5">Nomor Rekening<code> *</code></label>
                                                            <div class="input-group mb-3">
                                                                <input type="text" class="form-control" id="norek" name="norek" value="<?php if (isset($d->norek)) {
                                                            echo $d->norek; }?>" readonly>
                                                            <div class="err-norek" style="display: none;"> <code><i>* Wajib Diisi</i></code></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php } else { ?>
                                                   <div class="col-md-6">
                                                        <div class="form-group validate">
                                                            <label for="firstName5">Nomor Rekening<code> *</code></label>
                                                            <div class="input-group mb-3">
                                                                <input type="text" class="form-control" id="norek" name="norek" value="<?php if (isset($d->norek)) {
                                                            echo $d->norek; }?>" readonly>
                                                            <div class="err-norek" style="display:none;"> <code><i>* Wajib Diisi</i></code></div>
                                                            <div class="clearfix">
                                                                <a type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalCari" >Cari</a>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">No. Klaim <code>*</code></label>
                                                            <input type="text" name="no_klaim" class="form-control" placeholder="Masukan No. Klaim" required="" readonly="" value="<?php if (isset($d->no_klaim)) {
                                                            echo $d->no_klaim; } else {echo $noklaim;} ?>">

                                                            <input type="hidden" name="idna" value="<?php if (isset($d->id)) {
                                                            echo $d->id; }  ?>">
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label>No. Akad <code>*</code></label>
                                                            <input type="text" name="no_akad" id="no_akad" class="form-control" placeholder="Masukan No. Akad" required="" value="<?php if (isset($d->no_akad)) {
                                                            echo $d->no_akad; } ?>" readonly>
                                                            <div class="err-no_akad" style="display:none;"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div> -->
                                                   <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">No. CIF <code>*</code></label>
                                                            <input type="text" id="no_cif"name="no_cif" class="form-control" placeholder="Masukan No. CIF" required="" value="<?php if (isset($d->no_cif)) {
                                                            echo $d->no_cif; } ?>" readonly>
                                                            <div class="err-no_cif" style="display:none;"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Nama Debitur <code>*</code></label>
                                                            <input type="text" name="nama_debitur" id="nama_debitur" class="form-control" placeholder="Masukan Nama Debitur" required="" value="<?php if (isset($d->nama_debitur)) {
                                                            echo $d->nama_debitur; } ?>" readonly>
                                                            <div class="err-nama_debitur" style="display:none;"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div>
                                                   <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Tanggal Lahir <code>*</code></label>
                                                            <input type="text" name="tanggal_lahir" id="datepicker-basic" class="tgl_lahir form-control" placeholder="Masukan Tanggal lahir"  value="<?php if (isset($d->tanggal_lahir)) {
                                                            echo date('d-m-Y',strtotime($d->tanggal_lahir)); } ?>" required="" readonly>
                                                            <div class="err-tanggal_lahir" style="display:none;"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">No. KTP <code>*</code></label>
                                                            <input type="text" name="no_ktp" id="no_ktp" class="form-control" placeholder="Masukan No. KTP" required="" value="<?php if (isset($d->no_ktp)) {
                                                            echo $d->no_ktp; } ?>" readonly>
                                                            <div class="err-no_ktp" style="display:none"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Alamat <code>*</code></label>
                                                            <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Masukan Alamat" required="" value="<?php if (isset($d->alamat)) {
                                                            echo $d->alamat; } ?>" readonly>
                                                            <div class="err-alamat" style="display:none"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div>
                                                  <!--  <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">No. Sertifikat Polis <code>*</code></label>
                                                            <input type="text" name="no_sertifikat_polis" class="form-control" id="no_sertifikat_polis" placeholder="Masukan No. Sertifikat Polis" required="" value="<?php if (isset($d->no_sertifikat_polis)) {
                                                            echo $d->no_sertifikat_polis; } ?>" readonly>

                                                        </div>
                                                    </div> -->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Kantor Cabang (KC) <code>*</code></label>
                                                            <input type="text" name="kantor_cabang" id="nama_kc" class="form-control" placeholder="Masukan Nama Cabang" value="<?php if (isset($d->kantor_cabang)) {
                                                            echo $d->kantor_cabang; } ?>" required="" readonly>
                                                        </div>
                                                    </div>
                                                <!--  <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Nama Perusahaan Asuransi <code>*</code></label>
                                                            <input type="hidden" class="form-control" name="nama_perusahaan_asuransi" id="nama_perusahaan_asuransi" value="<?php if (isset($d->nama_perusahaan_asuransi)) {
                                                            echo $d->nama_perusahaan_asuransi; } ?>" readonly>
                                                            <input type="text" class="form-control" name="" id="namaasuransi" value="<?php if (isset($d->asuransi)) {
                                                            echo $d->asuransi; } ?>" readonly>
                                                        </div>
                                                    </div> -->
                                                    
                                                   <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Jenis Klaim <code>*</code></label>
                                                            <select class="form-select" name="jenis_klaim" id="jenis_klaim">
                                                                 <?php
                                                                
                                                               /* $q = $this->db->query("select a.norek, a.jenis_klaim, b.p_klaim_1, b.p_klaim_2 from tm_pengajuan_klaim_peralihan a 
                                                                join tm_penutupan b on a.norek = b.norek
                                                                where a.norek = '".$d->norek."' order by a.id asc ")->row();*/

                                                                $qq = $this->db->query("select * from tm_jenis_klaim");

                                                                foreach ($qq->result_array() as $value) {
                                                                    if ($d->jenis_klaim == $value['id']) {
                                                                      echo"<option selected='selected' value='".$value['id']."'>".$value['jenis_klaim']."</option>"; 
                                                                    } else {
                                                                      echo"<option value='".$value['id']."'>".$value['jenis_klaim']."</option>"; 
                                                                    }
                                                                } 
                                                                ?> 
                                                            </select>
                                                            <div class="err-jenis_klaim" style="display: none"> <code><i>* Wajib Diisi</i></code></div>

                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Fasilitas Kredit <code>*</code></label>
                                                            <input type="text" name="fasilitas_kredit" class="form-control"required="" value="TEST" placeholder="Masukan Fasilitas Kredit" readonly>
                                                        </div>
                                                    </div>
                                                    
                                                  <!--  <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Tanggal Kejadian <code>*</code></label>
                                                            <input type="date" name="tanggal_kejadian" class="form-control" required="" value="<?php if(isset($d->tanggal_kejadian)){echo $d->tanggal_kejadian;}?>">
                                                            <div class="err-tanggal_kejadian" style="display: none"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div> -->

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Jangka Waktu Mulai Kredit <code>*</code></label>
                                                            <input type="text" name="jangka_waktu_mulai_kredit" id="tgl_awal_tenor" class="form-control" required="" value="<?php if (isset($d->jangka_waktu_mulai_kredit)) {
                                                            echo date('d-m-Y', strtotime($d->jangka_waktu_mulai_kredit)); } ?>" readonly placeholder="Masukan Jangka Waktu Mulai Kredit">
                                                        </div>
                                                    </div>
                                                    
                                                   <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Jangka Waktu Akhir Kredit <code>*</code></label>
                                                            <input type="text" name="jangka_waktu_akhir_kredit" id="tgl_akhir_tenor" class="form-control" required="" value="<?php if (isset($d->jangka_waktu_akhir_kredit)) {
                                                            echo date('d-m-Y',strtotime($d->jangka_waktu_akhir_kredit)); } ?>" readonly placeholder="Masukan Jangka Waktu Akhir Kredit">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Tenor Kredit <code>*</code></label>
                                                            <input type="text" name="tenor_kredit" id="tenor" class="form-control" placeholder="Masukan Tenor Kredit" readonly="" required="" value="<?php if (isset($d->tenor_kredit)) {
                                                            echo $d->tenor_kredit; } ?>">
                                                        </div>
                                                    </div>
                                                    
                                                   <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Premi Asuransi<code>*</code></label>
                                                            <input type="text" name="premi_asuransi" id="premi_asuransi" class="numberformat form-control" placeholder="0" required="" value="<?php if (isset($d->premi_asuransi)) {
                                                            echo $d->premi_asuransi; } ?>" readonly >
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Nilai Plafond Kredit <code>*</code></label>
                                                            <input type="text" name="nilai_plafond_kredit" id="plafond" class="numberformat form-control" placeholder="0" required="" value="<?php if (isset($d->nilai_plafond_kredit)) {
                                                            echo $d->nilai_plafond_kredit; } ?>" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Nilai Pokok Pengajuan Klaim <code>*</code> <i id="ketPokok" style="display:none">(Min 75%, Max 100% dari Nilai Plafond)</i> </label>
                                                            <input type="text" name="nilai_pokok_pengajuan_klaim" id="nilai_pokok_pengajuan_klaim" class=" numberformat form-control" placeholder="0" required="" value="<?php if (isset($d->nilai_pokok_pengajuan_klaim)) {
                                                            echo $d->nilai_pokok_pengajuan_klaim; } ?>" >
                                                            <div class="err-nilai_pokok_pengajuan_klaim" style="display:none;"> <code><i>* Wajib Diisi</i></code></div>
                                                        </div>
                                                    </div>

                                                     <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Nilai Total Pengajuan Klaim <code>*</code></label>
                                                            <input type="text" name="nilai_total_pengajuan_klaim" id="nilai_total_pengajuan_klaim" class=" form-control" placeholder="0" required="" value="<?php if (isset($d->nilai_total_pengajuan_klaim)) {
                                                            echo $d->nilai_total_pengajuan_klaim; } ?>" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="progresspill-firstname-input">Sebab Klaim <code>*</code></label>
                                                            <textarea class="form-control" name="sebab_klaim" id="sebab_klaim" readonly><?php if (isset($d->sebab_klaim)) {
                                                                echo $d->sebab_klaim;
                                                            }?></textarea>
                                                            <div class="err-sebab_klaim" style="display:none;"><code>Wajib Diisi</code></div>
                                                            
                                                        </div>
                                                    </div>
                                                  
                                                </div>

                                                <ul class="pager wizard twitter-bs-wizard-pager-link">
                                                    <li class="margin-left"><a onclick="saveHiji()" class="btn btn-success" >Simpan <i class="mdi mdi-content-save-check"></i></a></li>
                                                    
                                                    <?php if (isset($d->id)) { ?>
                                                        <a href="<?=base_url('klaim_peralihan/printSuratPengajuan/'.$d->id)?>" target="_blank" class="btn btn-danger" >Print Surat Pengajuan Klaim <i class="fas far fa-file-pdf"></i></a>
                                                    <?php } ?>


                                                    <li class="next">
                                                        <a href="javascript: void(0);" class="btn btn-primary" onclick="nextTab()">Selanjutnya <i class="bx bx-chevron-right ms-1"></i>
                                                        </a>
                                                    </li>


                                                </ul>

                                            </form>

                                        </div>

                                    <div class="tab-pane" id="progress-company-document">
                                        <div>
                                            <form id="formUploadDataTambahan" method="post" enctype="multipart/form-data">

                                            <div class="row mb-4">
                                                <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Dokumen Tambahan</label>
                                                <div class="col-sm-9">
                                                  <input type="file" class="form-control" name="dokumen_tambahan" id="dokumen_tambahan" accept=".zip">
                                                  <div class="err-dokumen_tambahan" style="display:none;"><code><i>* Wajib Diisi</i></code></div>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <label for="horizontal-firstname-input" class="col-sm-3 col-form-label"></label>
                                                <div class="col-sm-9">
                                                  <button class="btn btn-warning" type="reset">Reset</button>
                                                  <button class="btn btn-primary" type="button" onclick="uploadData();">Simpan</button>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="col-md-6">
                                                            <strong style="font-size: 14;">List Dokumen yang sudah di terima:</strong>
                                                            <p></p>
                                                            <div class="col-md-12">
                                                               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_surat_pengajuan)) {
                                                                   echo 'checked onclick="return false;"';
                                                               }?>>
                                                              <label>
                                                                <?php if (isset($d->file_surat_pengajuan)) {
                                                                    echo '<a href="'.base_url($d->file_surat_pengajuan).'">Surat Pengajuan Klaim</a>';
                                                                } else {echo 'Surat Pengajuan Klaim';}?>
                                                              </label>  
                                                            </div>

                                                            <div class="col-md-12">
                                                               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_ktp)) {
                                                                   echo 'checked onclick="return false;"';
                                                               }?>>
                                                              <label>
                                                                <?php if (isset($d->file_ktp)) {
                                                                    echo '<a href="'.base_url($d->file_ktp).'" target="_blank">File KTP</a>';
                                                                } else {echo 'File KTP';}?>
                                                              </label> 
                                                            </div>

                                                            <div class="col-md-12">
                                                               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_kk)) {
                                                                   echo 'checked onclick="return false;"';
                                                               }?>>
                                                              <label>
                                                                <?php if (isset($d->file_kk)) {
                                                                    echo '<a href="'.base_url($d->file_kk).'" target="_blank">File KK</a>';
                                                                } else {echo 'File KK';}?>
                                                              </label> 
                                                            </div>

                                                            <div class="col-md-12">
                                                               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_akad)) {
                                                                   echo 'checked onclick="return false;"';
                                                               }?>>
                                                              <label>
                                                                <?php if (isset($d->file_akad)) {
                                                                    echo '<a href="'.base_url($d->file_akad).'" target="_blank">File Akad</a>';
                                                                } else {echo 'File Akad';}?>
                                                              </label> 
                                                            </div>

                                                            <div class="col-md-12">
                                                               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_realisasi)) {
                                                                   echo 'checked onclick="return false;"';
                                                               }?>>
                                                              <label>
                                                                <?php if (isset($d->file_akad)) {
                                                                    echo '<a href="'.base_url($d->file_akad).'" target="_blank">File Bukti Realisasi pencairan pembayaran</a>';
                                                                } else {echo 'File Bukti Realisasi pencairan pembayaran';}?>
                                                              </label> 
                                                            </div>

                                                            <div class="col-md-12">
                                                               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_norek)) {
                                                                   echo 'checked onclick="return false;"';
                                                               }?>>
                                                              <label>
                                                                <?php if (isset($d->file_norek)) {
                                                                    echo '<a href="'.base_url($d->file_norek).'" target="_blank">File Rekening</a>';
                                                                } else {echo 'File Rekening';}?>
                                                              </label> 
                                                            </div>

                                                            <div class="col-md-12">
                                                               <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_skpengangkatan)) {
                                                                   echo 'checked onclick="return false;"';
                                                               }?>>
                                                              <label>
                                                                <?php if (isset($d->file_skpengangkatan)) {
                                                                    echo '<a href="'.base_url($d->file_skpengangkatan).'" target="_blank">File SK Pengangkatan/NPWP</a>';
                                                                } else {echo 'File SK Pengangkatan/NPWP';}?>
                                                              </label> 
                                                            </div>
                                                           
                                                        </div>

                                                        <div class="col-md-6">

                                                            <?php if ($d->jenis_klaim == 1) { ?>
                                                                <div class="col-md-12">
                                                                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_meninggal)) {
                                                                       echo 'checked onclick="return false;"';
                                                                   }?>>
                                                                  <label>
                                                                <?php if (isset($d->file_meninggal)) {
                                                                    echo '<a href="'.base_url($d->file_meninggal).'" target="_blank">File SK Meninggal</a>';
                                                                } else {echo 'File SK Meninggal';}?>
                                                              </label>  
                                                                </div>
                                                            <?php } else if ($d->jenis_klaim == 2) { ?>
                                                                <div class="col-md-12">
                                                                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_kebakaran)) {
                                                                       echo 'checked onclick="return false;"';
                                                                   }?>>
                                                                  <label>
                                                                <?php if (isset($d->file_kebakaran)) {
                                                                    echo '<a href="'.base_url($d->file_kebakaran).'" target="_blank">File SK Kebakaran</a>';
                                                                } else {echo 'File SK Kebakaran';}?>
                                                              </label> 
                                                                </div>
                                                            <?php } else if ($d->jenis_klaim == 3) { ?>
                                                                <div class="col-md-12">
                                                                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_macet)) {
                                                                       echo 'checked onclick="return false;"';
                                                                   }?>>
                                                                  <label>
                                                                <?php if (isset($d->file_macet)) {
                                                                    echo '<a href="'.base_url($d->file_macet).'" target="_blank">File Surat tagihan</a>';
                                                                } else {echo 'File Surat tagihan';}?>
                                                              </label> 
                                                                </div>

                                                                <div class="col-md-12">
                                                                   <input type="checkbox" class="filled-in chk-col-primary" <?php if (isset($d->file_slik)) {
                                                                       echo 'checked onclick="return false;"';
                                                                   }?>>
                                                                  <label>
                                                                <?php if (isset($d->file_slik)) {
                                                                    echo '<a href="'.base_url($d->file_slik).'" target="_blank">File Slik OJK</a>';
                                                                } else {echo 'File SLIK OJK';}?>
                                                              </label> 
                                                                </div>
                                                            <?php } ?>
                                                           
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                

                                            </div>

                                        </form>    
                                        </div>
                                    </div>
                                    
                                    <div class="tab-pane" id="progress-bank-detail">
                                        <div>
                                            <form id="form3" method="post" enctype="multipart/form-data">
                                              <div class="row">
                                                  <div class="col-lg-6">
                                                      <div class="mb-3">
                                                          <label for="progresspill-namecard-input" class="form-label">Nilai Total Pengajuan Klaim<code> *</code></label>
                                                          <input type="text" class="form-control" name="nilai_total_pengajuan_klaim" id="nilaipengajuan" value="<?php if(isset($d->nilai_total_pengajuan_klaim)){echo $d->nilai_total_pengajuan_klaim;}?>" readonly>
                                                          <input type="hidden" class="form-control" name="id_pengajuan_klaim" value="<?php if(isset($d->id_pengajuan_klaim)){echo $d->id_pengajuan_klaim;}?>" readonly>
                                                      </div>
                                                  </div>

                                                   <div class="col-lg-6">
                                                      <div class="mb-3">
                                                          <label for="progresspill-namecard-input" class="form-label">Nilai Persetujuan Klaim</label>
                                                          <input type="text" class="form-control" name="nilai_persetujuan_klaim" id="nilaipersetujuan" value="<?php if(isset($d->nilai_persetujuan_klaim)){echo $d->nilai_persetujuan_klaim;}?>">
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-6">
                                                      <div class="mb-3">
                                                          <label for="progresspill-namecard-input" class="form-label">Nilai Klaim yg dibayarkan ke Bank<code> *</code></label>
                                                          <input type="text" class="form-control" name="nilai_klaim_yg_dbayar" id="nilaidibayar" value="<?php if(isset($d->nilai_klaim_yg_dbayar)){echo $d->nilai_klaim_yg_dbayar;}?>">
                                                      </div>
                                                  </div>

                                                   <div class="col-lg-6">
                                                      <div class="mb-3">
                                                          <label for="progresspill-namecard-input" class="form-label">Tanggal Pembayaran Klaim<code> *</code></label>
                                                          <input type="date" class="form-control" name="tanggal_pembayaran_klaim" value="<?php if(isset($d->tanggal_pembayaran_klaim)){echo $d->tanggal_pembayaran_klaim;}?>">
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-6">
                                                      <div class="mb-3">
                                                          <label for="progresspill-expiration-input" class="form-label">Bukti Pembayaran<code> *</code></label>
                                                          <input type="file" class="form-control" name="bukti_pembayaran">
                                                      </div>
                                                  </div>

                                                  <div class="col-lg-6">
                                                      <div class="mb-3">
                                                          <br>
                                                          <label for="progresspill-expiration-input" class="form-label">Uploaded File</label>
                                                          <p>
                                                              <?php if (isset($d->bukti_pembayaran)) {
                                                                  echo '
                                                                  <a target="_blank" href="'.base_url('upload/klaim_peralihan/'.$d->bukti_pembayaran).'">
                                                                    '.$d->bukti_pembayaran.'
                                                                  </a>';
                                                              } else {
                                                                echo 'File Belum diupload';
                                                              }
                                                              ?>
                                                          </p>
                                                      </div>
                                                  </div>

                                                  <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-namecard-input" class="form-label">Status Klaim<code> *</code></label>
                                                        <select class="form-select" name="status_klaim" required="">
                                                            <?php
                                                          foreach ($stat3 as $val) {
                                                          if ($d->status_klaim==$val['id']) {
                                                              echo"<option selected='selected' value='".$val['id']."'>".$val['status_klaim']."</option>"; 
                                                            } else {
                                                              echo"<option value='".$val['id']."'>".$val['status_klaim']."</option>"; 
                                                            }
                                                            }
                                                          ?> 
                                                        </select>
                                                    </div>
                                                </div>

                                              </div>
                                              <p></p>
                                              <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="progresspill-namecard-input" class="form-label"></label>

                                                      <?php if ($this->session->userdata('id_groups') == 1 ) { ?>

                                                       <button class="btn btn-success" type="button" id="btnForm3" onclick="submitForm3()">Simpan <i class="mdi mdi-content-save-check"></i></button>
                                                   <?php } else { ?>
                                                        <button class="btn btn-success" type="button" onclick="warning('warning','Oopss anda tidak berhak melakukan aksi ini!')" >Simpan <i class="mdi mdi-content-save-check"></i></button>
                                                   <?php } ?>

                                                       <!--  <a class="btn btn-info" type="submit">Download All Document <i class="bx bx-cloud-download"></i></a>
                                                        <a class="btn btn-warning" type="submit">Print Klaim to Asuransi <i class="bx bx-printer"></i></a> -->
                                                    </div>
                                                </div>

                                            </div>

                                            </form>
                                        </div>
                                    </div>
                            </div> 
                            <!-- end form wizard -->
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>

        </div><!-- end col -->
    </div><!-- end row -->

</div> <!-- container-fluid -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

    function uploadData()
    {
        let err = 0;

        var dokumen_tambahan = $("#dokumen_tambahan").val();
        if (dokumen_tambahan == '') {
            $(".err-dokumen_tambahan").show();
            err = 1;
        } else{
            $('.err-dokumen_tambahan').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        var formData = new FormData($("#formUploadDataTambahan")[0]);
        $.ajax({
              url:'<?=base_url('klaim_peralihan/uploadDataTambahan')?>',
              type:"post",
              data:formData,
              processData:false,
              contentType:false,
              cache:false,
              async:true,
              beforeSend: function(){
                    Swal.fire({
                        title: 'Waits',
                        html: 'File Uploading in progress',// add html attribute if you want or remove
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                success: function() {
                    swal.close();
                    message('success','Berhasil Disimpan');
                    window.setTimeout(function(){ 
                        window.location.reload();
                    } ,2000);
                },
                error: function(response){
                    warning('error', response.keterangan);
                }
        });
    }

    function saveHiji()
    {

        let err = 0;

        var norek = $("#norek").val();
        if (norek == '') {
            $(".err-norek").show();
            err = 1;
        } else{
            $('.err-norek').hide();
        }

        var noakad = $("#no_akad").val();
        if (noakad == '') {
            $(".err-no_akad").show();
            err = 1;
        } else{
            $('.err-no_akad').hide();
        }

        var sebab = $("#sebab_klaim").val();
        if (sebab == '') {
            $(".err-sebab_klaim").show();
            err = 1;
        } else{
            $('.err-sebab_klaim').hide();
        }

        var jenis_klaim = $("#jenis_klaim").val();
        if (jenis_klaim == '') {
            $(".err-jenis_klaim").show();
            err = 1;
        } else{
            $('.err-jenis_klaim').hide();
        }

        var tanggal_kejadian = $("#tanggal_kejadian").val();
        if (tanggal_kejadian == '') {
            $(".err-tanggal_kejadian").show();
            err = 1;
        } else{
            $('.err-tanggal_kejadian').hide();
        }

        var nilai_pokok_pengajuan_klaim = $("#nilai_pokok_pengajuan_klaim").val();
        if (nilai_pokok_pengajuan_klaim == '' || nilai_pokok_pengajuan_klaim == 0 ) {
            $(".err-nilai_pokok_pengajuan_klaim").show();
            err = 1;
        } else{
            $('.err-nilai_pokok_pengajuan_klaim').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '<?=base_url('klaim_peralihan/save')?>',
            data: $("#form1").serialize(),
            beforeSend: function(){
                Swal.fire({
                    title: 'Wait',
                    html: 'Data Saving in progress',// add html attribute if you want or remove
                    allowOutsideClick : false,
                    showConfirmButton : false,
                    didOpen: () => {
                        Swal.showLoading()
                    }
                });
            },
            success: function() {
                swal.close();
                    message('success', 'Berhasil disimpan');
                    window.setTimeout(function(){ 
                        window.location.reload();
                    } ,2000);
            }, error: function(){
                 warning('error', 'Gagal disimpan');
            }
        });
    }

    function submitForm2()
    {
      var formData = new FormData($("#form2")[0]);
        $.ajax({
              url:'<?=base_url('klaim_peralihan/save_persetujuandokumen')?>',
              type:"post",
              data:formData,
              processData:false,
              contentType:false,
              cache:false,
              async:true,
              beforeSend: function(){
                    Swal.fire({
                        title: 'Waits',
                        html: 'File Uploading in progress',// add html attribute if you want or remove
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                success: function() {
                    swal.close();
                    message('success','Berhasil Disimpan');
                    // window.setTimeout(function(){ 
                    //     window.location.reload();
                    // } ,2000);
                },
                error: function(){
                    warning('error', 'Gagal Disimpan');
                }
              
        });
    }

    function submitForm3()
    {
      var formData = new FormData($("#form3")[0]);
        $.ajax({
              url:'<?=base_url('klaim_peralihan/save_pembayaranklaim')?>',
              type:"post",
              data:formData,
              processData:false,
              contentType:false,
              cache:false,
              async:true,
              beforeSend: function(){
                    Swal.fire({
                        title: 'Waits',
                        html: 'File Uploading in progress',// add html attribute if you want or remove
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                success: function() {
                    swal.close();
                    message('success','Berhasil Disimpan');
                    window.setTimeout(function(){ 
                        window.location.reload();
                    } ,2000);
                },
                error: function(){
                    warning('error', 'Gagal Disimpan');
                }
        });
    }

    var premis = document.getElementById('premi_asuransi');
    premis.addEventListener('keyup', function(e)
    {
      premis.value = formatRupiah(this.value);
    });

    var plafonds = document.getElementById('plafond');
    plafonds.addEventListener('keyup', function(e)
    {
      plafonds.value = formatRupiah(this.value);
    });

    var nilaipengaju = document.getElementById('nilaipengajuan');
    nilaipengaju.addEventListener('keyup', function(e)
    {
      nilaipengaju.value = formatRupiah(this.value);
    });

    var nilaidibayar = document.getElementById('nilaidibayar');
    nilaidibayar.addEventListener('keyup', function(e)
    {
      nilaidibayar.value = formatRupiah(this.value);
    });

    var nilaipersetujuan = document.getElementById('nilaipersetujuan');
    nilaipersetujuan.addEventListener('keyup', function(e)
    {
      nilaipersetujuan.value = formatRupiah(this.value);
    });

    var pengajuan = document.getElementById('nilai_pokok_pengajuan_klaim');
    pengajuan.addEventListener('keyup', function(e)
    {
        
      pengajuan.value = formatRupiah(this.value);
      cek();
    });


    var total = document.getElementById('nilai_total_pengajuan_klaim');
    total.addEventListener('keyup', function(e)
    {
        total.value = formatRupiah(this.value, 'Rp. ');
    });

    function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

    function cek()
    {
        var jk = $('#jenis_klaim').val();
        var nilaipokook = $("#nilai_pokok_pengajuan_klaim").val().replace(/\./g,"").replace(",",".");
        var nilaiplafond = $("#plafond").val().replace(/\./g,"").replace(",",".");

        if (jk == 3) { //MACET
            $("#ketPokok").hide();

            var total = nilaipokook * (70/100);
            console.log(parseFloat(total));
            var x = formatRupiah(total.toString().replace('.',','), 'Rp. ');
            $("#nilai_total_pengajuan_klaim").val(x);
        } else if (jk == 1) { //MENINGGAL
            $("#ketPokok").hide();

            var y = formatRupiah(nilaipokook.toString().replace('.',','), 'Rp. ');
            $("#nilai_total_pengajuan_klaim").val(y);
        } else if (jk == 2) { //KEBAKARAN
            $("#ketPokok").show();

            var tujuhlima = nilaiplafond * 75/100;
            var seratus = nilaiplafond * 100/100;

            console.log(tujuhlima,seratus);

            if (nilaipokook <= tujuhlima) {
                var t = formatRupiah(tujuhlima.toString().replace('.',','), 'Rp. ');
                $("#nilai_total_pengajuan_klaim").val(t);
            } else if (nilaipokook >= seratus) {
                var s = formatRupiah(seratus.toString().replace('.',','), 'Rp. ');
                $("#nilai_total_pengajuan_klaim").val(s);
            } else {
                var n = formatRupiah(nilaipokook.toString().replace('.',','), 'Rp. ');
                $("#nilai_total_pengajuan_klaim").val(n);
            }

        }

        console.log(nilaiplafond);
        console.log(nilaipokook);

        if (parseFloat(nilaipokook) > parseFloat(nilaiplafond)) {
            $("#nilai_pokok_pengajuan_klaim").val(nilaiplafond);
        }

        
    }

    // function cekPokok()
    // {
    //     var nilaipokook = $("#nilai_pokok_pengajuan_klaim").val().replace(/\./g,"").replace(",",".");
    //     var nilaiplafond = $("#plafond").val().replace(/\./g,"").replace(",",".");

    //     if (nilaipokook > nilaiplafond) {
    //         $("#nilai_pokok_pengajuan_klaim").val(nilaiplafond);
    //     }
    // }

</script>



