<style type="text/css">
    th {
    white-space: nowrap;
}

td {
    white-space: nowrap;
}
</style>
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0 font-size-18"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Klaim</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">

        <div class="col-xl-12">
            <div class="card">
                
            
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#home" role="tab" aria-selected="false">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">List Data</span>    
                            </a>
                        </li>

                        <?php if ($this->session->userdata('id_groups') == 1 or $this->session->userdata('id_groups') == 2) { ?>

                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#profile" role="tab" aria-selected="false">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">Upload Data</span>    
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-bs-toggle="tab" href="#messages" role="tab" aria-selected="true">
                                <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                <span class="d-none d-sm-block">Upload Dokumen</span>    
                            </a>
                        </li>
                    <?php } ?>
                        
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content p-3 text-muted">
                        <div class="tab-pane active" id="home" role="tabpanel">
                           <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="mdi mdi-arrow-right text-primary me-1"></i> Filter Data
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div class="text-muted">
                                                <div class="mt-4 mt-lg-0">
                                                    <form id="form-filter" method="post">
                                                        
                                                         <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Region</label>
                                        <div class="col-sm-6">
                                            <select class="form-select" name="region" id="region" onchange="getArea()">
                                                <option></option>
                                                <?php

                                                $idregion = $this->session->userdata('id_region');
                                                $idcabang = $this->session->userdata('id_cabang');

                                                if ($this->session->userdata('id_groups') == 3) {
                                                   $gets = $this->db->query("select * from tm_cabang where id_region = '". $idregion."' ")->row();
                                                   $wherena = $gets->id_region;

                                                } else if ($this->session->userdata('id_groups') == 4) {
                                                   $gets = $this->db->query("select * from tm_cabang where id_region = '".$idregion."' ")->row();
                                                   $wherena = $gets->id_region;


                                                   // var_dump($idarea);
                                                   // die();

                                                } else if ($this->session->userdata('id_groups') == 5) {
                                                   $gets = $this->db->query("select * from tm_cabang where code_cabang = '".$idcabang."' ")->row();
                                                   $wherena = $gets->id_region;

                                                } else {
                                                    $gets = $this->db->query("select * from tm_cabang ")->row();
                                                }

                                                // $cab = $this->db->query();
                                                if ($this->session->userdata('id_groups') == 3 || $this->session->userdata('id_groups') == 4 || $this->session->userdata('id_groups') == 5 ) {
                                                    $q = $this->db->query("select id_region,region from tm_cabang where id_region = ".$gets->id_region." group by id_region,region ")->result_array();

                                                foreach ($q as $value) {
                                                     echo"<option value='".$value['id_region']."'>".$value['region']."</option>";
                                                }
                                                } else{
                                                $q = $this->db->query("select id_region,region from tm_cabang group by id_region,region ")->result_array();

                                                foreach ($q as $value) {
                                                     echo"<option value='".$value['id_region']."'>".$value['region']."</option>";
                                                }}
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Area</label>
                                        <div class="col-sm-6">
                                            <select class="form-select" name="area" id="area" onchange="getCabang()">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Cabang</label>
                                        <div class="col-sm-6">
                                            <select class="form-select" name="cabang" id="cabang" >
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Dari</label>
                                        <div class="col-sm-6">
                                            <input type="date" class="form-control" id="dari" name="dari">
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Sampai</label>
                                        <div class="col-sm-6">
                                            <input type="date" class="form-control" id="sampai" name="sampai">
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Status Klaim</label>
                                        <div class="col-sm-6">
                                           <select class="form-select" name="statusna" id="statusna" required>
                                                <option></option>
                                                <?php
                                                $q = $this->db->query("select id,status_klaim from tm_status_klaim")->result_array();
                                                foreach ($q as $value) {
                                                     echo"<option value='".$value['id']."'>".$value['status_klaim']."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    

                                                        <div class="hstack gap-3  justify-content-center">
                                                            <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                                                            <div class="vr"></div>
                                                            <button type="reset" class="btn btn-outline-danger" id="btn-reset">Reset</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- <div class="col-sm">
                                                <div class="mb-4">
                                                    <a href="<?=base_url('klaim_peralihan/add')?>" type="button" class="btn btn-soft-info waves-effect waves-light"><i class="bx bx-plus-circle me-1"></i> Buat Pengajuan</a>
                                                </div>
                                            </div> -->
                                            
                                            <table id="tablena" class="table table-striped mb-0" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Aksi</th>
                                                        <th>Status Klaim</th>
                                                        <th>Nomor Rekening</th>
                                                        <th>No. CIF</th>
                                                        <th>Nama Debitur</th>
                                                        <th>Tanggal lahir</th>
                                                        <th>No. KTP</th>
                                                        <th>Kantor Cabang</th>
                                                        <th>Jenis Klaim</th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                   
                                                </tbody>
                                            </table>

                                            <!-- <p><i>Keterangan : <i class="bx bx-edit-alt" style="color:blue"></i> Edit, <i class="bx bx-loader-circle fa-spin" style="color:red"></i> Dokumen Belum Lengkap </i>, <i class=" bx bxs-badge-check" style="color:green"></i> Dokumen Sudah Lengkap </i></p> -->
                                        </div>
                                    </div>
                                </div><!-- end card-body -->
                            </div>
                        </div>

                        <div class="tab-pane" id="profile" role="tabpanel">
                            <div class="card">
                                <div class="card-body">
                                    <form id="formDataKlaim" action="<?=base_url('klaim_peralihan/save')?>" method="post" enctype="multipart/form-data">
                                        <div class="row mb-2">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Upload Data Klaim <code>*</code> <i>(format excel)</i></label>
                                            <div class="col-sm-6">
                                              <input type="file" class="form-control" name="dokumen_klaim" id="dokumen_klaim"  accept=".xlsx">
                                              <div class="err-dokumen_klaim" style="display:none"><code><i>* Wajib Diisi</i></code></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row mb-2">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-6">
                                              <button type="reset" class="btn btn-warning">Reset</button>
                                              <button type="button" onclick="uploadData()" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                            
                        </div>

                        <div class="tab-pane " id="messages" role="tabpanel">
                            <div class="card">
                                <div class="card-body">
                                    <form id="formDataTambahan" action="<?=base_url('klaim_peralihan/save')?>" method="post" enctype="multipart/form-data">
                                        <div class="row mb-2">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Upload Dokumen <code>*</code> <i>(format .zip)</i></label>
                                            <div class="col-sm-6">
                                              <input type="file" class="form-control" name="dokumen_tambahan" id="dokumen_tambahan"  accept=".zip">
                                              <div class="err-dokumen_tambahan" style="display:none"><code><i>* Wajib Diisi</i></code></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row mb-2">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-6">
                                              <button type="reset" class="btn btn-warning">Reset</button>
                                              <button type="button" onclick="uploadDataTambahan()" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                        </div>

                    </div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div>

        


    </div>

</div><!-- end row -->




<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script> -->

<script type="text/javascript">
    $(document).ready( function(){
        $("#tablena").DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": '<?=base_url('klaim_peralihan/getData')?>',
                "type": "POST",
                "data": function ( data ) {
                    data.nama_debitur           = $('#nama_debitur').val();
                    data.no_akad                = $('#no_akad').val();
                    data.no_cif                 = $('#no_cif').val();
                    data.region         = $('#region').val();
                    data.area         = $('#area').val();
                    data.cabang         = $('#cabang').val();
                    data.statusna               = $('#statusna').val();
                    data.norek                  = $('#norek').val();
                    data.dari                  = $('#dari').val();
                    data.sampai                  = $('#sampai').val();
                    // data.address = $('#address').val();
                }
            },
            "columnDefs": [{
                "target": [-1],
                "orderable": false
            }],
            "scrollX": true
        });

        $('#btn-filter').click(function(){ 
            reloadTable();
        });
        $('#btn-reset').click(function(){ 
            $('#form-filter')[0].reset();
            reloadTable();
        });

    });

    function uploadData()
    {
        let err = 0;

        var dokumen_klaim = $("#dokumen_klaim").val();
        if (dokumen_klaim == '') {
            $(".err-dokumen_klaim").show();
            err = 1;
        } else{
            $('.err-dokumen_klaim').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        var formData = new FormData($("#formDataKlaim")[0]);
        $.ajax({
              url:'<?=base_url('klaim_peralihan/uploadData')?>',
              type:"post",
              data:formData,
              processData:false,
              contentType:false,
              cache:false,
              async:true,
              beforeSend: function(){
                    Swal.fire({
                        title: 'Waits',
                        html: 'File Uploading in progress',// add html attribute if you want or remove
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                success: function() {
                    swal.close();
                    message('success','Berhasil Disimpan');
                    window.setTimeout(function(){ 
                        window.location.reload();
                    } ,2000);
                },
                error: function(response){
                    warning('error', response.keterangan);
                }
        });
    }

    function uploadDataTambahan()
    {
        let err = 0;

        var dokumen_tambahan = $("#dokumen_tambahan").val();
        if (dokumen_tambahan == '') {
            $(".err-dokumen_tambahan").show();
            err = 1;
        } else{
            $('.err-dokumen_tambahan').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        var formData = new FormData($("#formDataTambahan")[0]);
        $.ajax({
              url:'<?=base_url('klaim_peralihan/uploadDataTambahan')?>',
              type:"post",
              data:formData,
              processData:false,
              contentType:false,
              cache:false,
              async:true,
              beforeSend: function(){
                    Swal.fire({
                        title: 'Waits',
                        html: 'File Uploading in progress',// add html attribute if you want or remove
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                success: function() {
                    swal.close();
                    message('success','Berhasil Disimpan');
                    window.setTimeout(function(){ 
                        window.location.reload();
                    } ,2000);
                },
                error: function(response){
                    warning('error', response.keterangan);
                }
        });
    }

    function cekDok(id) 
    {
        <?php if (isset($d->id)) { ?>
            $.get('<?=base_url('klaim_peralihan/statusdokwi/')?>' + id, function(data) {
            <?php } else { ?>
        $.get('<?=base_url('klaim_peralihan/statusdokni/')?>', function(data) {
        <?php } ?>
            $('#statusDokumen').html(data)
                $.ajax({
                    type: 'get',
                    url: $(this).attr('href'),
                    success: function(response){
                        $('#modalTitlena').text('Status Dokumen ');
                        $('#modalDok').modal('show');
                    }

                });
        });
    }

    function getArea()
    {        
        $.ajax({
            type: "POST", 
            url: "<?php echo base_url("penutupan_asuransi_jiwa/listArea"); ?>", 
            data: {id_region : $("#region").val()}, 
            dataType: "json",
            beforeSend: function(e) {
            if(e && e.overrideMimeType) {
                e.overrideMimeType("application/json;charset=UTF-8");
                }
            },
            success: function(response){ // Ketika proses pengiriman berhasil
                if (response.status == 'failed') {
                    $('#area').empty();
                    $('#cabang').empty();

                } else {
                    $("#area").html(response.list_area).show();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
            }
        });
    }

    function getCabang()
    {        
        $.ajax({
            type: "POST", 
            url: "<?php echo base_url("penutupan_asuransi_jiwa/listCabang"); ?>", 
            data: {id_area : $("#area").val()}, 
            dataType: "json",
            beforeSend: function(e) {
            if(e && e.overrideMimeType) {
                e.overrideMimeType("application/json;charset=UTF-8");
                }
            },
            success: function(response){ // Ketika proses pengiriman berhasil
                if (response.status == 'failed') {
                    $('#cabang').empty();
                    $('#area').empty();
                } else {
                    $("#cabang").html(response.list_cabang).show();

                }
            },
            error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
            }
        });
    }

</script>



