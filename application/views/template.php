<!doctype html>
<html lang="en">

    
<head>
        
        <meta charset="utf-8" />
        <title><?=$title?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="PT. Proteksi Jaya Mandiri" name="description" />
        <meta content="PT. Proteksi Jaya Mandiri" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url()?>assets/assets/images/favicon.ico">

        <!-- jquery.vectormap css -->
        <link href="<?=base_url()?>assets/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="<?=base_url()?>assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Responsive datatable examples -->
        <link href="<?=base_url()?>assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />  

        <!-- Bootstrap Css -->
        <link href="<?=base_url()?>assets/assets/css/bootstrap.min.css"  rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?=base_url()?>assets/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?=base_url()?>assets/assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body data-sidebar="dark">
    
    <!-- <body data-layout="horizontal" data-topbar="dark"> -->

        <!-- Begin page -->
        <div id="layout-wrapper">

            
            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box">
                            <a href="index.html" class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="<?=base_url()?>assets/assets/images/logo.png" alt="logo-sm-dark" height="70">
                                </span>
                                <span class="logo-lg">
                                    <img src="<?=base_url()?>assets/assets/images/logo.png" alt="logo-dark" height="70">
                                </span>
                            </a>

                            <a href="index.html" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="<?=base_url()?>assets/assets/images/logo.png" alt="logo-sm-light" height="70">
                                </span>
                                <span class="logo-lg">
                                    <img src="<?=base_url()?>assets/assets/images/logo.png" alt="logo-light" height="70">
                                </span>
                            </a>
                        </div>

                        <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                            <i class="ri-menu-2-line align-middle"></i>
                        </button>

                        
                    </div>

                    <div class="d-flex">

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
                                  data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="ri-notification-3-line"></i>
                                <span class="noti-dot"></span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                                aria-labelledby="page-header-notifications-dropdown">
                                <div class="p-3">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="m-0"> Notifications </h6>
                                        </div>
                                        <div class="col-auto">
                                            <a href="#!" class="small"> View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div data-simplebar style="max-height: 230px;">
                                    <a href="#" class="text-reset notification-item">
                                        <div class="d-flex">
                                            <div class="avatar-xs me-3">
                                                <span class="avatar-title bg-primary rounded-circle font-size-16">
                                                    <i class="ri-shopping-cart-line"></i>
                                                </span>
                                            </div>
                                            <div class="flex-1">
                                                <h6 class="mb-1">Welcome</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">Hy.. Selamat datang di Aplikasi E-Claim</p>
                                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    
                                </div>
                                <div class="p-2 border-top">
                                    <div class="d-grid">
                                        <a class="btn btn-sm btn-link font-size-14 text-center" href="javascript:void(0)">
                                            <i class="mdi mdi-arrow-right-circle me-1"></i> View More..
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block user-dropdown">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user" src="<?=base_url()?>assets/assets/images/users/avatar-2.jpg"
                                    alt="Header Avatar">
                                <span class="d-none d-xl-inline-block ms-1"><?=$this->session->userdata('nama')?></span>
                                <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <!-- item-->
                                <a class="dropdown-item" href="#"><i class="ri-lock-unlock-line align-middle me-1"></i> Setting</a>
                                <div class="dropdown-divider"></div>
                                <button class="dropdown-item text-danger" onclick="logout()"><i class="ri-shut-down-line align-middle me-1 text-danger"></i> Logout</button>
                            </div>
                        </div>

                        
            
                    </div>
                </div>
            </header>

            <!-- ========== Left Sidebar Start ========== -->
            <div class="vertical-menu">

                <div data-simplebar class="h-100">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu list-unstyled" id="side-menu">
                            <li class="menu-title">Menu</li>

                            <?php
                            $main = $this->db->query("select distinct a.* from tm_menu a join tt_menu b on a.id_menu = b.id_menu where b.id_groups = '".$this->session->userdata('id_groups')."' and a.kat_menu = 0 and a.status = '1' order by a.id_menu asc");

                            foreach ($main->result() as $m) {
                            $subs = $this->db->query("select a.* from tm_menu a join tt_menu b on a.id_menu = b.id_menu where b.id_groups = '".$this->session->userdata('id_groups')."' and a.kat_menu = '".$m->id_menu."' and a.status = '1' order by a.id_menu asc");

                                if ($subs->num_rows() > 0) {
                                    if ($sub_menu == $m->id_menu) {
                                        $open = 'mm-active';
                                    }else{
                                        $open = '';
                                    }
                                    
                                    echo '<li class="'.$open.'">
                                            <a href="'.$m->link.'" class="has-arrow waves-effect">
                                                <i class="'.$m->icon.'"></i>
                                                <span>'.$m->nama_menu.'</span>
                                            </a>';
                                    echo '<ul class="sub-menu" aria-expanded="false">';

                                 foreach ($subs->result() as $s) {
                                    if ($page_id == $s->id_menu){
                                        $aktif = 'mm-active'; 
                                    }else{
                                        $aktif = '';
                                    }

                                    echo '<li class="'.$aktif.'"><a href="'.base_url($s->link).'">'.$s->nama_menu.'</a></li>';
                                }
                                    echo "</ul>";
                                    echo '</li>';
                                } else {
                                    if ($page_id == $m->id_menu) {
                                        $aktif = 'mm-active';
                                    }else{
                                        $aktif = '';
                                    }

                                    echo '<li>
                                            <a href="'.base_url($m->link).'" class="waves-effect">
                                                <i class="'.$m->icon.'"></i>
                                                <span>'.$m->nama_menu.'</span>
                                            </a>
                                        </li>';
                                }
                            }
                        ?>
                
                        </ul>
                    </div>
                    <!-- Sidebar -->
                </div>
            </div>
            <!-- Left Sidebar End -->

            

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <?=$contents?>
                    
                </div>
                <!-- End Page-content -->
               
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <script>document.write(new Date().getFullYear())</script> © PT. Proteksi Jaya Mandiri.
                            </div>
                          <!--   <div class="col-sm-6">
                                <div class="text-sm-end d-none d-sm-block">
                                    Crafted with <i class="mdi mdi-heart text-danger"></i> by <a target="_blank">PT. Proteksi Jaya Mandiri</a>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </footer>
                
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="<?=base_url()?>assets/assets/libs/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/node-waves/waves.min.js"></script>

        
        <!-- apexcharts -->
        <script src="<?=base_url()?>assets/assets/libs/apexcharts/apexcharts.min.js"></script>

        <!-- jquery.vectormap map -->
        <script src="<?=base_url()?>assets/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-us-merc-en.js"></script>

        <!-- Required datatable js -->
        <script src="<?=base_url()?>assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        
        <!-- Responsive examples -->
        <script src="<?=base_url()?>assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

        <script src="<?=base_url()?>assets/assets/js/pages/dashboard.init.js"></script>

        <!-- App js -->
        <script src="<?=base_url()?>assets/assets/js/app.js"></script>

        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/assets/js/cus.js"></script>

        <script src="<?=base_url()?>assets/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

        <script src="<?=base_url()?>assets/assets/libs/twitter-bootstrap-wizard/prettify.js"></script>

        <!-- form wizard init -->
        <script src="<?=base_url()?>assets/assets/js/pages/form-wizard.init.js"></script>

        <script type="text/javascript">

             function update()
             {
                var id = '<?=$this->session->userdata('id_user')?>';
                $.ajax({
                    url         : "<?php echo base_url('login/update/'); ?>" + id,
                    type        : "post",
                    data        : $("#formSettings").serialize(),
                    dataType    : 'JSON',
                    success:function()
                    {
                       window.location.replace("<?=base_url('login/logout')?>");
                    },
                    error:function()
                    {
                        Swal.fire(
                          'Gagal!',
                          'Gagal untuk Logout',
                          'error'
                        )
                    }
                })
             }

            function logout()
            {
                Swal.fire({
                title: 'Anda Yakin?',
                text: "Akan Keluar Dari Aplikasi?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Keluar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url         : "<?php echo base_url('login/logout'); ?>",
                            type        : "post",
                            data        : $(this).serialize(),
                            success:function()
                            {
                               window.location.replace("<?=base_url('login')?>");
                            },
                            error:function()
                            {
                                Swal.fire(
                                  'Gagal!',
                                  'Gagal untuk Logout',
                                  'error'
                                )
                            }
                        })
                    }
                })
            }
        </script>
    </body>


</html>