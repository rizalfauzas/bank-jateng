<div class="container-fluid">
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Beranda</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-md-6">
            <div class="card border border" >
                <div class="card-header  text-white-50" style="text-align: center; background-color: #143668;">
                    <h5 class="my-0 text-white">Penutupan</h5>
                </div>
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-12 text-center">
                            <h4 class="mb-3">
                                <span>
                                    5,000
                                </span>
                                
                            </h4>
                        </div>

                    </div>
                    <div class="text-center">
                        <br>
                        <a href="#" class="btn btn-outline-primary waves-effect waves-light">View All</a>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="col-xl-6 col-md-6">
            <div class="card border border-primary">
                <div class="card-header  text-white-50" style="text-align: center; background-color:#143668">
                    <h5 class="my-0 text-white">Klaim</h5>
                </div>
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-12 text-center">
                            <h4 class="mb-3">
                                <span>
                                    5,000
                                </span>
                                
                            </h4>
                        </div>

                    </div>
                    <div class="text-center">
                        <br>
                        <a href="<?=base_url('klaim')?>" class="btn btn-outline-primary waves-effect waves-light">View All</a>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>

    <div class="row">
            
        <div class="col-xl-12 col-md-6 col-12">
            <div class="card">
                <div class="card-body">  
                    <div class="table-bordered table-responsive">
                        <table class="table">
                            <thead style="background-color: #143668;">
                                <tr>
                                <th class="text-center" style="width: 125px; color: white;">Open Date</th>
                                    <th class="text-center" style="color: white;">2019</th>

                                    <th class="text-center" style="color: white;">2020</th>

                                    <th class="text-center" style="color: white;">2021</th>

                                    <th class="text-center" style="color: white;">2022</th>

                                </tr>
                                
                            </thead>

                            <tbody>
                                <tr>
                                    <td class="text-center" style="color: white; background-color: #143668;">OS BALANCE TAGIHAN</td>
                                    <td class="text-center font-size-18 bg-secondary-light m-0">Rp.1.906.516.301,12</td>
                                    <td class="text-center font-size-18 bg-secondary-light m-0">Rp.28.995.013.203,77</td>
                                    <td class="text-center font-size-18 bg-secondary-light m-0">Rp.167.979.066.814,34</td>
                                    <td class="text-center font-size-18 bg-secondary-light m-0">Rp.120.429.919.913,43</td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                        <input type="hidden" id="chart-years-val" value="2019,2020,2021,2022">
                        <input type="hidden" id="chart-os-val" value="1906516301.12,28995013203.77,167979066814.34,120429919913.43">
                </div>                  
            </div>
        </div>
    
    
    </div>

    <div class="row">
            
        <div class="col-xl-6 col-md-6 col-12">
            <div class="card">
                <div class="card-body">  
                    <div class="table-bordered table-responsive">
                        <table class="table">
                            <thead style="background-color:#143668">
                                <tr>
                                    <th class="text-center" style="color: white;">Wilayah</th>
                                    <th class="text-center" style="color:white;">Outstanding Klaim</th>
                                </tr>
                                
                            </thead>

                            <tbody>
                                                                        <tr>
                                <td class="text-center font-size-12 bg-white m-0">Surakarta</td>
                                <td class="text-center font-size-12 bg-white m-0">Rp.32.716.625.285,55</td>
                                </tr><tr>
                                                                        </tr><tr>
                                <td class="text-center font-size-12 bg-white m-0">Sukoharjo</td>
                                <td class="text-center font-size-12 bg-white m-0">Rp.29.982.239.152,16</td>
                                </tr><tr>
                                                                        </tr><tr>
                                <td class="text-center font-size-12 bg-white m-0">Klaten</td>
                                <td class="text-center font-size-12 bg-white m-0">Rp.23.722.403.241,71</td>
                                </tr><tr>
                                                                        </tr><tr>
                                <td class="text-center font-size-12 bg-white m-0">Boyolali</td>
                                <td class="text-center font-size-12 bg-white m-0">Rp.20.511.690.055,58</td>
                                </tr><tr>
                                                                        </tr><tr>
                                <td class="text-center font-size-12 bg-white m-0">Karanganyar</td>
                                <td class="text-center font-size-12 bg-white m-0">Rp.20.300.991.023,50</td>
                                </tr><tr>
                                                                        </tr><tr>
                                <td class="text-center font-size-12 bg-white m-0">Wonogiri</td>
                                <td class="text-center font-size-12 bg-white m-0">Rp.18.986.381.204,48</td>
                                </tr><tr>
                                                                        </tr><tr>
                                <td class="text-center font-size-12 bg-white m-0">Sragen</td>
                                <td class="text-center font-size-12 bg-white m-0">Rp.18.684.033.472,78</td>
                                </tr><tr>
                                                                        </tr></tbody>

                        </table>
                    </div>
                            
                </div>                  
            </div>
        </div>

        <div class="col-xl-6 col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Outstanding Klaim</h4>
                   <div id="donut-chart" class="apex-charts"></div>

                    <div class="row">
                        <div class="col-4">
                            <div class="text-center mt-4">
                                <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-primary font-size-10 me-1"></i> 2020</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center mt-4">
                                <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-success font-size-10 me-1"></i> 2021</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center mt-4">
                                <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-warning font-size-10 me-1"></i> 2022</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Outstanding Klaim</h4>
                   <div id="donut-chart" class="apex-charts"></div>

                    <div class="row">
                        <div class="col-4">
                            <div class="text-center mt-4">
                                <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-primary font-size-10 me-1"></i> 2020</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center mt-4">
                                <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-success font-size-10 me-1"></i> 2021</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-center mt-4">
                                <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-warning font-size-10 me-1"></i> 2022</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->


    
    
    </div>

    

    

</div>