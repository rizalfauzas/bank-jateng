<div class="container-fluid">
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Beranda</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Cabang</th>
                                <th>Penyebab Klaim Ditolak</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>TEST1</strong></td>
                                <td>13/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>
                            <tr>
                                <td><strong></strong></td>
                                <td>14/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>
                            <tr>
                                <td><strong></strong></td>
                                <td>15/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>

                            <tr>
                                <td><strong>TEST2</strong></td>
                                <td>13/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>
                            <tr>
                                <td><strong></strong></td>
                                <td>14/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>
                            <tr>
                                <td><strong></strong></td>
                                <td>15/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>

                            <tr>
                                <td><strong>TEST3</strong></td>
                                <td>13/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>
                            <tr>
                                <td><strong></strong></td>
                                <td>14/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>
                            <tr>
                                <td><strong></strong></td>
                                <td>15/10/2022: Daluarsa pengajuan Klaim baru diajukan tgl 29/07/2022 yakni 147 hari kalender dari DOL, surat resmi akan segera kami kirim</td>
                            </tr>

                            <tr>
                                <td><strong>TEST4</strong></td>
                                <td>-</td>
                            </tr>
                            
                        </tbody>
                    </table>
                    
                </div>
            </div>
            
        </div>
    </div>

    

</div>