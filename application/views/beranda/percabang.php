<div class="container-fluid">
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Beranda</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">

                    <table class="table table-striped mb-0">

                        <thead>
                            <tr>
                                <th>Cabang</th>
                                <th>Jumlah Debitur</th>
                                <th>Total Outstanding</th>
                                <th>Persentase</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>TEST 1</td>
                                <td>4,763</td>
                                <td>316,506,857,833</td>
                                <td>14,06</td>
                            </tr>

                            <tr>
                                <td>TEST 2</td>
                                <td>4,763</td>
                                <td>316,506,857,833</td>
                                <td>14,06</td>
                            </tr>

                            <tr>
                                <td>TEST 3</td>
                                <td>4,763</td>
                                <td>316,506,857,833</td>
                                <td>14,06</td>
                            </tr>

                            <tr>
                                <td>TEST 4</td>
                                <td>4,763</td>
                                <td>316,506,857,833</td>
                                <td>14,06</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
        
    </div>

     <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="col-xl-12 col-md-12">
                        <div class="text-align-center"><h5>OUTSTANDING CLAIM BY MITRA</h5></div>
                        <div class="card-body" id="chart">
                        </div>
                    </div>

                    
                </div>
                
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="col-xl-12 col-md-12">

                        <div class="text-align-center"><h5>OUTSTANDING CLAIM (%)</h5></div>
                        <div class="card-body" id="chart2">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>

</div>

<script src="<?=base_url()?>assets/assets/libs/apexcharts/apexcharts.min.js"></script>
<script>

     var options = {
          series: [{
          name: 'TEST1',
          data: [3000000000000]
        }, {
          name: 'TEST2',
          data: [2000000000000]
        }, {
          name: 'TEST3',
          data: [1000000000000]
        },
        {
          name: 'TEST4',
          data: [1000000000000]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['TEST1','TEST2','TEST3','TEST4'],
        },
        yaxis: {
          title: {
            text: 'Rp (rupiah)'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "Rp " + val
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
      
      
</script>

<script type="text/javascript">
        var options = {
          series: [3000000000000, 2000000000000, 1000000000000,1000000000000],
          chart: {
          width: 400,
          type: 'pie',
        },
        labels: ['TEST1', 'TEST2', 'TEST3','TEST4'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart2"), options);
        chart.render();
</script>