<!doctype html>
<html lang="en">

    
<head>
        
        <meta charset="utf-8" />
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesdesign" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url()?>assets/assets/images/favicon.ico">

        <!-- Bootstrap Css -->
        <link href="<?=base_url()?>assets/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?=base_url()?>assets/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?=base_url()?>assets/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body class="bg-pattern" style="background-image: url('<?=base_url()?>assets/assets/images/p-1.png'); background-size: cover; background-position: center center;">
        <div class=""></div>
        <div class="account-pages my-5 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-4 col-lg-6 col-md-8">
                        <div class="card">
                            <div class="card-body p-4">
                                <div class="">
                                    <div class="text-center">
                                        <a href="index.html" class="">
                                            <img src="<?=base_url()?>assets/assets/images/logonax.png" alt="" height="50" class="auth-logo logo-dark mx-auto">
                                            <img src="<?=base_url()?>assets/assets/images/logonax.png" alt="" height="50" class="auth-logo logo-light mx-auto">
                                        </a>
                                    </div>
                                    <!-- end row -->
                                    <h4 class="font-size-18 text-muted mt-2 text-center">
                                        <?php 
                                            if (date('H:i:s') > '00:01:00' && date('H:i:s') < '10:00:00') {
                                              $selamatnaon = 'Pagi';
                                            } else if (date('H:i:s') > '10:00:00' && date('H:i:s') < '15:00:00') {
                                              $selamatnaon = 'Siang';
                                            } else if (date('H:i:s') > '15:00:00' && date('H:i:s') < '18:00:00') {
                                              $selamatnaon = 'Sore';
                                            } else if (date('H:i:s') > '18:00:00' && date('H:i:s') < '23:58:59') {
                                              $selamatnaon = 'Malam';
                                            }
                                        ?>
                                        Selamat <?=$selamatnaon?>
                                    </h4>
                                    <p class="mb-5 text-center">Sign in untuk masuk ke Aplikasi E-Claim.</p>
                                    <form class="form-horizontal" action="<?=base_url('login')?>" method="post">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-4">
                                                    <label class="form-label" for="username">Username</label>
                                                    <input type="text" class="form-control" name="username" id="username" placeholder="Enter username" autofocus>
                                                </div>
                                                <div class="mb-4">
                                                    <label class="form-label" for="userpassword">Password</label>
                                                    <input type="password" class="form-control" name="password" placeholder="Enter password">
                                                </div>

                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-check">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <div class="text-md-end mt-3 mt-md-0">
                                                            <button type="button" style="padding: 0;border: none;background: none;" onclick="warning('warning','hubungi Administrator untuk mereset Password')" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-grid mt-4">
                                                    <button class="btn btn-primary waves-effect waves-light" type="submit">Log In</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5 text-center">
                            <p class="text-black-50">Tidak punya Akun ? <button type="button" style="padding: 0;border: none;background: none;" onclick="warning('warning','Hubungi Administrator untuk membuat akun')" class="fw-medium text-primary"> Daftar </button> </p>
                            <p class="text-black-50">© <script>document.write(new Date().getFullYear())</script> PT. Proteksi Jaya Mandiri</p>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
        <!-- end Account pages -->

        <!-- JAVASCRIPT -->
        <script src="<?=base_url()?>assets/assets/libs/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?=base_url()?>assets/assets/libs/node-waves/waves.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script src="<?=base_url()?>assets/assets/js/app.js"></script>
        <script src="<?=base_url()?>assets/assets/js/cus.js"></script>

    </body>

<!-- Mirrored from themesdesign.in/upzet/layouts/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Oct 2022 07:47:03 GMT -->
</html>
